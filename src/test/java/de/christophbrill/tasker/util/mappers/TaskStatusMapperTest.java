package de.christophbrill.tasker.util.mappers;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.tasker.persistence.model.TaskStatusEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.ui.dto.TaskStatusGeneric;
import org.junit.jupiter.api.Test;

class TaskStatusMapperTest {

    @Test
    void testMapDtoToEntity() {
        TaskStatusGeneric source = new TaskStatusGeneric();
        source.comment = "Hallo";

        TaskStatusEntity target = TaskStatusMapper.INSTANCE.mapDtoToEntity(source);

        assertThat(target).isInstanceOf(TaskStatusGenericEntity.class);
        assertThat(((TaskStatusGenericEntity) target).comment).isEqualTo(source.comment);
    }
}
