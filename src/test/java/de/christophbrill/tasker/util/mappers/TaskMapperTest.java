package de.christophbrill.tasker.util.mappers;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEmailEntity;
import de.christophbrill.tasker.ui.dto.Task;
import de.christophbrill.tasker.ui.dto.TaskStatusEmail;
import java.util.Collections;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class TaskMapperTest {

    @Test
    void testMappingDtoToEntity() {
        TaskStatusEmail email = new TaskStatusEmail();
        email.body = UUID.randomUUID().toString();

        Task task = new Task();
        task.title = UUID.randomUUID().toString();
        task.status = Collections.singletonList(email);
        task.assignee = UUID.randomUUID().toString();

        TaskEntity entity = TaskMapper.INSTANCE.mapDtoToEntity(task);

        assertThat(entity).isNotNull();
        assertThat(entity.title).isEqualTo(task.title);
        assertThat(entity.status)
                .isNotNull()
                .isEmpty();
        assertThat(entity.assignee).isNull();
    }

    @Test
    void testMappingEntityToDto() {
        TaskStatusEmailEntity email = new TaskStatusEmailEntity();
        email.body = UUID.randomUUID().toString();

        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.status = Collections.singletonList(email);

        Task dto = TaskMapper.INSTANCE.mapEntityToDto(task);

        assertThat(dto).isNotNull();
        assertThat(dto.title).isEqualTo(task.title);
        assertThat(dto.status)
                .isNotNull()
                .hasSize(1);
    }
}
