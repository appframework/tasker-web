package de.christophbrill.tasker.ui;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.tasker.ui.dto.Label;
import de.christophbrill.tasker.ui.dto.Priority;
import de.christophbrill.tasker.ui.dto.State;
import de.christophbrill.tasker.ui.dto.Task;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;

@QuarkusTest
class TaskCRUDTest extends AbstractCRUDTest<Task> {

    @Override
    protected Task createFixture() {
        Task fixture = new Task();
        fixture.title = UUID.randomUUID().toString();
        fixture.state = State.NEW;
        fixture.priority = Priority.MEDIUM;
        return fixture;
    }

    @Override
    protected String getPath() {
        return "task";
    }

    @Override
    protected Class<Task> getFixtureClass() {
        return Task.class;
    }

    @Override
    protected void modifyFixture(Task fixture) {
        fixture.title = UUID.randomUUID().toString();
    }

    @Override
    protected void compareDtos(Task fixture, Task created) {
        assertThat(created.title).isEqualTo(fixture.title);
    }

    @Test
    void testLabeling() {
        // given: A list of existing labels
        List<Label> labelsBefore = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .get("/rest/label")
            .then()
            .statusCode(200)
            .extract()
            .body()
            .jsonPath()
            .getList(".", Label.class);
        final int labelsBeforeSize = labelsBefore.size();

        // when: a task gets created without any specified labels
        Task createdTask = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .body(createFixture())
            .post("/rest/task")
            .then()
            .statusCode(200)
            .extract()
            .as(Task.class);

        // then: The task has no labels
        assertThat(createdTask.labels).isNull();

        // when: A label is added to the task
        Label newLabel = new LabelCRUDTest().createFixture();
        Label createdLabel = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .body(newLabel)
            .post("/rest/task/{id}/addlabel", createdTask.id)
            .then()
            .statusCode(200)
            .extract()
            .as(Label.class);
        // then: it was created with black color
        assertThat(createdLabel.id).isNotNull();
        assertThat(createdLabel.color).isEqualTo("#000000");

        // when: the task is loaded again
        Task withLabel = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .body(createdLabel)
            .get("/rest/task/{id}", createdTask.id)
            .then()
            .statusCode(200)
            .extract()
            .as(Task.class);

        // then: it has a label
        assertThat(withLabel.labels).hasSize(1);
        assertThat(withLabel.labels.getFirst().name).isEqualTo(newLabel.name);

        // when: the label is removed from the task
        given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .body(createdLabel.id)
            .post("/rest/task/{id}/removelabel", createdTask.id)
            .then()
            .statusCode(204);

        // when: the task is loaded again
        Task withoutLabel = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .body(createdLabel)
            .get("/rest/task/{id}", createdTask.id)
            .then()
            .statusCode(200)
            .extract()
            .as(Task.class);

        // then: it no longer has a label
        assertThat(withoutLabel.labels).isEmpty();

        // then: the label still exists
        List<Label> labelsAfter = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .get("/rest/label")
            .then()
            .statusCode(200)
            .extract()
            .body()
            .jsonPath()
            .getList(".", Label.class);
        assertThat(labelsAfter).hasSize(labelsBeforeSize + 1);

        // when: the label is deleted
        given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .delete("/rest/label/{id}", createdLabel.id)
            .then()
            .statusCode(204);

        // then: the label is gone
        List<Label> labelsFinally = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .get("/rest/label")
            .then()
            .statusCode(200)
            .extract()
            .body()
            .jsonPath()
            .getList(".", Label.class);
        assertThat(labelsFinally).hasSize(labelsBeforeSize);
    }
}
