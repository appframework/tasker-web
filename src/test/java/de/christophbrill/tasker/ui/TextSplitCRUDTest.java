package de.christophbrill.tasker.ui;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.tasker.ui.dto.TextSplit;
import io.quarkus.test.junit.QuarkusTest;
import java.util.UUID;

@QuarkusTest
public class TextSplitCRUDTest extends AbstractCRUDTest<TextSplit> {

    @Override
    protected TextSplit createFixture() {
        TextSplit fixture = new TextSplit();
        fixture.expression = "create" + UUID.randomUUID();
        return fixture;
    }

    @Override
    protected String getPath() {
        return "textsplit";
    }

    @Override
    protected Class<TextSplit> getFixtureClass() {
        return TextSplit.class;
    }

    @Override
    protected void modifyFixture(TextSplit fixture) {
        fixture.expression = "update" + UUID.randomUUID();
    }

    @Override
    protected void compareDtos(TextSplit fixture, TextSplit created) {
        assertThat(created.expression).isEqualTo(fixture.expression);
    }
}
