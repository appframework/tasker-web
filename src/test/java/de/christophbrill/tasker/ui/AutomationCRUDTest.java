package de.christophbrill.tasker.ui;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.tasker.ui.dto.Automation;
import io.quarkus.test.junit.QuarkusTest;
import java.util.UUID;

@QuarkusTest
class AutomationCRUDTest extends AbstractCRUDTest<Automation> {

    @Override
    protected Automation createFixture() {
        Automation fixture = new Automation();
        fixture.expression = UUID.randomUUID().toString();
        fixture.textMatch = Automation.MatchType.EQUALS_COMPLETE;
        fixture.qualifiedName = "de.christophbrill.tasker.util.automations.WeeklyReminderAutomationProcessor";
        return fixture;
    }

    @Override
    protected String getPath() {
        return "automation";
    }

    @Override
    protected Class<Automation> getFixtureClass() {
        return Automation.class;
    }

    @Override
    protected void modifyFixture(Automation fixture) {
        fixture.expression = UUID.randomUUID().toString();
        fixture.textMatch = Automation.MatchType.REGULAR_EXPRESSION;
    }

    @Override
    protected void compareDtos(Automation fixture, Automation created) {
        assertThat(created.expression).isEqualTo(fixture.expression);
    }
}
