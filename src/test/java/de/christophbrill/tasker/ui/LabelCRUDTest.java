package de.christophbrill.tasker.ui;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.tasker.ui.dto.Label;
import io.quarkus.test.junit.QuarkusTest;
import java.util.UUID;

@QuarkusTest
public class LabelCRUDTest extends AbstractCRUDTest<Label> {

    @Override
    protected Label createFixture() {
        Label fixture = new Label();
        fixture.name = UUID.randomUUID().toString();
        fixture.color = "#001122";
        return fixture;
    }

    @Override
    protected String getPath() {
        return "label";
    }

    @Override
    protected Class<Label> getFixtureClass() {
        return Label.class;
    }

    @Override
    protected void modifyFixture(Label fixture) {
        fixture.color = "#334455";
    }

    @Override
    protected void compareDtos(Label fixture, Label created) {
        assertThat(created.color).isEqualTo(fixture.color);
    }
}
