package de.christophbrill.tasker.ui;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.ui.dto.State;
import de.christophbrill.tasker.ui.dto.TaskStatusGeneric;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.transaction.Transactional;
import java.util.UUID;

@QuarkusTest
public class TaskStatusGenericCRUDTest extends AbstractCRUDTest<TaskStatusGeneric> {

    @Override
    @Transactional
    protected TaskStatusGeneric createFixture() {
        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.persistAndFlush();

        TaskStatusGeneric fixture = new TaskStatusGeneric();
        fixture.comment = "create" + UUID.randomUUID();
        fixture.taskId = task.id;
        return fixture;
    }

    @Override
    protected String getPath() {
        return "taskstatus";
    }

    @Override
    protected Class<TaskStatusGeneric> getFixtureClass() {
        return TaskStatusGeneric.class;
    }

    @Override
    protected void modifyFixture(TaskStatusGeneric fixture) {
        fixture.comment = "update" + UUID.randomUUID();
        fixture.state = State.IN_PROGRESS;
    }

    @Override
    protected void compareDtos(TaskStatusGeneric fixture, TaskStatusGeneric created) {
        assertThat(created.comment).isEqualTo(fixture.comment);
    }

    @Override
    protected TaskStatusGeneric emptyFixture() {
        return new TaskStatusGeneric();
    }
}
