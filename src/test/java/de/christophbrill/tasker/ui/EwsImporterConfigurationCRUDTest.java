package de.christophbrill.tasker.ui;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.tasker.ui.dto.EwsImporterConfiguration;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Disabled;
import java.util.UUID;

@QuarkusTest
@Disabled("Somehow @JsonSubTypes no longer writes the type property, presumably since switching from quarkus-resteasy to quarkus-rest")
public class EwsImporterConfigurationCRUDTest extends AbstractCRUDTest<EwsImporterConfiguration> {

    @Override
    protected EwsImporterConfiguration createFixture() {
        EwsImporterConfiguration fixture = new EwsImporterConfiguration();
        fixture.server = UUID.randomUUID().toString();
        fixture.username = UUID.randomUUID().toString();
        fixture.password = UUID.randomUUID().toString();
        fixture.category = UUID.randomUUID().toString();
        return fixture;
    }

    @Override
    protected String getPath() {
        return "importerconfiguration";
    }

    @Override
    protected Class<EwsImporterConfiguration> getFixtureClass() {
        return EwsImporterConfiguration.class;
    }

    @Override
    protected void modifyFixture(EwsImporterConfiguration fixture) {
        fixture.server = UUID.randomUUID().toString();
    }

    @Override
    protected void compareDtos(EwsImporterConfiguration fixture, EwsImporterConfiguration created) {
        assertThat(created.server).isEqualTo(fixture.server);
    }

    @Override
    protected EwsImporterConfiguration emptyFixture() {
        return new EwsImporterConfiguration();
    }
}
