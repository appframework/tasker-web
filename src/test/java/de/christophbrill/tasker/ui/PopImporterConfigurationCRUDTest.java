package de.christophbrill.tasker.ui;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.tasker.ui.dto.PopImporterConfiguration;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Disabled;
import java.util.UUID;

@QuarkusTest
@Disabled("Somehow @JsonSubTypes no longer writes the type property, presumably since switching from quarkus-resteasy to quarkus-rest")
public class PopImporterConfigurationCRUDTest extends AbstractCRUDTest<PopImporterConfiguration> {

    @Override
    protected PopImporterConfiguration createFixture() {
        PopImporterConfiguration fixture = new PopImporterConfiguration();
        fixture.server = UUID.randomUUID().toString();
        fixture.username = UUID.randomUUID().toString();
        fixture.password = UUID.randomUUID().toString();
        return fixture;
    }

    @Override
    protected String getPath() {
        return "importerconfiguration";
    }

    @Override
    protected Class<PopImporterConfiguration> getFixtureClass() {
        return PopImporterConfiguration.class;
    }

    @Override
    protected void modifyFixture(PopImporterConfiguration fixture) {
        fixture.server = UUID.randomUUID().toString();
    }

    @Override
    protected void compareDtos(PopImporterConfiguration fixture, PopImporterConfiguration created) {
        assertThat(created.server).isEqualTo(fixture.server);
    }

    @Override
    protected PopImporterConfiguration emptyFixture() {
        return new PopImporterConfiguration();
    }
}
