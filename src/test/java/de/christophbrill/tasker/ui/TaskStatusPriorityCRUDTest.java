package de.christophbrill.tasker.ui;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.ui.dto.Priority;
import de.christophbrill.tasker.ui.dto.State;
import de.christophbrill.tasker.ui.dto.TaskStatusPriority;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.transaction.Transactional;
import java.util.UUID;

@QuarkusTest
public class TaskStatusPriorityCRUDTest extends AbstractCRUDTest<TaskStatusPriority> {

    @Override
    @Transactional
    protected TaskStatusPriority createFixture() {
        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.persistAndFlush();

        TaskStatusPriority fixture = new TaskStatusPriority();
        fixture.priority = Priority.MEDIUM;
        fixture.taskId = task.id;
        return fixture;
    }

    @Override
    protected String getPath() {
        return "taskstatus";
    }

    @Override
    protected Class<TaskStatusPriority> getFixtureClass() {
        return TaskStatusPriority.class;
    }

    @Override
    protected void modifyFixture(TaskStatusPriority fixture) {
        fixture.priority = Priority.LOW;
        fixture.state = State.IN_PROGRESS;
    }

    @Override
    protected void compareDtos(TaskStatusPriority fixture, TaskStatusPriority created) {
        assertThat(created.priority).isEqualTo(fixture.priority);
    }

    @Override
    protected TaskStatusPriority emptyFixture() {
        return new TaskStatusPriority();
    }
}
