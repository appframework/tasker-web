package de.christophbrill.tasker.ui;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.ui.dto.State;
import de.christophbrill.tasker.ui.dto.TaskStatusPerson;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.transaction.Transactional;
import java.util.UUID;

@QuarkusTest
public class TaskStatusPersonCRUDTest extends AbstractCRUDTest<TaskStatusPerson> {

    @Override
    @Transactional
    protected TaskStatusPerson createFixture() {
        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.persistAndFlush();

        TaskStatusPerson fixture = new TaskStatusPerson();
        fixture.name = "create" + UUID.randomUUID();
        fixture.taskId = task.id;
        return fixture;
    }

    @Override
    protected String getPath() {
        return "taskstatus";
    }

    @Override
    protected Class<TaskStatusPerson> getFixtureClass() {
        return TaskStatusPerson.class;
    }

    @Override
    protected void modifyFixture(TaskStatusPerson fixture) {
        fixture.name = "update" + UUID.randomUUID();
        fixture.state = State.IN_PROGRESS;
    }

    @Override
    protected void compareDtos(TaskStatusPerson fixture, TaskStatusPerson created) {
        assertThat(created.name).isEqualTo(fixture.name);
    }

    @Override
    protected TaskStatusPerson emptyFixture() {
        return new TaskStatusPerson();
    }
}
