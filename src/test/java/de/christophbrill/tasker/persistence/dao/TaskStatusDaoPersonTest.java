package de.christophbrill.tasker.persistence.dao;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusPersonEntity;
import de.christophbrill.tasker.ui.dto.State;
import io.quarkus.test.junit.QuarkusTest;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;

@QuarkusTest
class TaskStatusDaoPersonTest extends AbstractDaoCRUDTest<TaskStatusPersonEntity> {

    @Override
    protected TaskStatusPersonEntity createFixture() {
        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.persistAndFlush();

        TaskStatusPersonEntity taskstatus = new TaskStatusPersonEntity();
        taskstatus.task = task;
        taskstatus.state = State.IN_PROGRESS;
        taskstatus.name = UUID.randomUUID().toString();
        return taskstatus;
    }

    @Override
    protected void modifyFixture(TaskStatusPersonEntity taskstatus) {
        taskstatus.name = UUID.randomUUID().toString();
    }

    @Test
    void testGetAssignees() {
        List<Object[]> assignees = TaskStatusPersonEntity.getAssignees("");

        assertThat(assignees)
                .isNotNull()
                .hasSizeGreaterThanOrEqualTo(5);

        assignees = TaskStatusPersonEntity.getAssignees("admin");

        assertThat(assignees)
                .isNotNull()
                .isEmpty();
    }

    @Override
    protected TaskStatusPersonEntity findById(Long id) {
        return TaskStatusPersonEntity.findById(id);
    }

    @Override
    protected Long count() {
        return TaskStatusPersonEntity.count();
    }
}
