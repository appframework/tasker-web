package de.christophbrill.tasker.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusDueEntity;
import de.christophbrill.tasker.ui.dto.State;
import io.quarkus.test.junit.QuarkusTest;
import java.time.LocalDateTime;
import java.util.UUID;

@QuarkusTest
class TaskStatusDaoDueTest extends AbstractDaoCRUDTest<TaskStatusDueEntity> {

    @Override
    protected TaskStatusDueEntity createFixture() {
        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.persistAndFlush();

        TaskStatusDueEntity taskstatus = new TaskStatusDueEntity();
        taskstatus.task = task;
        taskstatus.state = State.IN_PROGRESS;
        taskstatus.deadline = false;
        taskstatus.due = LocalDateTime.now();
        return taskstatus;
    }

    @Override
    protected void modifyFixture(TaskStatusDueEntity taskstatus) {
        taskstatus.deadline = true;
    }

    @Override
    protected TaskStatusDueEntity findById(Long id) {
        return TaskStatusDueEntity.findById(id);
    }

    @Override
    protected Long count() {
        return TaskStatusDueEntity.count();
    }
}
