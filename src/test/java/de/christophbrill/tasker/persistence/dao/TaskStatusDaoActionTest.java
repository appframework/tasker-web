package de.christophbrill.tasker.persistence.dao;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusActionEntity;
import de.christophbrill.tasker.ui.dto.State;
import io.quarkus.test.junit.QuarkusTest;
import java.util.UUID;
import org.junit.jupiter.api.Test;

@QuarkusTest
class TaskStatusDaoActionTest extends AbstractDaoCRUDTest<TaskStatusActionEntity> {

    @Override
    protected TaskStatusActionEntity createFixture() {
        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.persistAndFlush();

        TaskStatusActionEntity taskstatus = new TaskStatusActionEntity();
        taskstatus.task = task;
        taskstatus.state = State.IN_PROGRESS;
        taskstatus.title = UUID.randomUUID().toString();
        return taskstatus;
    }

    @Override
    protected void modifyFixture(TaskStatusActionEntity taskstatus) {
        taskstatus.completed = true;
    }

    @Test
    void testGetAction() {
        TaskStatusActionEntity action = TaskStatusActionEntity.findByTaskAndTitle(5, "This is a title").firstResult();

        assertThat(action).isNotNull();
        assertThat(action.id).isEqualTo(11);

        action = TaskStatusActionEntity.findByTaskAndTitle(4, "This is a title").firstResult();
        assertThat(action).isNull();

        action = TaskStatusActionEntity.findByTaskAndTitle(5, "This is not a title").firstResult();
        assertThat(action).isNull();
    }

    @Override
    protected TaskStatusActionEntity findById(Long id) {
        return TaskStatusActionEntity.findById(id);
    }

    @Override
    protected Long count() {
        return TaskStatusActionEntity.count();
    }
}
