package de.christophbrill.tasker.persistence.dao;

import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusFileEntity;
import de.christophbrill.tasker.ui.dto.State;
import io.quarkus.test.junit.QuarkusTest;
import java.util.UUID;

@QuarkusTest
class TaskStatusDaoFileTest extends AbstractDaoCRUDTest<TaskStatusFileEntity> {

    @Override
    protected TaskStatusFileEntity createFixture() {
        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.persistAndFlush();

        BinaryDataEntity binaryData = new BinaryDataEntity();
        binaryData.contentType = "image/png";
        binaryData.filename = "image.png";
        binaryData.size = 5;
        binaryData.data = new byte[] { 0, 1, 2, 3, 4 };
        binaryData.persist();

        TaskStatusFileEntity taskstatus = new TaskStatusFileEntity();
        taskstatus.task = task;
        taskstatus.state = State.IN_PROGRESS;
        taskstatus.file = binaryData;
        return taskstatus;
    }

    @Override
    protected void modifyFixture(TaskStatusFileEntity taskstatus) {
        BinaryDataEntity binaryData = new BinaryDataEntity();
        binaryData.contentType = "image/jpg";
        binaryData.filename = "image.jpg";
        binaryData.size = 7;
        binaryData.data = new byte[] { 0, 1, 2, 3, 4, 5, 6 };
        binaryData.persist();

        taskstatus.file = binaryData;
    }

    @Override
    protected TaskStatusFileEntity findById(Long id) {
        return TaskStatusFileEntity.findById(id);
    }

    @Override
    protected Long count() {
        return TaskStatusFileEntity.count();
    }
}
