package de.christophbrill.tasker.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.ui.dto.State;
import io.quarkus.test.junit.QuarkusTest;
import java.util.UUID;

@QuarkusTest
class TaskStatusDaoGenericTest extends AbstractDaoCRUDTest<TaskStatusGenericEntity> {

    @Override
    protected TaskStatusGenericEntity createFixture() {
        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.persistAndFlush();

        TaskStatusGenericEntity taskstatus = new TaskStatusGenericEntity();
        taskstatus.task = task;
        taskstatus.state = State.IN_PROGRESS;
        taskstatus.comment = UUID.randomUUID().toString();
        return taskstatus;
    }

    @Override
    protected void modifyFixture(TaskStatusGenericEntity taskstatus) {
        taskstatus.comment = UUID.randomUUID().toString();
    }

    @Override
    protected TaskStatusGenericEntity findById(Long id) {
        return TaskStatusGenericEntity.findById(id);
    }

    @Override
    protected Long count() {
        return TaskStatusGenericEntity.count();
    }
}
