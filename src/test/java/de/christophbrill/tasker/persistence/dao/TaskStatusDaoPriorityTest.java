package de.christophbrill.tasker.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusPriorityEntity;
import de.christophbrill.tasker.ui.dto.Priority;
import de.christophbrill.tasker.ui.dto.State;
import io.quarkus.test.junit.QuarkusTest;
import java.util.UUID;

@QuarkusTest
class TaskStatusDaoPriorityTest extends AbstractDaoCRUDTest<TaskStatusPriorityEntity> {

    @Override
    protected TaskStatusPriorityEntity createFixture() {
        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.persistAndFlush();

        TaskStatusPriorityEntity taskstatus = new TaskStatusPriorityEntity();
        taskstatus.task = task;
        taskstatus.state = State.IN_PROGRESS;
        taskstatus.priority = Priority.LOW;
        return taskstatus;
    }

    @Override
    protected void modifyFixture(TaskStatusPriorityEntity taskstatus) {
        taskstatus.priority = Priority.HIGH;
    }

    @Override
    protected TaskStatusPriorityEntity findById(Long id) {
        return TaskStatusPriorityEntity.findById(id);
    }

    @Override
    protected Long count() {
        return TaskStatusPriorityEntity.count();
    }
}
