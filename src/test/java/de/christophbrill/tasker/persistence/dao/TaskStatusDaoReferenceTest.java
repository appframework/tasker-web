package de.christophbrill.tasker.persistence.dao;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusReferenceEntity;
import de.christophbrill.tasker.ui.dto.State;
import de.christophbrill.tasker.ui.dto.TaskStatusReference.ReferenceType;
import io.quarkus.test.junit.QuarkusTest;
import java.util.UUID;

@QuarkusTest
class TaskStatusDaoReferenceTest extends AbstractDaoCRUDTest<TaskStatusReferenceEntity> {

    @Override
    protected TaskStatusReferenceEntity createFixture() {
        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.persistAndFlush();

        TaskEntity reference = new TaskEntity();
        reference.title = UUID.randomUUID().toString();
        reference.persistAndFlush();

        TaskStatusReferenceEntity taskstatus = new TaskStatusReferenceEntity();
        taskstatus.task = task;
        taskstatus.state = State.IN_PROGRESS;
        taskstatus.reference = reference;
        taskstatus.relationType = ReferenceType.RELATES_TO;
        return taskstatus;
    }

    @Override
    protected void modifyFixture(TaskStatusReferenceEntity taskstatus) {
        taskstatus.relationType = ReferenceType.PART_OF;
    }

    @Override
    protected TaskStatusReferenceEntity findById(Long id) {
        return TaskStatusReferenceEntity.findById(id);
    }

    @Override
    protected Long count() {
        return TaskStatusReferenceEntity.count();
    }
}
