package de.christophbrill.tasker.persistence.dao;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.tasker.persistence.model.EmailEntity;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEmailEntity;
import de.christophbrill.tasker.ui.dto.State;
import io.quarkus.test.junit.QuarkusTest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;

@QuarkusTest
class TaskStatusDaoEmailTest extends AbstractDaoCRUDTest<TaskStatusEmailEntity> {

    @Override
    protected TaskStatusEmailEntity createFixture() {
        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.persistAndFlush();

        TaskStatusEmailEntity taskstatus = new TaskStatusEmailEntity();
        taskstatus.task = task;
        taskstatus.state = State.IN_PROGRESS;
        taskstatus.sender = new EmailEntity(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        taskstatus.at = LocalDateTime.now();
        List<EmailEntity> recipients = new ArrayList<>();
        recipients.add(new EmailEntity(null, UUID.randomUUID().toString()));
        taskstatus.recipients = recipients;
        taskstatus.subject = UUID.randomUUID().toString();
        taskstatus.body = UUID.randomUUID().toString();
        taskstatus.messageId = UUID.randomUUID().toString();
        return taskstatus;
    }

    @Override
    protected void modifyFixture(TaskStatusEmailEntity taskstatus) {
        taskstatus.messageId = UUID.randomUUID().toString();
    }

    @Test
    void testFindByMessageId() {
        List<TaskStatusEmailEntity> status = TaskStatusEmailEntity.findByMessageId("12345").list();

        assertThat(status)
                .isNotNull()
                .hasSize(1);

        status = TaskStatusEmailEntity.findByMessageId("125545").list();

        assertThat(status)
                .isNotNull()
                .isEmpty();
    }

    @Test
    void testFindByInReplyTo() {
        List<TaskStatusEmailEntity> status = TaskStatusEmailEntity.findByInReplyTo("23456").list();

        assertThat(status)
                .isNotNull()
                .hasSize(1);

        status = TaskStatusEmailEntity.findByInReplyTo("23457").list();

        assertThat(status)
                .isNotNull()
                .isEmpty();
    }

    @Override
    protected TaskStatusEmailEntity findById(Long id) {
        return TaskStatusEmailEntity.findById(id);
    }

    @Override
    protected Long count() {
        return TaskStatusEmailEntity.count();
    }
}
