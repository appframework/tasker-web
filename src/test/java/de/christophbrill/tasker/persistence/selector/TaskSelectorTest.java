package de.christophbrill.tasker.persistence.selector;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.christophbrill.appframework.persistence.model.DbObject_;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.tasker.persistence.model.LabelEntity;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskEntity_;
import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity_;
import de.christophbrill.tasker.persistence.model.TaskStatusPersonEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusPersonEntity_;
import de.christophbrill.tasker.ui.dto.State;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.ListJoin;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Subquery;
import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@QuarkusTest
class TaskSelectorTest extends AbstractResourceSelectorTest<TaskEntity> {

    @Override
    protected TaskSelector getSelector() {
        return new TaskSelector(em);
    }

    @Test
    @Disabled("Not an acutal test yet, just used for debugging purposes")
    @SuppressWarnings({"unchecked", "rawtypes"})
    void testEvaluateSearch() {
        List<Predicate> predicates = new ArrayList<>();

        CriteriaBuilder builder = mock(CriteriaBuilder.class);

        ListJoin<TaskEntity, LabelEntity> labelJoin = mock(ListJoin.class);

        Root<TaskEntity> from = mock(Root.class);
        when(from.join(TaskEntity_.labels)).thenReturn(labelJoin);
        when(from.get(DbObject_.id)).thenReturn(mock(Path.class));

        Root mock = mock(Root.class);
        when(mock.get(TaskStatusGenericEntity_.task)).thenReturn(mock(Path.class));

        Root mock1 = mock(Root.class);
        when(mock1.get(TaskStatusPersonEntity_.task)).thenReturn(mock(Path.class));

        Subquery subquery = mock(Subquery.class);
        when(subquery.from(TaskStatusGenericEntity.class)).thenReturn(mock);
        when(subquery.from(TaskStatusPersonEntity.class)).thenReturn(mock1);

        CriteriaQuery<?> criteriaQuery = mock(CriteriaQuery.class);
        when(criteriaQuery.subquery(Integer.class)).thenReturn(subquery);

        TaskSelector.evaluateSearch("label:Toast test", predicates, builder, from, criteriaQuery);
    }

    @Test
    @Transactional
    void testWithAssignee() {
        UserEntity user = UserEntity.<UserEntity>find("login", "tom").firstResult();
        assertThat(user).isNotNull();

        List<TaskEntity> before = getSelector().withAssignee(user).findAll();
        assertThat(before)
                .hasSizeGreaterThanOrEqualTo(5)
                .allSatisfy(
                        b -> assertThat(b.assignee).satisfiesAnyOf(c -> assertThat(c).isEqualTo(user), c -> assertThat(c).isNull())
                );
        int sizeBefore = before.size();

        TaskEntity task = new TaskEntity();
        task.title = UUID.randomUUID().toString();
        task.persistAndFlush();

        TaskStatusPersonEntity taskStatusPersonEntity = new TaskStatusPersonEntity();
        taskStatusPersonEntity.assignee = user;
        taskStatusPersonEntity.task = task;
        taskStatusPersonEntity.state = State.COMPLETED;
        taskStatusPersonEntity.persistAndFlush();

        List<TaskEntity> assigned = getSelector().withAssignee(user).findAll();
        assertThat(assigned).hasSize(sizeBefore + 1);

        taskStatusPersonEntity.delete();

        List<TaskEntity> after = getSelector().withAssignee(user).findAll();
        assertThat(after)
                .hasSize(sizeBefore + 1)
                .allSatisfy(
                        b -> assertThat(b.assignee).satisfiesAnyOf(c -> assertThat(c).isEqualTo(user), c -> assertThat(c).isNull())
                );
    }

    @Test
    void testWithSearch() {
        List<TaskEntity> tasks = getSelector().withSearch("label:test").findAll();
        assertThat(tasks)
                .isNotNull()
                .hasSize(2);

        tasks = getSelector().withSearch("label:toast").findAll();
        assertThat(tasks)
                .isNotNull()
                .hasSize(3);

        tasks = getSelector().withSearch("label:toast label:test").findAll();
        assertThat(tasks)
                .isNotNull()
                .hasSize(1);

        tasks = getSelector().withSearch("label:toast status:!NEW").findAll();
        assertThat(tasks)
                .isNotNull()
                .hasSize(2);

        tasks = getSelector().withSearch("label:toast status:IN_PROGRESS").findAll();
        assertThat(tasks)
                .isNotNull()
                .hasSize(1);

        tasks = getSelector().withSearch("assignee:\"Tom Tester\"").findAll();
        assertThat(tasks)
                .isNotNull()
                .hasSize(1);

        tasks = getSelector().withSearch("due:\"2017-10-19 15:00:10\"").findAll();
        assertThat(tasks)
                .isNotNull()
                .hasSize(1);

        tasks = getSelector().withSearch("due:\"!2017-10-19 15:10:00\"").findAll();
        assertThat(tasks)
                .isNotNull()
                .hasSize(1);

        tasks = getSelector().withSearch("due:\"2017-10-19 15:10:00\"").findAll();
        assertThat(tasks)
                .isNotNull()
                .isEmpty();
    }
}
