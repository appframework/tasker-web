package de.christophbrill.tasker.persistence.selector;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.tasker.persistence.model.ImporterConfigurationEntity;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

@QuarkusTest
class ImporterConfigurationSelectorTest extends AbstractResourceSelectorTest<ImporterConfigurationEntity> {

    @Override
    protected ImporterConfigurationSelector getSelector() {
        return new ImporterConfigurationSelector(em);
    }

    @Test
    void testOnlyEnabled() {
        assertThat(getSelector().onlyEnabled().count()).isLessThanOrEqualTo(getSelector().count());

        for (var configuration : getSelector().onlyEnabled().findAll()) {
            assertThat(configuration.disabled).isFalse();
        }
    }
}
