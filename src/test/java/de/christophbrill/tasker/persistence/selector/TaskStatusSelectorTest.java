package de.christophbrill.tasker.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.tasker.persistence.model.TaskStatusEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class TaskStatusSelectorTest extends AbstractResourceSelectorTest<TaskStatusEntity> {

    @Override
    protected TaskStatusSelector getSelector() {
        return new TaskStatusSelector(em);
    }
}
