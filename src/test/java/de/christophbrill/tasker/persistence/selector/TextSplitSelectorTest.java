package de.christophbrill.tasker.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.tasker.persistence.model.TextSplitEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class TextSplitSelectorTest extends AbstractResourceSelectorTest<TextSplitEntity> {

    @Override
    protected TextSplitSelector getSelector() {
        return new TextSplitSelector(em);
    }
}
