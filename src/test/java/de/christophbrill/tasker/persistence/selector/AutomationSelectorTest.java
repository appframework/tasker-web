package de.christophbrill.tasker.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.tasker.persistence.model.AutomationEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class AutomationSelectorTest extends AbstractResourceSelectorTest<AutomationEntity> {

    @Override
    protected AutomationSelector getSelector() {
        return new AutomationSelector(em);
    }
}
