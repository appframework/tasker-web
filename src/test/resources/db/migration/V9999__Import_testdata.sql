TRUNCATE TABLE
    automation,
    binary_data,
    importerconfiguration,
    importerconfiguration_ews,
    importerconfiguration_pop,
    label,
    recipient,
    recipient_cc,
    recipient_bcc,
    role,
    role_permissions,
    task,
    task_label,
    taskstatus,
    taskstatus_action,
    taskstatus_due,
    taskstatus_effort,
    taskstatus_email,
    taskstatus_file,
    taskstatus_generic,
    taskstatus_person,
    taskstatus_priority,
    taskstatus_reference,
    textsplit,
    user_,
    user_role;

INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (1,'Tom Tester','tom','ab4d8d2a5f480a137067da17100271cd176607a1','tom@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (2,'XXX','xxx','b60d121b438a380c343d5ec3c2037564b82ffef3','xxx@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (3,'YYY','yyy','186154712b2d5f6791d85b9a0987b98fa231779c','yyy@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
ALTER SEQUENCE user__SEQ RESTART WITH 4;

INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (1,'Administrators',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (2,'Users',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (3,'No Permissions',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
ALTER SEQUENCE role_SEQ RESTART WITH 4;

INSERT INTO role_permissions(role_id,permission) VALUES (1,'SHOW_AUTOMATIONS'), (1,'ADMIN_AUTOMATIONS'), (1,'SHOW_TEXTSPLITS'), (1,'ADMIN_TEXTSPLITS'), (1,'SHOW_USERS'), (1,'ADMIN_USERS'), (1,'SHOW_ROLES'), (1,'ADMIN_ROLES'), (1, 'SHOW_IMPORTERCONFIGURATION'), (1, 'ADMIN_IMPORTERCONFIGURATION'), (1, 'SHOW_LABELS'), (1, 'ADMIN_LABELS'), (1, 'SHOW_TASKS'), (1, 'ADMIN_TASKS'), (1, 'VIEW_ALL_TASKS');
INSERT INTO role_permissions(role_id,permission) VALUES (2,'SHOW_TASKS');

INSERT INTO user_role(user_id,role_id) VALUES (1,1);
INSERT INTO user_role(user_id,role_id) VALUES (2,2);
INSERT INTO user_role(user_id,role_id) VALUES (3,3);

INSERT INTO task (id, created, modified, title, state, priority, deadline, creator_id) VALUES (1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'My Task', 'NEW', 'MEDIUM', FALSE, 1);
INSERT INTO task (id, created, modified, title, state, priority, deadline, creator_id) VALUES (2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'My Task', 'NEW', 'MEDIUM', FALSE, 1);
INSERT INTO task (id, created, modified, title, state, priority, deadline, creator_id, assignee_id, due) VALUES (3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'My Task', 'COMPLETED', 'MEDIUM', FALSE, 1, 1, '2017-10-19 15:00:10');
INSERT INTO task (id, created, modified, title, state, priority, deadline, creator_id) VALUES (4, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'My Task', 'IN_PROGRESS', 'MEDIUM', FALSE, 1);
INSERT INTO task (id, created, modified, title, state, priority, deadline, creator_id) VALUES (5, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'My Task', 'IN_PROGRESS', 'MEDIUM', FALSE, 1);
ALTER SEQUENCE task_SEQ RESTART WITH 6;

INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (1,  CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 'person', 'NEW', 1);
INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (2,  CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 'person', 'NEW', 1);
INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (3,  CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 2, 'generic', 'NEW', 1);
INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (4,  CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 3, 'generic', 'NEW', 1);
INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (5,  CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 3, 'person', 'IN_PROGRESS', 1);
INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (6,  CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 3, 'person', 'IN_PROGRESS', 1);
INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (7,  CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 3, 'due', 'COMPLETED', 1);
INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (8,  CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4, 'generic', 'NEW', 1);
INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (9,  CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4, 'generic', 'IN_PROGRESS', 1);
INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (10, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4, 'generic', 'NEW', 1);
INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (11, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 5, 'action', 'IN_PROGRESS', 1);
INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (12, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 5, 'email', 'IN_PROGRESS', 1);
INSERT INTO taskstatus (id, created, modified, task_id, type, state, creator_id) VALUES (13, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 5, 'email', 'IN_PROGRESS', 1);
ALTER SEQUENCE taskstatus_SEQ RESTART WITH 14;

INSERT INTO taskstatus_person (taskstatus_id, name) VALUES (1, 'John Doe');
INSERT INTO taskstatus_person (taskstatus_id, name) VALUES (2, 'Jane Doe');
INSERT INTO taskstatus_person (taskstatus_id, assignee_id) VALUES (3, 1);

INSERT INTO taskstatus_generic (taskstatus_id, comment) VALUES (3, 'This is a comment');

INSERT INTO taskstatus_generic (taskstatus_id, comment) VALUES (4, 'This is a comment');
INSERT INTO taskstatus_person (taskstatus_id, name) VALUES (5, 'Jane Doe');
INSERT INTO taskstatus_generic (taskstatus_id, comment) VALUES (6, 'This is a comment');
INSERT INTO taskstatus_due (taskstatus_id, due, deadline) VALUES (7, '2017-10-19 15:00:10', FALSE);

INSERT INTO taskstatus_generic (taskstatus_id, comment) VALUES (8, 'This is a comment');
INSERT INTO taskstatus_generic (taskstatus_id, comment) VALUES (9, 'This is a comment');

INSERT INTO taskstatus_generic (taskstatus_id, comment) VALUES (10, 'This is a comment');
INSERT INTO taskstatus_action (taskstatus_id, title, completed) VALUES (11, 'This is a title', FALSE);
INSERT INTO taskstatus_email (taskstatus_id, sender, "at", subject, body, message_id) VALUES (12, 'from@dev.null', CURRENT_TIMESTAMP, 'Subject', 'Body', '12345');
INSERT INTO recipient (taskstatusemail_id, name, address) VALUES (12, NULL, 'to@dev.null');
INSERT INTO taskstatus_email (taskstatus_id, sender, "at", subject, body, in_reply_to) VALUES (13, 'to@dev.null', CURRENT_TIMESTAMP, 'Re: Subject', 'Body', '23456');
INSERT INTO recipient (taskstatusemail_id, name, address) VALUES (13, NULL, 'from@dev.null');

INSERT INTO label(id, created, modified, name, color, creator_id) VALUES (1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'test', '#00ff00', 1);
INSERT INTO label(id, created, modified, name, color, creator_id) VALUES (2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'toast', '#0000ff', 1);
INSERT INTO label(id, created, modified, name, color, creator_id) VALUES (3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'töst', '#ff0000', 1);
ALTER SEQUENCE label_SEQ RESTART WITH 4;

INSERT INTO task_label(task_id, label_id) VALUES (1, 1);
INSERT INTO task_label(task_id, label_id) VALUES (3, 1);
INSERT INTO task_label(task_id, label_id) VALUES (2, 2);
INSERT INTO task_label(task_id, label_id) VALUES (3, 2);
INSERT INTO task_label(task_id, label_id) VALUES (4, 2);

INSERT INTO importerconfiguration(id, created, modified, type, server, username, password, disabled, max_failed_logins, current_failed_logins, creator_id) VALUES (1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'POP', 'server1', 'username1', 'password1', TRUE, 3, 0, 1);
INSERT INTO importerconfiguration_pop(importerconfiguration_id, port) VALUES (1, 1000);
INSERT INTO importerconfiguration(id, created, modified, type, server, username, password, disabled, max_failed_logins, current_failed_logins, creator_id) VALUES (2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'POP', 'server2', 'username2', 'password2', TRUE, 3, 0, 1);
INSERT INTO importerconfiguration_pop(importerconfiguration_id, port) VALUES (2, 2000);
INSERT INTO importerconfiguration(id, created, modified, type, server, username, password, disabled, max_failed_logins, current_failed_logins, creator_id) VALUES (3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'POP', 'server3', 'username3', 'password3', FALSE, 3, 0, 1);
INSERT INTO importerconfiguration_pop(importerconfiguration_id, port) VALUES (3, 3000);
ALTER SEQUENCE importerconfiguration_SEQ RESTART WITH 4;

INSERT INTO automation (id, qualified_name, expression, text_match) VALUES
                                                                    (1, 'de.christophbrill.tasker.util.automations.WeeklyReminderAutomationProcessor', 'wv++', 'EQUALS_COMPLETE'),
                                                                    (2, 'de.christophbrill.tasker.util.automations.DailyReminderAutomationProcessor', 'wv+', 'EQUALS_COMPLETE'),
                                                                    (3, 'de.christophbrill.tasker.util.automations.ForceCloseAutomationProcessor', 'ze', 'EQUALS_COMPLETE'),
                                                                    (4, 'de.christophbrill.tasker.util.automations.ActionPatternAutomationProcessor', '#((\d+)|(assign|todo|done)"([^"]+)")', 'REGULAR_EXPRESSION');

ALTER SEQUENCE automation_SEQ RESTART WITH 5;
