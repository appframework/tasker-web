ALTER TABLE automation
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE binary_data
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE importerconfiguration
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE importerconfiguration_ews
    ALTER COLUMN importerconfiguration_id TYPE bigint;
ALTER TABLE importerconfiguration_pop
    ALTER COLUMN importerconfiguration_id TYPE bigint;
ALTER TABLE label
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE recipient
    ALTER COLUMN taskstatusemail_id TYPE bigint;
ALTER TABLE recipient_bcc
    ALTER COLUMN taskstatusemail_id TYPE bigint;
ALTER TABLE recipient_cc
    ALTER COLUMN taskstatusemail_id TYPE bigint;
ALTER TABLE role
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE role_permissions
    ALTER COLUMN role_id TYPE bigint;
ALTER TABLE task
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN assignee_id TYPE bigint;
ALTER TABLE task_label
    ALTER COLUMN task_id TYPE bigint,
    ALTER COLUMN label_id TYPE bigint;
ALTER TABLE taskstatus
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN task_id TYPE bigint;
ALTER TABLE taskstatus_action
    ALTER COLUMN taskstatus_id TYPE bigint;
ALTER TABLE taskstatus_due
    ALTER COLUMN taskstatus_id TYPE bigint;
ALTER TABLE taskstatus_effort
    ALTER COLUMN taskstatus_id TYPE bigint;
ALTER TABLE taskstatus_email
    ALTER COLUMN taskstatus_id TYPE bigint;
ALTER TABLE taskstatus_email_attachment
    ALTER COLUMN taskstatus_id TYPE bigint,
    ALTER COLUMN binarydata_id TYPE bigint;
ALTER TABLE taskstatus_file
    ALTER COLUMN taskstatus_id TYPE bigint,
    ALTER COLUMN binarydata_id TYPE bigint;
ALTER TABLE taskstatus_generic
    ALTER COLUMN taskstatus_id TYPE bigint;
ALTER TABLE taskstatus_person
    ALTER COLUMN taskstatus_id TYPE bigint,
    ALTER COLUMN assignee_id TYPE bigint;
ALTER TABLE taskstatus_priority
    ALTER COLUMN taskstatus_id TYPE bigint;
ALTER TABLE taskstatus_reference
    ALTER COLUMN taskstatus_id TYPE bigint,
    ALTER COLUMN reference_task_id TYPE bigint;
ALTER TABLE textsplit
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE user_
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN picture_id TYPE bigint;
ALTER TABLE user_role
    ALTER COLUMN user_id TYPE bigint,
    ALTER COLUMN role_id TYPE bigint;
