CREATE TABLE automation (
  id serial NOT NULL,
  created TIMESTAMP NULL,
  modified TIMESTAMP NULL,
  creator_id INT NULL,
  modificator_id INT NULL,
  qualified_name VARCHAR(255) NOT NULL,
  expression VARCHAR(2048) NOT NULL,
  text_match VARCHAR(30) NOT NULL,
  source VARCHAR(2048),
  compiled bytea,
  PRIMARY KEY(id)
);

ALTER TABLE automation ADD CONSTRAINT FK_automation_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE automation ADD CONSTRAINT FK_automation_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

INSERT INTO automation (qualified_name, expression, text_match) VALUES
    ('de.christophbrill.tasker.util.automations.WeeklyReminderAutomationProcessor', 'wv++', 'EQUALS_COMPLETE'),
    ('de.christophbrill.tasker.util.automations.DailyReminderAutomationProcessor', 'wv+', 'EQUALS_COMPLETE'),
    ('de.christophbrill.tasker.util.automations.ForceCloseAutomationProcessor', 'ze', 'EQUALS_COMPLETE'),
    ('de.christophbrill.tasker.util.automations.ActionPatternAutomationProcessor', '#((\d+)|(assign|todo|done)"([^"]+)")', 'REGULAR_EXPRESSION');