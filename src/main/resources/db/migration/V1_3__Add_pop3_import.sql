CREATE TABLE popimporterconfiguration (
  id serial NOT NULL,
  created TIMESTAMP NULL,
  modified TIMESTAMP NULL,
  creator_id INT NULL,
  modificator_id INT NULL,
  server VARCHAR(255) NOT NULL,
  port INT NOT NULL,
  username VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  PRIMARY KEY(id)
);

CREATE INDEX FK_popimporterconfiguration_creator_IX ON popimporterconfiguration(creator_id);
CREATE INDEX FK_popimporterconfiguration_modificator_IX ON popimporterconfiguration(modificator_id);

ALTER TABLE popimporterconfiguration ADD CONSTRAINT FK_popimporterconfiguration_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE popimporterconfiguration ADD CONSTRAINT FK_popimporterconfiguration_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

ALTER TABLE taskstatus_email
    ADD COLUMN message_id VARCHAR(2048);