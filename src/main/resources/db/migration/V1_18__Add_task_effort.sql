CREATE TABLE taskstatus_effort (
  taskstatus_id INT NOT NULL,
  at TIMESTAMP NOT NULL,
  duration INT NOT NULL,
  PRIMARY KEY (taskstatus_id),
  CONSTRAINT FK_taskstatuseeffort_taskstatus FOREIGN KEY (taskstatus_id) REFERENCES taskstatus (id)
);
