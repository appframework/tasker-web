ALTER TABLE popimporterconfiguration RENAME TO importerconfiguration;

ALTER TABLE importerconfiguration
  ADD COLUMN type VARCHAR(10);
UPDATE importerconfiguration
  SET type = 'POP';
ALTER TABLE importerconfiguration
  ALTER COLUMN type SET NOT NULL;

CREATE TABLE importerconfiguration_pop (
  importerconfiguration_id INT NOT NULL,
  port INT NOT NULL,
  CONSTRAINT FK_importerconfigurationpop_importerconfiguration FOREIGN KEY (importerconfiguration_id) REFERENCES importerconfiguration (id)
);

INSERT INTO importerconfiguration_pop(importerconfiguration_id, port)
  SELECT id, port FROM importerconfiguration;
ALTER TABLE importerconfiguration
  DROP COLUMN port;

CREATE TABLE importerconfiguration_ews (
  importerconfiguration_id INT NOT NULL,
  category VARCHAR(255) NOT NULL,
  proxy_server VARCHAR(255),
  proxy_port INT,
  CONSTRAINT FK_importerconfigurationews_importerconfiguration FOREIGN KEY (importerconfiguration_id) REFERENCES importerconfiguration (id)
);

UPDATE role_permissions
  SET permission = 'SHOW_IMPORTERCONFIGURATION'
  WHERE permission = 'SHOW_POPIMPORTERCONFIGURATION';
UPDATE role_permissions
  SET permission = 'ADMIN_IMPORTERCONFIGURATION'
  WHERE permission = 'ADMIN_POPIMPORTERCONFIGURATION';

ALTER TABLE importerconfiguration
  ADD COLUMN max_failed_logins int DEFAULT 0 NOT NULL;
ALTER TABLE importerconfiguration
  ADD COLUMN current_failed_logins int DEFAULT 0 NOT NULL;
ALTER TABLE importerconfiguration
  ADD COLUMN disabled BOOLEAN DEFAULT FALSE NOT NULL;
ALTER TABLE importerconfiguration
  ADD COLUMN last_import TIMESTAMP;

CREATE TABLE recipient(
  taskstatusemail_id INT NOT NULL,
  name VARCHAR(255),
  address VARCHAR(255) NOT NULL,
  CONSTRAINT FK_recipient_taskstatusemail FOREIGN KEY (taskstatusemail_id) REFERENCES taskstatus_email (taskstatus_id)
);

CREATE TABLE recipient_cc(
  taskstatusemail_id INT NOT NULL,
  name VARCHAR(255),
  address VARCHAR(255) NOT NULL,
  CONSTRAINT FK_recipientcc_taskstatusemail FOREIGN KEY (taskstatusemail_id) REFERENCES taskstatus_email (taskstatus_id)
);

CREATE TABLE recipient_bcc(
  taskstatusemail_id INT NOT NULL,
  name VARCHAR(255),
  address VARCHAR(255) NOT NULL,
  CONSTRAINT FK_recipientbcc_taskstatusemail FOREIGN KEY (taskstatusemail_id) REFERENCES taskstatus_email (taskstatus_id)
);

INSERT INTO recipient(taskstatusemail_id, address)
  SELECT taskstatus_id, recipient FROM taskstatus_email;

ALTER TABLE taskstatus_email
  DROP COLUMN recipient;