CREATE TABLE task (
  id serial NOT NULL,
  created timestamp NULL,
  modified timestamp NULL,
  creator_id INT NULL,
  modificator_id INT NULL,
  title VARCHAR(255) NOT NULL,
  due TIMESTAMP,
  assignee_id INT,
  state VARCHAR(20) NOT NULL,
  PRIMARY KEY(id)
);

ALTER TABLE task ADD CONSTRAINT FK_task_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE task ADD CONSTRAINT FK_task_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);
ALTER TABLE task ADD CONSTRAINT FK_task_assignee FOREIGN KEY (assignee_id) REFERENCES user_ (id);

CREATE TABLE taskstatus (
  id serial NOT NULL,
  created timestamp NULL,
  modified timestamp NULL,
  creator_id INT NULL,
  modificator_id INT NULL,
  task_id INT NOT NULL,
  type VARCHAR(10) NOT NULL,
  state VARCHAR(20) NOT NULL,
  CONSTRAINT FK_taskstatus_task FOREIGN KEY (task_id) REFERENCES task (id),
  PRIMARY KEY(id)
);

ALTER TABLE taskstatus ADD CONSTRAINT FK_taskstatus_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE taskstatus ADD CONSTRAINT FK_taskstatus_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE TABLE taskstatus_email (
  taskstatus_id INT NOT NULL,
  sender VARCHAR(255) NOT NULL,
  "at" timestamp NOT NULL,
  recipient VARCHAR(255) NOT NULL,
  subject VARCHAR(1024),
  body VARCHAR(1000000),
  PRIMARY KEY (taskstatus_id),
  CONSTRAINT FK_taskstatusemail_taskstatus FOREIGN KEY (taskstatus_id) REFERENCES taskstatus (id)
);

CREATE TABLE taskstatus_person (
  taskstatus_id INT NOT NULL,
  name VARCHAR(255),
  assignee_id INT,
  PRIMARY KEY (taskstatus_id),
  CONSTRAINT FK_taskstatuseperson_taskstatus FOREIGN KEY (taskstatus_id) REFERENCES taskstatus (id),
  CONSTRAINT FK_taskstatuseperson_user FOREIGN KEY (assignee_id) REFERENCES user_ (id)
);

CREATE TABLE taskstatus_generic (
  taskstatus_id INT NOT NULL,
  comment VARCHAR(1000000) NOT NULL,
  PRIMARY KEY (taskstatus_id),
  CONSTRAINT FK_taskstatusegeneric_taskstatus FOREIGN KEY (taskstatus_id) REFERENCES taskstatus (id)
);

CREATE TABLE taskstatus_due (
  taskstatus_id INT NOT NULL,
  due TIMESTAMP NOT NULL,
  PRIMARY KEY (taskstatus_id),
  CONSTRAINT FK_taskstatusedue_taskstatus FOREIGN KEY (taskstatus_id) REFERENCES taskstatus (id)
);

CREATE TABLE taskstatus_action (
  taskstatus_id INT NOT NULL,
  title VARCHAR(1024),
  completed BOOLEAN NOT NULL,
  PRIMARY KEY (taskstatus_id),
  CONSTRAINT FK_taskstatusaction_taskstatus FOREIGN KEY (taskstatus_id) REFERENCES taskstatus (id)
);
