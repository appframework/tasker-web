CREATE UNIQUE INDEX email_UNIQUE ON user_(email);
CREATE UNIQUE INDEX login_UNIQUE ON user_(login);

CREATE INDEX FK_rolepermission_role_IX ON role_permissions(role_id);

CREATE INDEX FK_userrole_user_IX ON user_role (user_id);
CREATE INDEX FK_userrole_role_IX ON user_role (role_id);
