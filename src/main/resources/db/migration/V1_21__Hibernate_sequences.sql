create sequence automation_SEQ INCREMENT 50;
select setval('automation_SEQ', (select max(id) from automation));

create sequence binary_data_SEQ INCREMENT 50;
select setval('binary_data_SEQ', (select max(id) from binary_data));

create sequence importerconfiguration_SEQ INCREMENT 50;
select setval('importerconfiguration_SEQ', (select max(id) from importerconfiguration));

create sequence label_SEQ INCREMENT 50;
select setval('label_SEQ', (select max(id) from label));

create sequence role_SEQ INCREMENT 50;
select setval('role_SEQ', (select max(id) from role));

create sequence user__SEQ INCREMENT 50;
select setval('user__SEQ', (select max(id) from user_));

create sequence task_SEQ INCREMENT 50;
select setval('task_SEQ', (select max(id) from task));

create sequence taskstatus_SEQ INCREMENT 50;
select setval('taskstatus_SEQ', (select max(id) from taskstatus));

create sequence textsplit_SEQ INCREMENT 50;
select setval('textsplit_SEQ', (select max(id) from textsplit));

drop sequence if exists hibernate_sequence;