CREATE TABLE taskstatus_priority (
  taskstatus_id INT NOT NULL,
  priority VARCHAR(20) NOT NULL,
  PRIMARY KEY (taskstatus_id),
  CONSTRAINT FK_taskstatusepriority_taskstatus FOREIGN KEY (taskstatus_id) REFERENCES taskstatus (id)
);
CREATE INDEX FK_taskstatuspriority_taskstatus_IX ON taskstatus_priority(taskstatus_id);

-- Add missing indexes
CREATE INDEX FK_taskstatusaction_taskstatus_IX ON taskstatus_due(taskstatus_id);
CREATE INDEX FK_taskstatusdue_taskstatus_IX ON taskstatus_due(taskstatus_id);
CREATE INDEX FK_taskstatusemail_taskstatus_IX ON taskstatus_email(taskstatus_id);
CREATE INDEX FK_taskstatusgeneric_taskstatus_IX ON taskstatus_generic(taskstatus_id);
CREATE INDEX FK_taskstatusperson_taskstatus_IX ON taskstatus_person(taskstatus_id);

ALTER TABLE task
ADD COLUMN priority VARCHAR(20);
UPDATE task SET priority = 'MEDIUM';
ALTER TABLE task
ALTER COLUMN priority SET NOT NULL;
