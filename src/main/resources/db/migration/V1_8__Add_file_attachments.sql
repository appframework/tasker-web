CREATE TABLE taskstatus_file (
  taskstatus_id INT NOT NULL,
  binarydata_id INT NOT NULL,
  PRIMARY KEY (taskstatus_id),
  CONSTRAINT FK_taskstatusfile_taskstatus FOREIGN KEY (taskstatus_id) REFERENCES taskstatus (id),
  CONSTRAINT FK_taskstatusfile_binarydata FOREIGN KEY (binarydata_id) REFERENCES binary_data (id)
);
