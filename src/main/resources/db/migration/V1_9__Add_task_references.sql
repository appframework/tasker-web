CREATE TABLE taskstatus_reference (
  taskstatus_id INT NOT NULL,
  reference_task_id INT NOT NULL,
  relation_type VARCHAR(60) NOT NULL,
  PRIMARY KEY (taskstatus_id),
  CONSTRAINT FK_taskstatusreference_taskstatus FOREIGN KEY (taskstatus_id) REFERENCES taskstatus (id),
  CONSTRAINT FK_taskstatusreference_task FOREIGN KEY (reference_task_id) REFERENCES task (id)
);
