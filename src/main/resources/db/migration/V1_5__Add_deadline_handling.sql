ALTER TABLE taskstatus_due
    ADD COLUMN deadline BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE taskstatus_due SET deadline = TRUE;

ALTER TABLE task
  ADD COLUMN deadline BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE task SET deadline = TRUE WHERE due IS NOT NULL;

INSERT INTO role_permissions(role_id,permission)
  VALUES ((SELECT id FROM role WHERE name = 'Administrators'),'VIEW_ALL_TASKS');