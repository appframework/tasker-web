CREATE TABLE textsplit (
  id serial NOT NULL,
  created TIMESTAMP NULL,
  modified TIMESTAMP NULL,
  creator_id INT NULL,
  modificator_id INT NULL,
  expression VARCHAR(2048) NOT NULL,
  PRIMARY KEY(id)
);

ALTER TABLE textsplit ADD CONSTRAINT FK_textsplit_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE textsplit ADD CONSTRAINT FK_textsplit_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

INSERT INTO textsplit(created, modified, creator_id, modificator_id, expression)
VALUES
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '-- '),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '-----Ursprüngliche Nachricht-----'),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '\n\s*Vielen Dank und Grüße,?\s*\n'),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '\n\s*Liebe Grüße,?\s*\n'),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '\n\s*Danke,?\s*\n'),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '\n\s*Vielen Dank,?\s*\n'),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '\n\s*Kind regards,?\s*\n'),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '\n\s*Thank you,?\s*\n'),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '\n\s*Viele Grüße,?\s*\n'),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '\n\s*Beste Grüße,?\s*\n'),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '\n\s*Schöne Grüße,?\s*\n'),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '\n\s*VG,?\s*\n'),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, '\n\s*LG,?\s*\n'),
       (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1, 1, 'Sent from my iPhone');

INSERT INTO role_permissions(role_id,permission)
 VALUES ((SELECT id FROM role WHERE name = 'Administrators'),'SHOW_TEXTSPLITS'),
        ((SELECT id FROM role WHERE name = 'Administrators'),'ADMIN_TEXTSPLITS');
