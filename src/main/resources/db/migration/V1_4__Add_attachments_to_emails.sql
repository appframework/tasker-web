CREATE TABLE taskstatus_email_attachment (
  taskstatus_id INT NOT NULL,
  binarydata_id INT NOT NULL,
  PRIMARY KEY (taskstatus_id, binarydata_id)
);
