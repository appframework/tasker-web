CREATE TABLE label (
  id serial NOT NULL,
  created TIMESTAMP NULL,
  modified TIMESTAMP NULL,
  creator_id INT NULL,
  modificator_id INT NULL,
  name VARCHAR(255) NOT NULL,
  color VARCHAR(127) NOT NULL,
  PRIMARY KEY(id)
);

ALTER TABLE label ADD CONSTRAINT FK_label_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE label ADD CONSTRAINT FK_label_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE TABLE task_label (
  task_id INT NOT NULL,
  label_id INT NOT NULL,
  PRIMARY KEY (task_id, label_id)
);

ALTER TABLE task_label ADD CONSTRAINT FK_tasklabel_task FOREIGN KEY (task_id) REFERENCES task (id);
ALTER TABLE task_label ADD CONSTRAINT FK_tasklabel_label FOREIGN KEY (label_id) REFERENCES label (id);
