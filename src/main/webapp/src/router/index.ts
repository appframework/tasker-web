import { createRouter, createWebHistory } from 'vue-router'
import { search } from '@/App.vue'
import { isLoggedIn } from '@/store'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue')
    },
    {
      path: '/login',
      component: () => import('../appframework/views/LoginView.vue')
    },
    {
      path: '/logout',
      component: () => import('../appframework/views/LogoutView.vue')
    },
    {
      path: '/roles',
      component: () => import('../appframework/views/RolesView.vue')
    },
    {
      path: '/roles/:id',
      component: () => import('../appframework/views/RoleView.vue')
    },
    {
      path: '/roles/clone/:id',
      component: () => import('../appframework/views/RoleView.vue')
    },
    {
      path: '/users',
      component: () => import('../appframework/views/UsersView.vue')
    },
    {
      path: '/users/:id',
      component: () => import('../appframework/views/UserView.vue')
    },
    {
      path: '/users/clone/:id',
      component: () => import('../appframework/views/UserView.vue')
    },
    {
      path: '/profile',
      component: () => import('../appframework/views/ProfileView.vue')
    },
    {
      path: '/labels',
      component: () => import('../views/LabelsView.vue')
    },
    {
      path: '/labels/:id',
      component: () => import('../views/LabelView.vue')
    },
    {
      path: '/labels/clone/:id',
      component: () => import('../views/LabelView.vue')
    },
    {
      path: '/tasks',
      component: () => import('../views/TasksView.vue')
    },
    {
      path: '/task/chart',
      component: () => import('../views/TaskChartView.vue')
    },
    {
      path: '/tasks/:id',
      component: () => import('../views/TaskView.vue')
    },
    {
      path: '/tasks/clone/:id',
      component: () => import('../views/TaskView.vue')
    },
    {
      path: '/tasks/history/:id',
      component: () => import('../views/TaskHistoryView.vue')
    },
    {
      path: '/automations',
      component: () => import('../views/AutomationsView.vue')
    },
    {
      path: '/automations/:id',
      component: () => import('../views/AutomationView.vue')
    },
    {
      path: '/automations/clone/:id',
      component: () => import('../views/AutomationView.vue')
    },
    {
      path: '/textsplits',
      component: () => import('../views/TextsplitsView.vue')
    },
    {
      path: '/textsplits/:id',
      component: () => import('../views/TextsplitView.vue')
    },
    {
      path: '/textsplits/clone/:id',
      component: () => import('../views/TextsplitView.vue')
    },
    {
      path: '/importerconfigurations',
      component: () => import('../views/ImporterConfigurationsView.vue')
    },
    {
      path: '/importerconfigurations/:id',
      component: () => import('../views/ImporterConfigurationView.vue')
    },
    {
      path: '/importerconfigurations/clone/:id',
      component: () => import('../views/ImporterConfigurationView.vue')
    }
  ]
})

router.beforeEach((to, from) => {
  // Not logged in, go to login page
  if (!isLoggedIn() && to.path !== '/login') {
    return { path: '/login' }
  }

  // Clear search between modules
  if (to.path === from.path) {
    return
  }
  let currentModule = from.path.split('/')[1]
  if (currentModule === '') {
    currentModule = 'tasks'
  }
  let nextModule = to.path.split('/')[1]
  if (nextModule === '') {
    nextModule = 'tasks'
  }
  if (currentModule !== nextModule) {
    search.value = ''
  }
  return true
})

export default router
