/* tslint:disable */
/* eslint-disable */

export interface AbstractDto {
    id: number;
}

export interface BinaryData extends AbstractDto {
    filename: string;
    size: number;
    contentType: string;
}

export interface Credentials {
    username: string;
    password: string;
}

export interface DeletionAction {
    effect: string;
    action: string;
    affected: number;
}

export interface ImportResult {
    skipped: number;
    created: number;
    updated: number;
    deleted: number;
}

export interface Problem {
    title: string;
    status: number;
    detail: string;
}

export interface Progress<T> {
    key: string;
    result: T;
    value: number;
    max: number;
    message: string;
    completed: boolean;
    success: boolean;
}

export interface Role extends AbstractDto {
    name: string;
    permissions: string[];
}

export interface User extends AbstractDto {
    name: string;
    login: string;
    password: string;
    email: string;
    /**
     * @deprecated
     */
    roleIds: number[];
    roles: string[];
    pictureId: number;
}

export interface VersionInformation {
    maven: string;
    git: string;
    buildTimestamp: Date;
}

export interface Assignee {
    assigneeId: number;
    name: string;
}

export interface Automation extends AbstractDto {
    qualifiedName: string;
    expression: string;
    textMatch: MatchType;
    source: string;
    compiled: boolean;
    builtin: boolean;
}

export interface Email {
    name: string;
    address: string;
}

export interface EwsImporterConfiguration extends ImporterConfiguration {
    type: "EWS";
    category: string;
    proxyServer: string;
    proxyPort: number;
}

export interface ImporterConfiguration extends AbstractDto {
    type: "EWS" | "POP";
    server: string;
    username: string;
    password: string;
    maxFailedLogins: number;
    currentFailedLogins: number;
    disabled: boolean;
    lastImport: Date;
}

export interface Label extends AbstractDto {
    name: string;
    color: string;
}

export interface PopImporterConfiguration extends ImporterConfiguration {
    type: "POP";
    port: number;
}

export interface Task extends AbstractDto {
    title: string;
    status: TaskStatusUnion[];
    due: Date;
    deadline: boolean;
    assignee: string;
    state: State;
    labels: Label[];
    priority: Priority;
    referencedBy: TaskStatusReference[];
}

export interface TaskStatus extends AbstractDto {
    type: "action" | "due" | "effort" | "email" | "file" | "generic" | "person" | "priority" | "reference";
    taskId: number;
    state: State;
    created: Date;
    createdBy: string;
}

export interface TaskStatusAction extends TaskStatus {
    type: "action";
    title: string;
    completed: boolean;
}

export interface TaskStatusDue extends TaskStatus {
    type: "due";
    due: Date;
    deadline: boolean;
}

export interface TaskStatusEffort extends TaskStatus {
    type: "effort";
    at: Date;
    duration: number;
}

export interface TaskStatusEmail extends TaskStatus {
    type: "email";
    sender: Email;
    at: Date;
    recipients: Email[];
    recipientsCc: Email[];
    recipientsBcc: Email[];
    subject: string;
    body: string;
    messageId: string;
    inReplyTo: string;
    attachments: BinaryData[];
}

export interface TaskStatusFile extends TaskStatus {
    type: "file";
    file: BinaryData;
}

export interface TaskStatusGeneric extends TaskStatus {
    type: "generic";
    comment: string;
}

export interface TaskStatusPerson extends TaskStatus {
    type: "person";
    name: string;
    assigneeId: number;
}

export interface TaskStatusPriority extends TaskStatus {
    type: "priority";
    priority: Priority;
}

export interface TaskStatusReference extends TaskStatus {
    type: "reference";
    reference: Task;
    relationType: ReferenceType;
}

export interface TextSplit extends AbstractDto {
    expression: string;
}

export type Priority = "HIGH" | "MEDIUM" | "LOW";

export type State = "NEW" | "IN_PROGRESS" | "COMPLETED";

export type MatchType = "EQUALS_COMPLETE" | "REGULAR_EXPRESSION";

export type ReferenceType = "PART_OF" | "RELATES_TO";

export type ImporterConfigurationUnion = EwsImporterConfiguration | PopImporterConfiguration;

export type TaskStatusUnion = TaskStatusAction | TaskStatusDue | TaskStatusGeneric | TaskStatusPerson | TaskStatusEmail | TaskStatusPriority | TaskStatusFile | TaskStatusReference | TaskStatusEffort;
