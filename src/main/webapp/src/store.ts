import { createGlobalState, useStorage } from '@vueuse/core'

export const appState = createGlobalState(() => {
  return useStorage(
    'state-tasker',
    {
      user: {
        username: '',
        authdata: ''
      },
      permissions: [] as string[],
      locale: navigator.language.split('-')[0]
    },
    localStorage,
    { mergeDefaults: true }
  )
})

export const allPermissions = createGlobalState(async () => {
  const state = appState()
  const response = await fetch('/rest/permissions', {
    headers: {
      Authorization: 'Basic ' + state.value.user.authdata
    }
  })
  return (await response.json()) as string[]
})

export const isLoggedIn = function () {
  const state = appState()
  return !!state.value.user.username
}
