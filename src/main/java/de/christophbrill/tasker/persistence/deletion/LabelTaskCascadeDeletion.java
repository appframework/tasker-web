package de.christophbrill.tasker.persistence.deletion;

import de.christophbrill.appframework.persistence.deletion.CascadeDeletion;
import de.christophbrill.appframework.ui.dto.DeletionAction;
import de.christophbrill.tasker.persistence.model.LabelEntity;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import jakarta.persistence.EntityManager;
import java.util.List;

public class LabelTaskCascadeDeletion implements CascadeDeletion<LabelEntity> {

    @Override
    public int affectedByDeletion(EntityManager em, LabelEntity label) {
        return em
            .createQuery("select count(t) from Task t where :label in elements(t.labels)", Number.class)
            .setParameter("label", label)
            .getSingleResult()
            .intValue();
    }

    @Override
    public int delete(EntityManager em, LabelEntity label) {
        List<TaskEntity> tasks = em
            .createQuery("select t from Task t where :label in elements(t.labels)", TaskEntity.class)
            .setParameter("label", label)
            .getResultList();
        for (TaskEntity task : tasks) {
            task.labels.remove(label);
        }
        return tasks.size();
    }

    @Override
    public DeletionAction getAction() {
        return new DeletionAction("Tasks using the label", "will loose the label");
    }
}
