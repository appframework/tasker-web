package de.christophbrill.tasker.persistence.listener;

import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusPersonEntity;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreRemove;

public class TaskStatusPersonListener {

    @PrePersist
    public void prePersist(TaskStatusPersonEntity taskStatusPerson) {
        taskStatusPerson.task.assignee = taskStatusPerson.assignee;
        taskStatusPerson.task.assigneeName = taskStatusPerson.name;
    }

    @PreRemove
    public void preRemove(TaskStatusPersonEntity taskStatusPerson) {
        TaskEntity task = taskStatusPerson.task;
        task.assignee = null;
        for (TaskStatusEntity taskStatus : task.status) {
            if (
                taskStatus instanceof TaskStatusPersonEntity taskStatusPerson_ &&
                !taskStatus.id.equals(taskStatusPerson.id)
            ) {
                task.assignee = taskStatusPerson_.assignee;
                task.assigneeName = taskStatusPerson_.name;
            }
        }
    }
}
