package de.christophbrill.tasker.persistence.listener;

import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusDueEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEntity;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreRemove;

public class TaskStatusDueListener {

    @PrePersist
    public void prePersist(TaskStatusDueEntity taskStatusDue) {
        taskStatusDue.task.due = taskStatusDue.due;
        taskStatusDue.task.deadline = taskStatusDue.deadline;
    }

    @PreRemove
    public void preRemove(TaskStatusDueEntity taskStatusDue) {
        TaskEntity task = taskStatusDue.task;
        task.due = null;
        for (TaskStatusEntity taskStatus : task.status) {
            if (taskStatus instanceof TaskStatusDueEntity taskStatusDue_ && !taskStatus.id.equals(taskStatusDue.id)) {
                task.due = taskStatusDue_.due;
                task.deadline = taskStatusDue_.deadline;
            }
        }
    }
}
