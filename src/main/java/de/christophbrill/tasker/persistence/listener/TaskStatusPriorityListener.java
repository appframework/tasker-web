package de.christophbrill.tasker.persistence.listener;

import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusPriorityEntity;
import de.christophbrill.tasker.ui.dto.Priority;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreRemove;

public class TaskStatusPriorityListener {

    @PrePersist
    public void prePersist(TaskStatusPriorityEntity taskStatusPriority) {
        taskStatusPriority.task.priority = taskStatusPriority.priority;
    }

    @PreRemove
    public void preRemove(TaskStatusPriorityEntity taskStatusPriority) {
        TaskEntity task = taskStatusPriority.task;
        task.priority = Priority.MEDIUM;
        for (TaskStatusEntity taskStatus : task.status) {
            if (
                taskStatus instanceof TaskStatusPriorityEntity taskStatusPriority_ &&
                !taskStatus.id.equals(taskStatusPriority.id)
            ) {
                task.priority = taskStatusPriority_.priority;
            }
        }
    }
}
