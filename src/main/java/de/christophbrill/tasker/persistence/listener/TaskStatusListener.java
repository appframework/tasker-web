package de.christophbrill.tasker.persistence.listener;

import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEntity;
import de.christophbrill.tasker.ui.dto.State;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreRemove;

public class TaskStatusListener {

    @PrePersist
    public void prePersist(TaskStatusEntity taskStatus) {
        taskStatus.task.state = taskStatus.state;
    }

    @PreRemove
    public void preRemove(TaskStatusEntity taskStatus) {
        TaskEntity task = taskStatus.task;
        task.state = State.NEW;
        for (TaskStatusEntity s : task.status) {
            if (!s.id.equals(taskStatus.id)) {
                task.state = s.state;
            }
        }
    }
}
