package de.christophbrill.tasker.persistence.selector;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.tasker.persistence.model.TaskEntity_;
import de.christophbrill.tasker.persistence.model.TaskStatusEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEntity_;
import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class TaskStatusSelector extends AbstractResourceSelector<TaskStatusEntity> {

    private String search;

    public TaskStatusSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(
        @Nonnull CriteriaBuilder builder,
        @Nonnull Root<TaskStatusEntity> from,
        @Nonnull CriteriaQuery<?> criteriaQuery
    ) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.like(from.get(TaskStatusEntity_.task).get(TaskEntity_.title), '%' + search + '%'));
        }

        return predicates;
    }

    @Override
    public TaskStatusSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<TaskStatusEntity> getEntityClass() {
        return TaskStatusEntity.class;
    }
}
