package de.christophbrill.tasker.persistence.selector;

import de.christophbrill.appframework.persistence.model.DbObject_;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.persistence.model.UserEntity_;
import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.tasker.persistence.model.LabelEntity;
import de.christophbrill.tasker.persistence.model.LabelEntity_;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskEntity_;
import de.christophbrill.tasker.persistence.model.TaskStatusEmailEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEmailEntity_;
import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity_;
import de.christophbrill.tasker.persistence.model.TaskStatusPersonEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusPersonEntity_;
import de.christophbrill.tasker.ui.dto.State;
import io.quarkus.hibernate.orm.panache.PanacheEntity_;
import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CommonAbstractCriteria;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.ListJoin;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Subquery;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public class TaskSelector extends AbstractResourceSelector<TaskEntity> {

    private static final String TYPE_LABEL = "label";
    private static final String TYPE_ASSIGNEE = "assignee";
    private static final String TYPE_STATUS = "status";
    private static final String TYPE_PRIORITY = "priority";
    private static final String TYPE_DUE = "due";
    private static final Pattern PATTERN_SEARCH = Pattern.compile(
        '(' +
        TYPE_LABEL +
        '|' +
        TYPE_ASSIGNEE +
        '|' +
        TYPE_STATUS +
        '|' +
        TYPE_PRIORITY +
        '|' +
        TYPE_DUE +
        "):\\s*(\"([^\"]+)\"|([^\\s]+))"
    );

    private String search;
    private UserEntity assignee;
    private LocalDateTime followupBeforeOrDeadline;
    private UserEntity withoutAssignee;
    private State excludeState;
    private UserEntity visibleTo;

    public TaskSelector(EntityManager em) {
        super(em);
    }

    @Nonnull
    @Override
    protected List<Predicate> generatePredicateList(
        @Nonnull CriteriaBuilder builder,
        @Nonnull Root<TaskEntity> from,
        @Nonnull CriteriaQuery<?> criteriaQuery
    ) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            if (search.contains(":")) {
                evaluateSearch(search, predicates, builder, from, criteriaQuery);
            } else {
                applySearch(search, predicates, builder, from, criteriaQuery);
            }
        }

        if (assignee != null) {
            predicates.add(
                builder.or(
                    builder.and(
                        builder.isNull(from.get(TaskEntity_.assignee)),
                        builder.isNull(from.get(TaskEntity_.assigneeName))
                    ),
                    builder.equal(from.get(TaskEntity_.assignee), assignee),
                    builder.equal(from.get(TaskEntity_.assigneeName), assignee.name)
                )
            );
        }

        if (withoutAssignee != null) {
            predicates.add(
                builder.and(
                    builder.or(
                        builder.isNotNull(from.get(TaskEntity_.assignee)),
                        builder.isNotNull(from.get(TaskEntity_.assigneeName))
                    ),
                    builder.or(
                        builder.isNull(from.get(TaskEntity_.assignee)),
                        builder.notEqual(from.get(TaskEntity_.assignee), withoutAssignee)
                    ),
                    builder.or(
                        builder.isNull(from.get(TaskEntity_.assigneeName)),
                        builder.notEqual(from.get(TaskEntity_.assigneeName), withoutAssignee.name)
                    )
                )
            );
        }

        if (followupBeforeOrDeadline != null) {
            predicates.add(
                builder.or(
                    builder.equal(from.get(TaskEntity_.deadline), true),
                    builder.and(
                        builder.equal(from.get(TaskEntity_.deadline), false),
                        builder.lessThanOrEqualTo(from.get(TaskEntity_.due), followupBeforeOrDeadline)
                    ),
                    builder.isNull(from.get(TaskEntity_.due))
                )
            );
        }

        if (excludeState != null) {
            predicates.add(builder.notEqual(from.get(TaskEntity_.state), excludeState));
        }

        if (visibleTo != null) {
            // TODO breaks selector.count() by returning to many lines
            criteriaQuery.distinct(true);
            ListJoin<TaskEntity, TaskStatusPersonEntity> treat = builder.treat(
                from.join(TaskEntity_.status),
                TaskStatusPersonEntity.class
            );
            predicates.add(builder.equal(treat.get(TaskStatusPersonEntity_.assignee), visibleTo));
        }

        return predicates;
    }

    private static void applySearch(
        String search,
        List<Predicate> predicates,
        @Nonnull CriteriaBuilder builder,
        @Nonnull Root<TaskEntity> from,
        @Nonnull CriteriaQuery<?> criteriaQuery
    ) {
        Subquery<Long> subqueryGenericComment = criteriaQuery.subquery(Long.class);
        {
            Root<TaskStatusGenericEntity> subfrom = subqueryGenericComment.from(TaskStatusGenericEntity.class);
            subqueryGenericComment.select(subfrom.get(TaskStatusGenericEntity_.task).get(DbObject_.id));
            subqueryGenericComment.where(
                builder.like(
                    builder.lower(subfrom.get(TaskStatusGenericEntity_.comment)),
                    '%' + search.toLowerCase() + '%'
                )
            );
        }

        // Query for assignee subtasks
        Subquery<Long> subqueryPersonName = criteriaQuery.subquery(Long.class);
        {
            Root<TaskStatusPersonEntity> subfrom = subqueryPersonName.from(TaskStatusPersonEntity.class);
            subqueryPersonName.select(subfrom.get(TaskStatusGenericEntity_.task).get(DbObject_.id));
            subqueryPersonName.where(
                builder.like(builder.lower(subfrom.get(TaskStatusPersonEntity_.name)), '%' + search.toLowerCase() + '%')
            );
        }

        // Query for email subtasks
        Subquery<Long> subqueryEmailContent = criteriaQuery.subquery(Long.class);
        {
            Root<TaskStatusEmailEntity> subfrom = subqueryEmailContent.from(TaskStatusEmailEntity.class);
            subqueryEmailContent.select(subfrom.get(TaskStatusEmailEntity_.task).get(DbObject_.id));
            subqueryEmailContent.where(
                builder.like(builder.lower(subfrom.get(TaskStatusEmailEntity_.body)), '%' + search.toLowerCase() + '%')
            );
        }

        predicates.add(
            builder.or(
                builder.like(builder.lower(from.get(TaskEntity_.title)), '%' + search.toLowerCase() + '%'),
                from.get(DbObject_.id).in(subqueryGenericComment),
                from.get(DbObject_.id).in(subqueryPersonName),
                from.get(DbObject_.id).in(subqueryEmailContent)
            )
        );
    }

    static void evaluateSearch(
        String search,
        List<Predicate> predicates,
        CriteriaBuilder builder,
        Root<TaskEntity> from,
        CriteriaQuery<?> criteriaQuery
    ) {
        Matcher matcher = PATTERN_SEARCH.matcher(search);
        boolean found = false;
        int lastIndex = -1;
        while (matcher.find()) {
            found = true;
            String type = matcher.group(1);
            String term = matcher.group(3);
            if (term == null) {
                term = matcher.group(4);
            }
            term = term.toLowerCase();
            if (lastIndex + 1 < matcher.start()) {
                applySearch(
                    search.substring(lastIndex + 1, matcher.start()).trim(),
                    predicates,
                    builder,
                    from,
                    criteriaQuery
                );
            }
            switch (type) {
                case TYPE_LABEL -> {
                    if (term.startsWith("!")) {
                        Subquery<Long> subqueryLabel = subqueryLabel(builder, criteriaQuery, '%' + term.substring(1) + '%');
                        predicates.add(builder.not(from.get(PanacheEntity_.id).in(subqueryLabel)));
                    } else {
                        Subquery<Long> subqueryLabel = subqueryLabel(builder, criteriaQuery, '%' + term + '%');
                        predicates.add(from.get(PanacheEntity_.id).in(subqueryLabel));
                    }
                }
                case TYPE_ASSIGNEE -> predicates.add(
                    builder.or(
                        termToStringPredicate(
                            builder,
                            term,
                            from.join(TaskEntity_.assignee, JoinType.LEFT).get(UserEntity_.name)
                        ),
                        termToStringPredicate(builder, term, from.get(TaskEntity_.assigneeName))
                    )
                );
                case TYPE_STATUS -> predicates.add(
                    termToStringPredicate(builder, term, from.get(TaskEntity_.state).as(String.class))
                );
                case TYPE_PRIORITY -> predicates.add(
                    termToStringPredicate(builder, term, from.get(TaskEntity_.priority).as(String.class))
                );
                case TYPE_DUE -> predicates.add(
                    termToDatePredicate(builder, term, from.get(TaskEntity_.due))
                );
                default -> Log.errorv("Matcher found illegal group {0}, skipping", type);
            }
            lastIndex = matcher.end();
        }
        if (lastIndex + 1 < search.length()) {
            applySearch(search.substring(lastIndex + 1), predicates, builder, from, criteriaQuery);
            found = true;
        }
        if (!found) {
            applySearch(search, predicates, builder, from, criteriaQuery);
        }
    }

    private static Predicate termToDatePredicate(CriteriaBuilder builder, String term, Expression<LocalDateTime> expression) {
        term = term.replaceFirst(" ", "T");
        if (term.contains("T")) {
            if (term.startsWith("!")) {
                return builder.notEqual(expression, LocalDateTime.parse(term.substring(1)));
            } else {
                return builder.equal(expression, LocalDateTime.parse(term));
            }
        } else {
            if (term.startsWith("!")) {
                return builder.notEqual(expression, LocalDate.parse(term.substring(1)));
            } else {
                return builder.equal(expression, LocalDate.parse(term));
            }
        }
    }

    private static Predicate termToStringPredicate(CriteriaBuilder builder, String term, Expression<String> expression) {
        if (term.startsWith("!")) {
            return builder.notLike(builder.lower(expression), '%' + term.substring(1) + '%');
        } else {
            return builder.like(builder.lower(expression), '%' + term + '%');
        }
    }

    private static Subquery<Long> subqueryLabel(CriteriaBuilder builder, CommonAbstractCriteria query,
                                                String labelLike) {
        Subquery<Long> subqueryLabel = query.subquery(Long.class);
        Root<TaskEntity> subfrom = subqueryLabel.from(TaskEntity.class);
        ListJoin<TaskEntity, LabelEntity> fromAbsences = subfrom.join(TaskEntity_.labels, JoinType.LEFT);
        subqueryLabel.select(subfrom.get(PanacheEntity_.id));
        subqueryLabel.where(
                builder.like(builder.lower(fromAbsences.get(LabelEntity_.name)), labelLike)
        );
        return subqueryLabel;
    }


    @Override
    public TaskSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Nonnull
    @Override
    protected Class<TaskEntity> getEntityClass() {
        return TaskEntity.class;
    }

    public TaskSelector withAssignee(UserEntity assignee) {
        this.assignee = assignee;
        return this;
    }

    public TaskSelector withFollowupBeforeOrDeadline(LocalDateTime followupBeforeOrDeadline) {
        this.followupBeforeOrDeadline = followupBeforeOrDeadline;
        return this;
    }

    public TaskSelector withoutAssignee(UserEntity withoutAssignee) {
        this.withoutAssignee = withoutAssignee;
        return this;
    }

    public TaskSelector withoutState(State excludeState) {
        this.excludeState = excludeState;
        return this;
    }

    public TaskSelector withVisibleTo(UserEntity visibleTo) {
        this.visibleTo = visibleTo;
        return this;
    }
}
