package de.christophbrill.tasker.persistence.selector;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.tasker.persistence.model.TextSplitEntity;
import de.christophbrill.tasker.persistence.model.TextSplitEntity_;
import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class TextSplitSelector extends AbstractResourceSelector<TextSplitEntity> {

    private String search;

    public TextSplitSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(
        @Nonnull CriteriaBuilder builder,
        @Nonnull Root<TextSplitEntity> from,
        @Nonnull CriteriaQuery<?> criteriaQuery
    ) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.like(from.get(TextSplitEntity_.expression), '%' + search + '%'));
        }

        return predicates;
    }

    @Override
    public TextSplitSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<TextSplitEntity> getEntityClass() {
        return TextSplitEntity.class;
    }
}
