package de.christophbrill.tasker.persistence.selector;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.tasker.persistence.model.ImporterConfigurationEntity;
import de.christophbrill.tasker.persistence.model.ImporterConfigurationEntity_;
import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class ImporterConfigurationSelector extends AbstractResourceSelector<ImporterConfigurationEntity> {

    private String search;
    private Boolean disabled;

    public ImporterConfigurationSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(
        @Nonnull CriteriaBuilder builder,
        @Nonnull Root<ImporterConfigurationEntity> from,
        @Nonnull CriteriaQuery<?> criteriaQuery
    ) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.like(from.get(ImporterConfigurationEntity_.username), '%' + search + '%'));
        }

        if (disabled != null) {
            predicates.add(builder.equal(from.get(ImporterConfigurationEntity_.disabled), disabled));
        }

        return predicates;
    }

    @Override
    public ImporterConfigurationSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<ImporterConfigurationEntity> getEntityClass() {
        return ImporterConfigurationEntity.class;
    }

    public ImporterConfigurationSelector onlyEnabled() {
        disabled = false;
        return this;
    }
}
