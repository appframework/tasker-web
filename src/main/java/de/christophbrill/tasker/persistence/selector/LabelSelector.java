package de.christophbrill.tasker.persistence.selector;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.tasker.persistence.model.LabelEntity;
import de.christophbrill.tasker.persistence.model.LabelEntity_;
import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class LabelSelector extends AbstractResourceSelector<LabelEntity> {

    private String search;

    public LabelSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(
        @Nonnull CriteriaBuilder builder,
        @Nonnull Root<LabelEntity> from,
        @Nonnull CriteriaQuery<?> criteriaQuery
    ) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.like(builder.lower(from.get(LabelEntity_.name)), '%' + search.toLowerCase() + '%'));
        }

        return predicates;
    }

    @Override
    public LabelSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<LabelEntity> getEntityClass() {
        return LabelEntity.class;
    }
}
