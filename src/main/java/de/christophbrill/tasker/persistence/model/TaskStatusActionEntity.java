package de.christophbrill.tasker.persistence.model;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Parameters;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import java.io.Serial;

@Entity(name = "TaskStatusAction")
@Table(name = "taskstatus_action")
@DiscriminatorValue("action")
@PrimaryKeyJoinColumn(name = "taskstatus_id")
public class TaskStatusActionEntity extends TaskStatusEntity {

    @Serial
    private static final long serialVersionUID = -3741539378438453224L;

    public String title;
    public boolean completed;

    public static PanacheQuery<TaskStatusActionEntity> findByTaskAndTitle(long taskId, String title) {
        return find("task.id = :id and title = :title", Parameters.with("id", taskId).and("title", title));
    }
}
