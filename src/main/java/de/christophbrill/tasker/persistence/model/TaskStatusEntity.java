package de.christophbrill.tasker.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.tasker.persistence.listener.TaskStatusListener;
import de.christophbrill.tasker.ui.dto.State;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.DiscriminatorType;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.io.Serial;
import java.util.List;
import java.util.Locale;

@Entity(name = "TaskStatus")
@Table(name = "taskstatus")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING, length = 10)
@EntityListeners(TaskStatusListener.class)
public abstract class TaskStatusEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = -3767497350080122327L;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "task_id", nullable = false)
    @NotNull
    public TaskEntity task;

    @Enumerated(EnumType.STRING)
    public State state = State.IN_PROGRESS;

    public static List<Object[]> getAssignees(String search) {
        return getEntityManager()
            .createNativeQuery(
                "select name, id, occurences " +
                "from ( " +
                "select name, null as id, count(*) as occurences " +
                "from taskstatus_person " +
                "where lower(name) like :name " +
                "group by name " +
                "union " +
                "select user_.name, user_.id, count(tp.taskstatus_id) as occurences " +
                "from user_ " +
                "left join taskstatus_person tp on tp.assignee_id = user_.id " +
                "where lower(user_.name) like :name " +
                "or lower(user_.login) like :name " +
                "or lower(user_.email) like :name " +
                "group by user_.name, user_.id " +
                ") data " +
                "order by occurences desc",
                Object[].class
            )
            .setParameter("name", '%' + search.toLowerCase(Locale.ROOT) + '%')
            .getResultList();
    }
}
