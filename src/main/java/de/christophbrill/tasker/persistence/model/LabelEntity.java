package de.christophbrill.tasker.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import java.io.Serial;

@Entity(name = "Label")
@Table(name = "label")
public class LabelEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = -4425203580968124878L;

    public String name;
    public String color;
}
