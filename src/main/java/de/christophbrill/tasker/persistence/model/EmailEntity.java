package de.christophbrill.tasker.persistence.model;

import jakarta.persistence.Embeddable;
import java.io.Serial;
import java.io.Serializable;

@Embeddable
public class EmailEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -9086029627116339806L;

    public String name;
    public String address;

    public EmailEntity() {}

    public EmailEntity(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
