package de.christophbrill.tasker.persistence.model;

import de.christophbrill.tasker.ui.dto.TaskStatusReference.ReferenceType;
import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.io.Serial;

@Entity(name = "TaskStatusReference")
@Table(name = "taskstatus_reference")
@DiscriminatorValue("reference")
@PrimaryKeyJoinColumn(name = "taskstatus_id")
public class TaskStatusReferenceEntity extends TaskStatusEntity {

    @Serial
    private static final long serialVersionUID = -8997382743807046536L;

    @ManyToOne(optional = false)
    @JoinColumn(name = "reference_task_id", nullable = false)
    @NotNull
    public TaskEntity reference;

    @Enumerated(EnumType.STRING)
    @Column(name = "relation_type")
    public ReferenceType relationType;
}
