package de.christophbrill.tasker.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.tasker.ui.dto.Automation.MatchType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import java.io.Serial;

@Entity(name = "Automation")
@Table(name = "automation")
public class AutomationEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = -6412540387773855214L;

    @Column(name = "qualified_name")
    public String qualifiedName;

    public String expression;

    @Column(name = "text_match", nullable = false)
    @Enumerated(EnumType.STRING)
    public MatchType textMatch;

    public String source;
    public byte[] compiled;
}
