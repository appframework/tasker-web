package de.christophbrill.tasker.persistence.model;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.tasker.persistence.listener.TaskStatusPersonListener;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import java.io.Serial;

@Entity(name = "TaskStatusPerson")
@Table(name = "taskstatus_person")
@DiscriminatorValue("person")
@PrimaryKeyJoinColumn(name = "taskstatus_id")
@EntityListeners(TaskStatusPersonListener.class)
public class TaskStatusPersonEntity extends TaskStatusEntity {

    @Serial
    private static final long serialVersionUID = -5263241985073474128L;

    public String name;

    @ManyToOne
    @JoinColumn(name = "assignee_id")
    public UserEntity assignee;
}
