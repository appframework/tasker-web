package de.christophbrill.tasker.persistence.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import java.io.Serial;
import java.time.LocalDateTime;

@Entity(name = "TaskStatusEffort")
@Table(name = "taskstatus_effort")
@DiscriminatorValue("effort")
@PrimaryKeyJoinColumn(name = "taskstatus_id")
public class TaskStatusEffortEntity extends TaskStatusEntity {

    @Serial
    private static final long serialVersionUID = -7504298086777039771L;

    public LocalDateTime at;
    /** In minutes */
    public int duration;
}
