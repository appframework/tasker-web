package de.christophbrill.tasker.persistence.model;

import de.christophbrill.tasker.persistence.listener.TaskStatusPriorityListener;
import de.christophbrill.tasker.ui.dto.Priority;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import java.io.Serial;

@Entity(name = "TaskStatusPriority")
@Table(name = "taskstatus_priority")
@DiscriminatorValue("priority")
@PrimaryKeyJoinColumn(name = "taskstatus_id")
@EntityListeners(TaskStatusPriorityListener.class)
public class TaskStatusPriorityEntity extends TaskStatusEntity {

    @Serial
    private static final long serialVersionUID = 4357724935730457525L;

    @Enumerated(EnumType.STRING)
    public Priority priority;
}
