package de.christophbrill.tasker.persistence.model;

import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.io.Serial;

@Entity(name = "TaskStatusFile")
@Table(name = "taskstatus_file")
@DiscriminatorValue("file")
@PrimaryKeyJoinColumn(name = "taskstatus_id")
public class TaskStatusFileEntity extends TaskStatusEntity {

    @Serial
    private static final long serialVersionUID = 6068801033065801268L;

    @ManyToOne(optional = false)
    @JoinColumn(name = "binarydata_id", nullable = false)
    @NotNull
    public BinaryDataEntity file;
}
