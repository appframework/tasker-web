package de.christophbrill.tasker.persistence.model;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.io.Serial;

@Entity(name = "TaskStatusGeneric")
@Table(name = "taskstatus_generic")
@DiscriminatorValue("generic")
@PrimaryKeyJoinColumn(name = "taskstatus_id")
public class TaskStatusGenericEntity extends TaskStatusEntity {

    @Serial
    private static final long serialVersionUID = -3734646268314582458L;

    @NotNull
    @Column(nullable = false, length = 1_000_000)
    public String comment;
}
