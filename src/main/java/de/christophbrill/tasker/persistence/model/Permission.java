/*
 * Copyright 2013  Christoph Brill <christophbrill@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.christophbrill.tasker.persistence.model;

/**
 * @author Christoph Brill &lt;christophbrill@gmail.com&gt;
 */
public enum Permission {
    /** Users, Roles */
    SHOW_USERS,
    ADMIN_USERS,
    SHOW_ROLES,
    ADMIN_ROLES,
    /** Labels */
    SHOW_LABELS,
    ADMIN_LABELS,
    SHOW_IMPORTERCONFIGURATIONS,
    ADMIN_IMPORTERCONFIGURATIONS,
    SHOW_TASKS,
    ADMIN_TASKS,
    /** View all tasks */
    VIEW_ALL_TASKS,
    /** Textsplits */
    ADMIN_TEXTSPLITS,
    SHOW_TEXTSPLITS,
    ADMIN_AUTOMATIONS,
    SHOW_AUTOMATIONS,
}
