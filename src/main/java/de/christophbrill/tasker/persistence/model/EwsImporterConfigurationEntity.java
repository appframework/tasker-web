package de.christophbrill.tasker.persistence.model;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.io.Serial;

@Entity(name = "EwsImporterConfiguration")
@Table(name = "importerconfiguration_ews")
@DiscriminatorValue("EWS")
@PrimaryKeyJoinColumn(name = "importerconfiguration_id")
public class EwsImporterConfigurationEntity extends ImporterConfigurationEntity {

    @Serial
    private static final long serialVersionUID = 2291379818574470094L;

    @NotNull
    public String category;

    @Column(name = "proxy_server")
    public String proxyServer;

    @Column(name = "proxy_port")
    public Integer proxyPort;
}
