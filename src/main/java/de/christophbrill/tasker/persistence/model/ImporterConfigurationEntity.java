package de.christophbrill.tasker.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.DiscriminatorType;
import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.io.Serial;
import java.time.LocalDateTime;

@Entity(name = "ImporterConfiguration")
@Table(name = "importerconfiguration")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING, length = 10)
public abstract class ImporterConfigurationEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = 5654004887249016251L;

    public String server;

    @NotNull
    public String username;

    @NotNull
    public String password;

    @Column(name = "max_failed_logins")
    public int maxFailedLogins;

    @Column(name = "current_failed_logins")
    public int currentFailedLogins;

    public boolean disabled;

    @Column(name = "last_import")
    public LocalDateTime lastImport;
}
