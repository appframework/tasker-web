package de.christophbrill.tasker.persistence.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import java.io.Serial;

@Entity(name = "PopImporterConfiguration")
@Table(name = "importerconfiguration_pop")
@DiscriminatorValue("POP")
@PrimaryKeyJoinColumn(name = "importerconfiguration_id")
public class PopImporterConfigurationEntity extends ImporterConfigurationEntity {

    @Serial
    private static final long serialVersionUID = 3817833393478757827L;

    public int port;
}
