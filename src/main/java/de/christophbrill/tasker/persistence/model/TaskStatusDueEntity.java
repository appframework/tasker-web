package de.christophbrill.tasker.persistence.model;

import de.christophbrill.tasker.persistence.listener.TaskStatusDueListener;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import java.io.Serial;
import java.time.LocalDateTime;

@Entity(name = "TaskStatusDue")
@Table(name = "taskstatus_due")
@DiscriminatorValue("due")
@PrimaryKeyJoinColumn(name = "taskstatus_id")
@EntityListeners(TaskStatusDueListener.class)
public class TaskStatusDueEntity extends TaskStatusEntity {

    @Serial
    private static final long serialVersionUID = -8573289884332059895L;

    public LocalDateTime due;
    public boolean deadline;
}
