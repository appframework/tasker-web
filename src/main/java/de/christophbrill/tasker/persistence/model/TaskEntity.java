package de.christophbrill.tasker.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.tasker.ui.dto.Priority;
import de.christophbrill.tasker.ui.dto.State;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.io.Serial;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Task")
@Table(name = "task")
public class TaskEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = 534156721347300593L;

    public String title;

    @OneToMany(mappedBy = "task", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @OrderBy("created")
    public List<TaskStatusEntity> status = new ArrayList<>(0);

    // Cache of latest state
    public LocalDateTime due;
    public boolean deadline;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "assignee_id")
    public UserEntity assignee;

    @Column(name = "assignee_name")
    public String assigneeName;

    @NotNull
    @Enumerated(EnumType.STRING)
    public State state = State.NEW;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "task_label",
        joinColumns = @JoinColumn(name = "task_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "label_id", referencedColumnName = "id")
    )
    @OrderBy("name")
    public List<LabelEntity> labels = new ArrayList<>(0);

    @NotNull
    @Enumerated(EnumType.STRING)
    public Priority priority = Priority.MEDIUM;

    @OneToMany(mappedBy = "reference")
    public List<TaskStatusReferenceEntity> referencedBy = new ArrayList<>(0);
}
