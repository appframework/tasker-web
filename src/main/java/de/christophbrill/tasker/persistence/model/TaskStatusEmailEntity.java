package de.christophbrill.tasker.persistence.model;

import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.AttributeOverrides;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import java.io.Serial;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "TaskStatusEmail")
@Table(name = "taskstatus_email")
@DiscriminatorValue("email")
@PrimaryKeyJoinColumn(name = "taskstatus_id")
public class TaskStatusEmailEntity extends TaskStatusEntity {

    @Serial
    private static final long serialVersionUID = 4367528442353294433L;

    @AttributeOverrides(
        {
            @AttributeOverride(name = "name", column = @Column(name = "sendername")),
            @AttributeOverride(name = "address", column = @Column(name = "sender")),
        }
    )
    @Embedded
    public EmailEntity sender;

    @Column(name = "`at`")
    public LocalDateTime at;

    // XXX not supported by ReactivePaddedBatchingCollectionInitializerBuilder BatchSize(size = 5)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "recipient", joinColumns = @JoinColumn(name = "taskstatusemail_id"))
    public List<EmailEntity> recipients = new ArrayList<>(0);

    @ElementCollection
    @CollectionTable(name = "recipient_cc", joinColumns = @JoinColumn(name = "taskstatusemail_id"))
    public List<EmailEntity> recipientsCc = new ArrayList<>(0);

    @ElementCollection
    @CollectionTable(name = "recipient_bcc", joinColumns = @JoinColumn(name = "taskstatusemail_id"))
    public List<EmailEntity> recipientsBcc = new ArrayList<>(0);

    public String subject;
    public String body;

    @Column(name = "message_id", length = 2048)
    public String messageId;

    @Column(name = "in_reply_to", length = 2048)
    public String inReplyTo;

    @ManyToMany
    @JoinTable(
        name = "taskstatus_email_attachment",
        joinColumns = @JoinColumn(name = "taskstatus_id", referencedColumnName = "taskstatus_id"),
        inverseJoinColumns = @JoinColumn(name = "binarydata_id", referencedColumnName = "id")
    )
    public List<BinaryDataEntity> attachments = new ArrayList<>(0);

    public static PanacheQuery<TaskStatusEmailEntity> findByMessageId(String messageId) {
        return find("messageId", messageId);
    }

    public static PanacheQuery<TaskStatusEmailEntity> findByInReplyTo(String inReplyTo) {
        return find("inReplyTo", inReplyTo);
    }
}
