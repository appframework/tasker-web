package de.christophbrill.tasker.persistence.model;

import de.christophbrill.appframework.persistence.model.DbObject;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import java.io.Serial;

@Entity(name = "TextSplit")
@Table(name = "textsplit")
public class TextSplitEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = -2262435569880917326L;

    public String expression;
}
