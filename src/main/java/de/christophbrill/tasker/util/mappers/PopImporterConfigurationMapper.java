package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.tasker.persistence.model.PopImporterConfigurationEntity;
import de.christophbrill.tasker.ui.dto.PopImporterConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = PopImporterConfigurationFactory.class)
public interface PopImporterConfigurationMapper
    extends ResourceMapper<PopImporterConfiguration, PopImporterConfigurationEntity> {
    PopImporterConfigurationMapper INSTANCE = Mappers.getMapper(PopImporterConfigurationMapper.class);

    @Mapping(target = "currentFailedLogins", ignore = true)
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    PopImporterConfigurationEntity mapDtoToEntity(PopImporterConfiguration dto);

    @AfterMapping
    default void customDtoToEntity(PopImporterConfiguration dto, @MappingTarget PopImporterConfigurationEntity entity) {
        if (StringUtils.isNotEmpty(dto.password)) {
            entity.password = dto.password;
        }
        if (!dto.disabled && entity.disabled) {
            entity.currentFailedLogins = 0;
        }
    }

    @Mapping(target = "currentFailedLogins", ignore = true)
    @Mapping(target = "password", ignore = true)
    @Override
    PopImporterConfiguration mapEntityToDto(PopImporterConfigurationEntity entity);

    @AfterMapping
    default void customEntityToDto(PopImporterConfigurationEntity entity, @MappingTarget PopImporterConfiguration dto) {
        dto.currentFailedLogins = entity.currentFailedLogins;
    }
}
