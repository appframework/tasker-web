package de.christophbrill.tasker.util.mappers;

import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusReferenceEntity;
import de.christophbrill.tasker.ui.dto.Task;
import de.christophbrill.tasker.ui.dto.TaskStatusReference;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = TaskStatusFactory.class)
interface TaskStatusReferenceMapper extends AbstractTaskStatusMapper<TaskStatusReference, TaskStatusReferenceEntity> {
    TaskStatusReferenceMapper INSTANCE = Mappers.getMapper(TaskStatusReferenceMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "task", ignore = true)
    @Mapping(target = "reference", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    TaskStatusReferenceEntity mapDtoToEntity(TaskStatusReference dto);

    @AfterMapping
    default void customDtoToEntity(TaskStatusReference dto, @MappingTarget TaskStatusReferenceEntity entity) {
        if (dto.reference != null && dto.reference.id != null) {
            entity.reference = TaskEntity.findById(dto.reference.id);
        } else {
            entity.reference = null;
        }
    }

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "taskId", ignore = true)
    @Mapping(target = "reference", ignore = true)
    @Override
    TaskStatusReference mapEntityToDto(TaskStatusReferenceEntity entity);

    @AfterMapping
    default void customEntityToDto(TaskStatusReferenceEntity entity, @MappingTarget TaskStatusReference dto) {
        if (entity.reference != null) {
            dto.reference = new Task();
            dto.reference.id = entity.reference.id;
            dto.reference.state = entity.reference.state;
            dto.reference.title = entity.reference.title;
        } else {
            dto.reference = null;
        }
    }
}
