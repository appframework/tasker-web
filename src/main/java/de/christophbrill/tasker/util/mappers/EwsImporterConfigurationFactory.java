package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.tasker.persistence.model.EwsImporterConfigurationEntity;
import de.christophbrill.tasker.ui.dto.EwsImporterConfiguration;
import org.mapstruct.ObjectFactory;

public class EwsImporterConfigurationFactory {

    @ObjectFactory
    public EwsImporterConfigurationEntity createEntity(EwsImporterConfiguration dto) {
        if (dto != null && dto.id != null) {
            EwsImporterConfigurationEntity entity = EwsImporterConfigurationEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("EwsImporterConfiguration with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new EwsImporterConfigurationEntity();
    }
}
