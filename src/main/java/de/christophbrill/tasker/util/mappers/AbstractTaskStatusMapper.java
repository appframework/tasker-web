package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEntity;
import de.christophbrill.tasker.ui.dto.TaskStatus;
import org.mapstruct.AfterMapping;
import org.mapstruct.MappingTarget;

public interface AbstractTaskStatusMapper<DTO extends TaskStatus, ENTITY extends TaskStatusEntity>
    extends ResourceMapper<DTO, ENTITY> {
    @AfterMapping
    default void customDtoToEntityBase(DTO dto, @MappingTarget ENTITY entity) {
        if (dto.taskId != null) {
            entity.task = TaskEntity.findById(dto.taskId);
        } else {
            entity.task = null;
        }
    }

    @AfterMapping
    default void customEntityToDtoBase(ENTITY entity, @MappingTarget DTO dto) {
        if (entity.task != null) {
            dto.taskId = entity.task.id;
        } else {
            dto.taskId = null;
        }
        dto.createdBy = map(entity.createdBy);
    }

    default String map(UserEntity value) {
        if (value == null) {
            return null;
        }
        return value.name;
    }
}
