package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.tasker.persistence.model.TaskStatusActionEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusDueEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEffortEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEmailEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusFileEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusPersonEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusPriorityEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusReferenceEntity;
import de.christophbrill.tasker.ui.dto.TaskStatus;
import de.christophbrill.tasker.ui.dto.TaskStatusAction;
import de.christophbrill.tasker.ui.dto.TaskStatusDue;
import de.christophbrill.tasker.ui.dto.TaskStatusEffort;
import de.christophbrill.tasker.ui.dto.TaskStatusEmail;
import de.christophbrill.tasker.ui.dto.TaskStatusFile;
import de.christophbrill.tasker.ui.dto.TaskStatusGeneric;
import de.christophbrill.tasker.ui.dto.TaskStatusPerson;
import de.christophbrill.tasker.ui.dto.TaskStatusPriority;
import de.christophbrill.tasker.ui.dto.TaskStatusReference;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = TaskStatusFactory.class)
public interface TaskStatusMapper extends AbstractTaskStatusMapper<TaskStatus, TaskStatusEntity> {
    TaskStatusMapper INSTANCE = Mappers.getMapper(TaskStatusMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "task", ignore = true)
    @Override
    default TaskStatusEntity mapDtoToEntity(TaskStatus dto) {
        return switch (dto) {
            case TaskStatusGeneric taskStatusGeneric -> TaskStatusGenericMapper.INSTANCE.mapDtoToEntity(
                taskStatusGeneric
            );
            case TaskStatusPerson taskStatusPerson -> TaskStatusPersonMapper.INSTANCE.mapDtoToEntity(taskStatusPerson);
            case TaskStatusDue taskStatusDue -> TaskStatusDueMapper.INSTANCE.mapDtoToEntity(taskStatusDue);
            case TaskStatusAction taskStatusAction -> TaskStatusActionMapper.INSTANCE.mapDtoToEntity(taskStatusAction);
            case TaskStatusEmail taskStatusEmail -> TaskStatusEmailMapper.INSTANCE.mapDtoToEntity(taskStatusEmail);
            case TaskStatusPriority taskStatusPriority -> TaskStatusPriorityMapper.INSTANCE.mapDtoToEntity(
                taskStatusPriority
            );
            case TaskStatusFile taskStatusFile -> TaskStatusFileMapper.INSTANCE.mapDtoToEntity(taskStatusFile);
            case TaskStatusReference taskStatusReference -> TaskStatusReferenceMapper.INSTANCE.mapDtoToEntity(
                taskStatusReference
            );
            case TaskStatusEffort taskStatusEffort -> TaskStatusEffortMapper.INSTANCE.mapDtoToEntity(taskStatusEffort);
            case null -> null;
            default -> throw new BadArgumentException("Cannot handle class " + dto.getClass());
        };
    }

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "taskId", ignore = true)
    @Override
    default TaskStatus mapEntityToDto(TaskStatusEntity entity) {
        return switch (entity) {
            case TaskStatusGenericEntity taskStatusGenericEntity -> TaskStatusGenericMapper.INSTANCE.mapEntityToDto(
                taskStatusGenericEntity
            );
            case TaskStatusPersonEntity taskStatusPersonEntity -> TaskStatusPersonMapper.INSTANCE.mapEntityToDto(
                taskStatusPersonEntity
            );
            case TaskStatusDueEntity taskStatusDueEntity -> TaskStatusDueMapper.INSTANCE.mapEntityToDto(
                taskStatusDueEntity
            );
            case TaskStatusActionEntity taskStatusActionEntity -> TaskStatusActionMapper.INSTANCE.mapEntityToDto(
                taskStatusActionEntity
            );
            case TaskStatusEmailEntity taskStatusEmailEntity -> TaskStatusEmailMapper.INSTANCE.mapEntityToDto(
                taskStatusEmailEntity
            );
            case TaskStatusPriorityEntity taskStatusPriorityEntity -> TaskStatusPriorityMapper.INSTANCE.mapEntityToDto(
                taskStatusPriorityEntity
            );
            case TaskStatusFileEntity taskStatusFileEntity -> TaskStatusFileMapper.INSTANCE.mapEntityToDto(
                taskStatusFileEntity
            );
            case TaskStatusReferenceEntity taskStatusReferenceEntity -> TaskStatusReferenceMapper.INSTANCE.mapEntityToDto(
                taskStatusReferenceEntity
            );
            case TaskStatusEffortEntity taskStatusEffortEntity -> TaskStatusEffortMapper.INSTANCE.mapEntityToDto(
                taskStatusEffortEntity
            );
            case null -> null;
            default -> throw new BadArgumentException("Cannot handle class " + entity.getClass());
        };
    }
}
