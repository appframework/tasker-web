package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.tasker.persistence.model.PopImporterConfigurationEntity;
import de.christophbrill.tasker.ui.dto.PopImporterConfiguration;
import org.mapstruct.ObjectFactory;

public class PopImporterConfigurationFactory {

    @ObjectFactory
    public PopImporterConfigurationEntity createEntity(PopImporterConfiguration dto) {
        if (dto != null && dto.id != null) {
            PopImporterConfigurationEntity entity = PopImporterConfigurationEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("PopImporterConfiguration with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new PopImporterConfigurationEntity();
    }
}
