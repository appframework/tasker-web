package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.tasker.persistence.model.EwsImporterConfigurationEntity;
import de.christophbrill.tasker.ui.dto.EwsImporterConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = EwsImporterConfigurationFactory.class)
public interface EwsImporterConfigurationMapper
    extends ResourceMapper<EwsImporterConfiguration, EwsImporterConfigurationEntity> {
    EwsImporterConfigurationMapper INSTANCE = Mappers.getMapper(EwsImporterConfigurationMapper.class);

    @Mapping(target = "currentFailedLogins", ignore = true)
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    EwsImporterConfigurationEntity mapDtoToEntity(EwsImporterConfiguration dto);

    @AfterMapping
    default void customDtoToEntity(EwsImporterConfiguration dto, @MappingTarget EwsImporterConfigurationEntity entity) {
        if (StringUtils.isNotEmpty(dto.password)) {
            entity.password = dto.password;
        }
        if (!dto.disabled && entity.disabled) {
            entity.currentFailedLogins = 0;
        }
    }

    @Mapping(target = "currentFailedLogins", ignore = true)
    @Mapping(target = "password", ignore = true)
    @Override
    EwsImporterConfiguration mapEntityToDto(EwsImporterConfigurationEntity entity);

    @AfterMapping
    default void customEntityToDto(EwsImporterConfigurationEntity entity, @MappingTarget EwsImporterConfiguration dto) {
        dto.currentFailedLogins = entity.currentFailedLogins;
    }
}
