package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusReferenceEntity;
import de.christophbrill.tasker.ui.dto.Task;
import de.christophbrill.tasker.ui.dto.TaskStatus;
import de.christophbrill.tasker.ui.dto.TaskStatusReference;
import java.util.ArrayList;
import java.util.List;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { LabelMapper.class, TaskFactory.class })
public interface TaskMapper extends ResourceMapper<Task, TaskEntity> {
    TaskMapper INSTANCE = Mappers.getMapper(TaskMapper.class);
    TaskStatusFactory dtoFactory = new TaskStatusFactory();

    @Mapping(target = "status", ignore = true)
    @Mapping(target = "assignee", ignore = true)
    @Mapping(target = "assigneeName", ignore = true)
    @Mapping(target = "referencedBy", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    TaskEntity mapDtoToEntity(Task dto);

    @Mapping(target = "assignee", ignore = true)
    @Mapping(target = "referencedBy", ignore = true)
    @Override
    Task mapEntityToDto(TaskEntity entity);

    @AfterMapping
    default void customEntityToDto(TaskEntity entity, @MappingTarget Task dto) {
        dto.assignee = entity.assignee != null ? entity.assignee.name : entity.assigneeName;

        if (entity.referencedBy != null) {
            List<TaskStatusReference> references = new ArrayList<>(entity.referencedBy.size());
            for (TaskStatusReferenceEntity reference : entity.referencedBy) {
                TaskStatusReference statusReference = new TaskStatusReference();
                statusReference.reference = new Task();
                statusReference.reference.id = reference.task.id;
                statusReference.reference.title = reference.task.title;
                statusReference.reference.state = reference.task.state;
                statusReference.relationType = reference.relationType;
                references.add(statusReference);
            }
            dto.referencedBy = references;
        }
    }

    default List<TaskStatus> map(List<TaskStatusEntity> entities) {
        if (entities == null) {
            return null;
        }
        return TaskStatusMapper.INSTANCE.mapEntitiesToDtos(entities);
    }
}
