package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.appframework.ui.exceptions.BadStateException;
import de.christophbrill.tasker.persistence.model.EwsImporterConfigurationEntity;
import de.christophbrill.tasker.persistence.model.ImporterConfigurationEntity;
import de.christophbrill.tasker.persistence.model.PopImporterConfigurationEntity;
import de.christophbrill.tasker.ui.dto.EwsImporterConfiguration;
import de.christophbrill.tasker.ui.dto.ImporterConfiguration;
import de.christophbrill.tasker.ui.dto.PopImporterConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ImporterConfigurationMapper
    extends ResourceMapper<ImporterConfiguration, ImporterConfigurationEntity> {
    ImporterConfigurationMapper INSTANCE = Mappers.getMapper(ImporterConfigurationMapper.class);

    @Override
    default ImporterConfiguration mapEntityToDto(ImporterConfigurationEntity entity) {
        return switch (entity) {
            case EwsImporterConfigurationEntity ewsImporterConfigurationEntity -> EwsImporterConfigurationMapper.INSTANCE.mapEntityToDto(
                ewsImporterConfigurationEntity
            );
            case PopImporterConfigurationEntity popImporterConfigurationEntity -> PopImporterConfigurationMapper.INSTANCE.mapEntityToDto(
                popImporterConfigurationEntity
            );
            case null -> null;
            default -> throw new BadStateException("No mapper for " + entity.getClass());
        };
    }

    @Override
    default ImporterConfigurationEntity mapDtoToEntity(ImporterConfiguration dto) {
        return switch (dto) {
            case EwsImporterConfiguration ewsImporterConfiguration -> EwsImporterConfigurationMapper.INSTANCE.mapDtoToEntity(
                ewsImporterConfiguration
            );
            case PopImporterConfiguration popImporterConfiguration -> PopImporterConfigurationMapper.INSTANCE.mapDtoToEntity(
                popImporterConfiguration
            );
            case null -> null;
            default -> throw new BadStateException("No mapper for " + dto.getClass());
        };
    }
}
