package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.tasker.persistence.model.AutomationEntity;
import de.christophbrill.tasker.ui.dto.Automation;
import org.mapstruct.ObjectFactory;

public class AutomationFactory {

    @ObjectFactory
    public AutomationEntity createEntity(Automation dto) {
        if (dto != null && dto.id != null) {
            AutomationEntity entity = AutomationEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Automation with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new AutomationEntity();
    }
}
