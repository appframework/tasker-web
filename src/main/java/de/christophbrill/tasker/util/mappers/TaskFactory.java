package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.ui.dto.Task;
import org.mapstruct.ObjectFactory;

public class TaskFactory {

    @ObjectFactory
    public TaskEntity createEntity(Task dto) {
        if (dto != null && dto.id != null) {
            TaskEntity entity = TaskEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Task with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new TaskEntity();
    }
}
