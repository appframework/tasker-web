package de.christophbrill.tasker.util.mappers;

import de.christophbrill.tasker.persistence.model.TaskStatusActionEntity;
import de.christophbrill.tasker.ui.dto.TaskStatusAction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = TaskStatusFactory.class)
interface TaskStatusActionMapper extends AbstractTaskStatusMapper<TaskStatusAction, TaskStatusActionEntity> {
    TaskStatusActionMapper INSTANCE = Mappers.getMapper(TaskStatusActionMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "taskId", ignore = true)
    @Override
    TaskStatusAction mapEntityToDto(TaskStatusActionEntity entity);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "task", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    TaskStatusActionEntity mapDtoToEntity(TaskStatusAction dto);
}
