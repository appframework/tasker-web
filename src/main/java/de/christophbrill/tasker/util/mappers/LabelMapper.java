package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.tasker.persistence.model.LabelEntity;
import de.christophbrill.tasker.ui.dto.Label;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = LabelFactory.class)
public interface LabelMapper extends ResourceMapper<Label, LabelEntity> {
    LabelMapper INSTANCE = Mappers.getMapper(LabelMapper.class);

    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Override
    LabelEntity mapDtoToEntity(Label label);
}
