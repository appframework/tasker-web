package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.tasker.persistence.model.AutomationEntity;
import de.christophbrill.tasker.ui.dto.Automation;
import de.christophbrill.tasker.util.compiler.Compiler;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(uses = AutomationFactory.class)
public interface AutomationMapper extends ResourceMapper<Automation, AutomationEntity> {
    AutomationMapper INSTANCE = Mappers.getMapper(AutomationMapper.class);

    @Mapping(target = "compiled", qualifiedByName = "AutomationMapper#compiled")
    @Mapping(target = "builtin", ignore = true)
    @Override
    Automation mapEntityToDto(AutomationEntity entity);

    @AfterMapping
    default void customEntityToDto(AutomationEntity entity, @MappingTarget Automation dto) {
        dto.compiled = map(entity.compiled);
        try {
            Class.forName(entity.qualifiedName);
            dto.builtin = true;
        } catch (ClassNotFoundException e) {
            dto.builtin = false;
        }
    }

    @Named("AutomationMapper#compiled")
    default boolean map(byte[] compiled) {
        return compiled != null && compiled.length > 0;
    }

    @Mapping(target = "compiled", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    AutomationEntity mapDtoToEntity(Automation dto);

    @AfterMapping
    default void customDtoToEntity(Automation dto, @MappingTarget AutomationEntity entity) {
        if (dto.source != null && !dto.source.isEmpty()) {
            entity.compiled = Compiler.compile(dto.qualifiedName, dto.source);
        } else {
            entity.compiled = null;
        }
    }
}
