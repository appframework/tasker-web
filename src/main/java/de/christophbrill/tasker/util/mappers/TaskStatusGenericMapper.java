package de.christophbrill.tasker.util.mappers;

import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.ui.dto.TaskStatusGeneric;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = TaskStatusFactory.class)
interface TaskStatusGenericMapper extends AbstractTaskStatusMapper<TaskStatusGeneric, TaskStatusGenericEntity> {
    TaskStatusGenericMapper INSTANCE = Mappers.getMapper(TaskStatusGenericMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "taskId", ignore = true)
    @Override
    TaskStatusGeneric mapEntityToDto(TaskStatusGenericEntity entity);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "task", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    TaskStatusGenericEntity mapDtoToEntity(TaskStatusGeneric dto);
}
