package de.christophbrill.tasker.util.mappers;

import de.christophbrill.tasker.persistence.model.TaskStatusEffortEntity;
import de.christophbrill.tasker.ui.dto.TaskStatusEffort;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = TaskStatusFactory.class)
interface TaskStatusEffortMapper extends AbstractTaskStatusMapper<TaskStatusEffort, TaskStatusEffortEntity> {
    TaskStatusEffortMapper INSTANCE = Mappers.getMapper(TaskStatusEffortMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "taskId", ignore = true)
    @Override
    TaskStatusEffort mapEntityToDto(TaskStatusEffortEntity entity);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "task", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    TaskStatusEffortEntity mapDtoToEntity(TaskStatusEffort dto);
}
