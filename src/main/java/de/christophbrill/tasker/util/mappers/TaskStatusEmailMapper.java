package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.mapping.BinaryDataMapper;
import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import de.christophbrill.appframework.ui.dto.BinaryData;
import de.christophbrill.tasker.persistence.model.EmailEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEmailEntity;
import de.christophbrill.tasker.ui.dto.TaskStatusEmail;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { TaskStatusMapper.class, BinaryDataMapper.class, TaskStatusFactory.class })
interface TaskStatusEmailMapper extends AbstractTaskStatusMapper<TaskStatusEmail, TaskStatusEmailEntity> {
    TaskStatusEmailMapper INSTANCE = Mappers.getMapper(TaskStatusEmailMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "taskId", ignore = true)
    @Override
    TaskStatusEmail mapEntityToDto(TaskStatusEmailEntity entity);

    @AfterMapping
    default void customEntityToDto(TaskStatusEmailEntity entity, @MappingTarget TaskStatusEmail dto) {
        if (CollectionUtils.isNotEmpty(entity.attachments)) {
            List<BinaryData> attachments = new ArrayList<>();
            for (BinaryDataEntity binary : entity.attachments) {
                attachments.add(new BinaryData(binary.id, binary.filename, binary.size, binary.contentType));
            }
            dto.attachments = attachments;
        } else {
            dto.attachments = null;
        }
    }

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "task", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    TaskStatusEmailEntity mapDtoToEntity(TaskStatusEmail dto);

    default String mapEmailEntityToString(EmailEntity source) {
        return source != null ? source.address : null;
    }
}
