package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.tasker.persistence.model.TaskStatusActionEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusDueEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEffortEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEmailEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusFileEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusPersonEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusPriorityEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusReferenceEntity;
import de.christophbrill.tasker.ui.dto.TaskStatus;
import de.christophbrill.tasker.ui.dto.TaskStatusAction;
import de.christophbrill.tasker.ui.dto.TaskStatusDue;
import de.christophbrill.tasker.ui.dto.TaskStatusEffort;
import de.christophbrill.tasker.ui.dto.TaskStatusEmail;
import de.christophbrill.tasker.ui.dto.TaskStatusFile;
import de.christophbrill.tasker.ui.dto.TaskStatusGeneric;
import de.christophbrill.tasker.ui.dto.TaskStatusPerson;
import de.christophbrill.tasker.ui.dto.TaskStatusPriority;
import de.christophbrill.tasker.ui.dto.TaskStatusReference;
import java.util.function.Supplier;
import org.mapstruct.ObjectFactory;

public class TaskStatusFactory {

    @ObjectFactory
    public TaskStatusGenericEntity newTaskStatusGeneric(TaskStatusGeneric dto) {
        return loadOrDefault(dto, TaskStatusGenericEntity::new);
    }

    @ObjectFactory
    public TaskStatusPersonEntity newTaskStatusPerson(TaskStatusPerson dto) {
        return loadOrDefault(dto, TaskStatusPersonEntity::new);
    }

    @ObjectFactory
    public TaskStatusDueEntity newTaskStatusDue(TaskStatusDue dto) {
        return loadOrDefault(dto, TaskStatusDueEntity::new);
    }

    @ObjectFactory
    public TaskStatusActionEntity newTaskStatusAction(TaskStatusAction dto) {
        return loadOrDefault(dto, TaskStatusActionEntity::new);
    }

    @ObjectFactory
    public TaskStatusEmailEntity newTaskStatusEmail(TaskStatusEmail dto) {
        return loadOrDefault(dto, TaskStatusEmailEntity::new);
    }

    @ObjectFactory
    public TaskStatusPriorityEntity newTaskStatusPriority(TaskStatusPriority dto) {
        return loadOrDefault(dto, TaskStatusPriorityEntity::new);
    }

    @ObjectFactory
    public TaskStatusFileEntity newTaskStatusFile(TaskStatusFile dto) {
        return loadOrDefault(dto, TaskStatusFileEntity::new);
    }

    @ObjectFactory
    public TaskStatusReferenceEntity newTaskStatusReference(TaskStatusReference dto) {
        return loadOrDefault(dto, TaskStatusReferenceEntity::new);
    }

    @ObjectFactory
    public TaskStatusEffortEntity newTaskStatusEffort(TaskStatusEffort dto) {
        return loadOrDefault(dto, TaskStatusEffortEntity::new);
    }

    private <T extends TaskStatusEntity> T loadOrDefault(TaskStatus dto, Supplier<T> constructor) {
        if (dto.id != null) {
            T entity = T.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("TaskStatusGenericEntity with ID " + dto.id + " not found");
            }
            return entity;
        }
        return constructor.get();
    }
}
