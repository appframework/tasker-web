package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.tasker.persistence.model.TextSplitEntity;
import de.christophbrill.tasker.ui.dto.TextSplit;
import org.mapstruct.ObjectFactory;

public class TextSplitFactory {

    @ObjectFactory
    public TextSplitEntity createEntity(TextSplit dto) {
        if (dto != null && dto.id != null) {
            TextSplitEntity entity = TextSplitEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("TextSplit with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new TextSplitEntity();
    }
}
