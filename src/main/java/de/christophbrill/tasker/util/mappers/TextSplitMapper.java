package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.tasker.persistence.model.TextSplitEntity;
import de.christophbrill.tasker.ui.dto.TextSplit;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = TextSplitFactory.class)
public interface TextSplitMapper extends ResourceMapper<TextSplit, TextSplitEntity> {
    TextSplitMapper INSTANCE = Mappers.getMapper(TextSplitMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    TextSplitEntity mapDtoToEntity(TextSplit dto);
}
