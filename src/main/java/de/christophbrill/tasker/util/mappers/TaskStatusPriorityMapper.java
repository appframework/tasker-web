package de.christophbrill.tasker.util.mappers;

import de.christophbrill.tasker.persistence.model.TaskStatusPriorityEntity;
import de.christophbrill.tasker.ui.dto.TaskStatusPriority;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = TaskStatusFactory.class)
interface TaskStatusPriorityMapper extends AbstractTaskStatusMapper<TaskStatusPriority, TaskStatusPriorityEntity> {
    TaskStatusPriorityMapper INSTANCE = Mappers.getMapper(TaskStatusPriorityMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "taskId", ignore = true)
    @Override
    TaskStatusPriority mapEntityToDto(TaskStatusPriorityEntity entity);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "task", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    TaskStatusPriorityEntity mapDtoToEntity(TaskStatusPriority dto);
}
