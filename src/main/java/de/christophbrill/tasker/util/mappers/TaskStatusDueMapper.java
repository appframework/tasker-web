package de.christophbrill.tasker.util.mappers;

import de.christophbrill.tasker.persistence.model.TaskStatusDueEntity;
import de.christophbrill.tasker.ui.dto.TaskStatusDue;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = TaskStatusFactory.class)
interface TaskStatusDueMapper extends AbstractTaskStatusMapper<TaskStatusDue, TaskStatusDueEntity> {
    TaskStatusDueMapper INSTANCE = Mappers.getMapper(TaskStatusDueMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "taskId", ignore = true)
    @Override
    TaskStatusDue mapEntityToDto(TaskStatusDueEntity entity);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "task", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    TaskStatusDueEntity mapDtoToEntity(TaskStatusDue dto);
}
