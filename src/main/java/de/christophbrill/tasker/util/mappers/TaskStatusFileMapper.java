package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.mapping.BinaryDataMapper;
import de.christophbrill.tasker.persistence.model.TaskStatusFileEntity;
import de.christophbrill.tasker.ui.dto.TaskStatusFile;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { TaskStatusMapper.class, BinaryDataMapper.class, TaskStatusFactory.class })
interface TaskStatusFileMapper extends AbstractTaskStatusMapper<TaskStatusFile, TaskStatusFileEntity> {
    TaskStatusFileMapper INSTANCE = Mappers.getMapper(TaskStatusFileMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "taskId", ignore = true)
    @Override
    TaskStatusFile mapEntityToDto(TaskStatusFileEntity entity);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "task", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    TaskStatusFileEntity mapDtoToEntity(TaskStatusFile dto);
}
