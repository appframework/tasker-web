package de.christophbrill.tasker.util.mappers;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusPersonEntity;
import de.christophbrill.tasker.ui.dto.TaskStatusPerson;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = TaskStatusFactory.class)
interface TaskStatusPersonMapper extends AbstractTaskStatusMapper<TaskStatusPerson, TaskStatusPersonEntity> {
    TaskStatusPersonMapper INSTANCE = Mappers.getMapper(TaskStatusPersonMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "task", ignore = true)
    @Mapping(target = "assignee", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Override
    TaskStatusPersonEntity mapDtoToEntity(TaskStatusPerson dto);

    @AfterMapping
    default void customDtoToEntity(TaskStatusPerson dto, @MappingTarget TaskStatusPersonEntity entity) {
        if (entity.name != null) {
            entity.assignee = UserEntity.findByName(entity.name);
        } else {
            entity.assignee = null;
        }
    }

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "taskId", ignore = true)
    @Mapping(target = "assigneeId", ignore = true)
    @Override
    TaskStatusPerson mapEntityToDto(TaskStatusPersonEntity entity);

    @AfterMapping
    default void customEntityToDto(TaskStatusPersonEntity entity, @MappingTarget TaskStatusPerson dto) {
        if (entity.assignee != null) {
            dto.name = entity.assignee.name;
        }
    }
}
