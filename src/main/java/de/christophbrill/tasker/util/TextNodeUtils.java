package de.christophbrill.tasker.util;

import jakarta.annotation.Nonnull;
import org.jsoup.nodes.CDataNode;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;

public class TextNodeUtils {

    public static String text(Element element_) {
        final StringBuilder accum = new StringBuilder();
        NodeTraversor.traverse(
            new NodeVisitor() {
                public void head(@Nonnull Node node, int depth) {
                    if (node instanceof TextNode textNode) {
                        appendNormalisedText(accum, textNode);
                    } else if (node instanceof Element element) {
                        if (
                            !accum.isEmpty() &&
                            (element.isBlock() ||
                                element.tagName().equals("br") ||
                                (element.tagName().equals("o:p") && !element.hasText())) &&
                            !lastCharIsWhitespace(accum) &&
                            !lastCharIsNewline(accum)
                        ) accum.append('\n');
                    }
                }

                @Override
                public void tail(@Nonnull Node node, int depth) {
                    // make sure there is a space between block tags and immediately following text nodes <div>One</div>Two should be "One Two".
                    if (node instanceof Element element) {
                        if (
                            element.isBlock() &&
                            (node.nextSibling() instanceof TextNode) &&
                            !lastCharIsWhitespace(accum) &&
                            !lastCharIsNewline(accum)
                        ) accum.append('x');
                    }
                }
            },
            element_
        );
        return accum.toString().trim();
    }

    private static void appendNormalisedText(StringBuilder accum, TextNode textNode) {
        String text = textNode.getWholeText();

        if (preserveWhitespace(textNode.parent()) || textNode instanceof CDataNode) accum.append(text);
        else appendNormalisedWhitespace(accum, text, lastCharIsWhitespace(accum) || lastCharIsNewline(accum));
    }

    public static void appendNormalisedWhitespace(StringBuilder accum, String string, boolean stripLeading) {
        boolean lastWasWhite = false;
        boolean reachedNonWhite = false;

        int len = string.length();
        int c;
        for (int i = 0; i < len; i += Character.charCount(c)) {
            c = string.codePointAt(i);
            if (isActuallyWhitespace(c)) {
                if ((stripLeading && !reachedNonWhite) || lastWasWhite) continue;
                accum.append(' ');
                lastWasWhite = true;
            } else if (!isInvisibleChar(c)) {
                accum.appendCodePoint(c);
                lastWasWhite = false;
                reachedNonWhite = true;
            }
        }
    }

    public static boolean isActuallyWhitespace(int c) {
        return c == ' ' || c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == 160;
        // 160 is &nbsp; (non-breaking space). Not in the spec but expected.
    }

    public static boolean isInvisibleChar(int c) {
        return (Character.getType(c) == 16 && (c == 8203 || c == 8204 || c == 8205 || c == 173));
        // zero width sp, zw non join, zw join, soft hyphen
    }

    private static boolean lastCharIsNewline(StringBuilder sb) {
        return !sb.isEmpty() && sb.charAt(sb.length() - 1) == '\n';
    }

    private static boolean lastCharIsWhitespace(StringBuilder sb) {
        return !sb.isEmpty() && sb.charAt(sb.length() - 1) == ' ';
    }

    static boolean preserveWhitespace(Node node) {
        // looks only at this element and five levels up, to prevent recursion & needless stack searches
        if (node instanceof Element el) {
            int i = 0;
            do {
                if (el.tag().preserveWhitespace()) return true;
                el = el.parent();
                i++;
            } while (i < 6 && el != null);
        }
        return false;
    }
}
