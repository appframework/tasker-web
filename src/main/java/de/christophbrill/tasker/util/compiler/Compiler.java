package de.christophbrill.tasker.util.compiler;

import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.SimpleJavaFileObject;
import javax.tools.ToolProvider;

public class Compiler {

    private static final JavaCompiler JAVA_COMPILER = ToolProvider.getSystemJavaCompiler();
    private static final List<String> OPTIONS = Arrays.asList("-classpath", getClassPath());
    private static final BufferingJavaFileManager FILE_MANAGER;

    static {
        if (JAVA_COMPILER != null) {
            FILE_MANAGER = new BufferingJavaFileManager(JAVA_COMPILER.getStandardFileManager(null, null, null));
        } else {
            Log.warn("No java compiler available");
            FILE_MANAGER = null;
        }
    }

    /**
     * Get everything on the class loader right now
     */
    private static String getClassPath() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        StringBuilder buf = new StringBuilder();
        buf.append(".");
        for (URL url : ((URLClassLoader) classLoader).getURLs()) {
            buf.append(File.pathSeparatorChar).append(url.getFile());
        }
        return buf.toString();
    }

    public static Class<?> load(@Nonnull String name, @Nonnull byte[] compiled) throws ClassNotFoundException {
        // No file manager, no class
        if (FILE_MANAGER == null) {
            return null;
        }

        FILE_MANAGER.inject(name, compiled);
        return new RuntimeClassLoader(FILE_MANAGER, Thread.currentThread().getContextClassLoader()).loadClass(name);
    }

    @Nullable
    public static byte[] compile(@Nonnull String name, @Nonnull String source) {
        // No compiler, no class
        if (JAVA_COMPILER == null) {
            return null;
        }

        FILE_MANAGER.clear(name);

        JavaCompiler.CompilationTask compilationTask = JAVA_COMPILER.getTask(
            null,
            FILE_MANAGER,
            null,
            OPTIONS,
            null,
            Collections.singletonList(new MemoryJavaFileObject(name, source))
        );
        if (Boolean.FALSE.equals(compilationTask.call())) {
            return null;
        }

        ClassJavaFileObject klass = FILE_MANAGER.getClass(name);
        assert klass != null;
        return klass.getBytes();
    }

    private static class MemoryJavaFileObject extends SimpleJavaFileObject {

        private final String source;

        public MemoryJavaFileObject(String name, String source) {
            super(URI.create("string:///" + name.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
            this.source = source;
        }

        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
            return source;
        }
    }

    private static class ClassJavaFileObject extends SimpleJavaFileObject {

        private final ByteArrayOutputStream outputStream;

        protected ClassJavaFileObject(String className, Kind kind) {
            super(URI.create("mem:///" + className.replace('.', '/') + kind.extension), kind);
            outputStream = new ByteArrayOutputStream();
        }

        @Override
        public OutputStream openOutputStream() throws IOException {
            return outputStream;
        }

        public byte[] getBytes() {
            return outputStream.toByteArray();
        }
    }

    private static class BufferingJavaFileManager extends ForwardingJavaFileManager<JavaFileManager> {

        private final Map<String, ClassJavaFileObject> files = new HashMap<>();

        protected BufferingJavaFileManager(JavaFileManager fileManager) {
            super(fileManager);
        }

        @Override
        public JavaFileObject getJavaFileForOutput(
            Location location,
            String className,
            JavaFileObject.Kind kind,
            FileObject sibling
        ) throws IOException {
            ClassJavaFileObject file = new ClassJavaFileObject(className, kind);
            files.put(className, file);
            return file;
        }

        public void inject(String className, byte[] compiled) {
            ClassJavaFileObject file = new ClassJavaFileObject(className, Kind.CLASS);
            try (OutputStream out = file.openOutputStream()) {
                out.write(compiled);
            } catch (IOException e) {
                return;
            }
            files.put(className, file);
        }

        public void clear(String className) {
            files.remove(className);
        }

        @Nullable
        public ClassJavaFileObject getClass(@Nonnull String className) {
            return files.get(className);
        }
    }

    private static class RuntimeClassLoader extends ClassLoader {

        private final BufferingJavaFileManager fileManager;

        private RuntimeClassLoader(BufferingJavaFileManager fileManager, ClassLoader delegate) {
            super(delegate);
            this.fileManager = fileManager;
        }

        @Override
        protected Class<?> findClass(String name) throws ClassNotFoundException {
            assert name != null;
            ClassJavaFileObject file = fileManager.getClass(name);
            if (file != null) {
                byte[] bytes = file.getBytes();
                return super.defineClass(name, bytes, 0, bytes.length);
            }
            return super.findClass(name);
        }
    }
}
