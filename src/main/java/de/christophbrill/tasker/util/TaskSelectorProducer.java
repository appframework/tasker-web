package de.christophbrill.tasker.util;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.persistence.selector.UserSelector;
import de.christophbrill.tasker.persistence.model.Permission;
import de.christophbrill.tasker.persistence.selector.TaskSelector;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.persistence.EntityManager;
import jakarta.persistence.FlushModeType;
import java.security.Principal;

@ApplicationScoped
@Named
public class TaskSelectorProducer {

    @Inject
    EntityManager em;

    @Inject
    SecurityIdentity identity;

    public TaskSelector getSelector() {
        TaskSelector taskSelector = new TaskSelector(em);
        if (identity.getRoles().contains(Permission.VIEW_ALL_TASKS.name())) {
            return taskSelector;
        }
        return taskSelector.withVisibleTo(loadUser(identity.getPrincipal()));
    }

    public UserEntity loadUser(Principal subject) {
        return new UserSelector(em).withLogin(subject.toString()).withFlushMode(FlushModeType.COMMIT).find();
    }
}
