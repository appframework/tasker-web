package de.christophbrill.tasker.util.automations;

import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.ui.dto.TaskStatusDue;
import de.christophbrill.tasker.ui.rest.TaskStatusService;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class WeeklyReminderAutomationProcessor implements AutomationProcessor {

    @Override
    public void automate(TaskStatusGenericEntity status, TaskStatusService service) {
        TaskStatusDue due = new TaskStatusDue();
        due.taskId = status.task.id;
        due.due = LocalDateTime.now().plusDays(7).truncatedTo(ChronoUnit.DAYS);
        due.deadline = status.task.deadline;
        service.create(due);
    }
}
