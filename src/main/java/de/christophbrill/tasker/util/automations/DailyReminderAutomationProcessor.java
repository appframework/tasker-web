package de.christophbrill.tasker.util.automations;

import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.ui.dto.TaskStatusDue;
import de.christophbrill.tasker.ui.rest.TaskStatusService;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class DailyReminderAutomationProcessor implements AutomationProcessor {

    @Override
    public void automate(TaskStatusGenericEntity status, TaskStatusService service) {
        TaskStatusDue due = new TaskStatusDue();
        due.taskId = status.task.id;
        LocalDateTime days = LocalDateTime.now().plusDays(1);
        switch (days.getDayOfWeek()) {
            case SATURDAY -> days = days.plusDays(2);
            case SUNDAY -> days = days.plusDays(1);
            default -> {}
        }
        due.due = days.truncatedTo(ChronoUnit.DAYS);
        due.deadline = status.task.deadline;
        service.create(due);
    }
}
