package de.christophbrill.tasker.util.automations;

import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.ui.rest.TaskStatusService;

public interface AutomationProcessor {
    void automate(TaskStatusGenericEntity status, TaskStatusService service);
}
