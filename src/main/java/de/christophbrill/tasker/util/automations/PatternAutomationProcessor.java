package de.christophbrill.tasker.util.automations;

import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.ui.rest.TaskStatusService;
import java.util.regex.Matcher;

public interface PatternAutomationProcessor {
    void automate(TaskStatusGenericEntity status, Matcher matcher, TaskStatusService service);
}
