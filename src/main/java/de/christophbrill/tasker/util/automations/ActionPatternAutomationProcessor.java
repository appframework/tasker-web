package de.christophbrill.tasker.util.automations;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusActionEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.ui.dto.Task;
import de.christophbrill.tasker.ui.dto.TaskStatusAction;
import de.christophbrill.tasker.ui.dto.TaskStatusPerson;
import de.christophbrill.tasker.ui.dto.TaskStatusReference;
import de.christophbrill.tasker.ui.dto.TaskStatusReference.ReferenceType;
import de.christophbrill.tasker.ui.rest.TaskStatusService;
import java.util.regex.Matcher;

public class ActionPatternAutomationProcessor implements PatternAutomationProcessor {

    @Override
    public void automate(TaskStatusGenericEntity status, Matcher matcher, TaskStatusService service) {
        String referenceId = matcher.group(2);
        if (referenceId != null) {
            TaskStatusReference reference = new TaskStatusReference();
            reference.taskId = status.task.id;
            Task referenceTask = new Task();
            referenceTask.id = Long.valueOf(referenceId);
            reference.reference = referenceTask;
            reference.relationType = ReferenceType.RELATES_TO;
            service.create(reference);
        } else {
            String task = matcher.group(3);
            String topic = matcher.group(4);
            switch (task) {
                case "assign" -> {
                    TaskStatusPerson person = new TaskStatusPerson();
                    person.taskId = status.task.id;
                    UserEntity user = UserEntity.<UserEntity>find("name", topic).firstResult();
                    if (user != null) {
                        person.name = user.name;
                    } else {
                        person.name = topic;
                    }
                    service.create(person);
                }
                case "todo" -> {
                    TaskStatusAction action = new TaskStatusAction();
                    action.taskId = status.task.id;
                    action.title = topic;
                    service.create(action);
                }
                case "done" -> {
                    TaskStatusActionEntity action = TaskStatusActionEntity.findByTaskAndTitle(
                        status.task.id,
                        topic
                    ).firstResult();
                    action.completed = true;
                    action.persist();
                }
            }
        }
    }
}
