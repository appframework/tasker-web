package de.christophbrill.tasker.util.automations;

import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.ui.dto.State;
import de.christophbrill.tasker.ui.rest.TaskStatusService;

public class ForceCloseAutomationProcessor implements AutomationProcessor {

    @Override
    public void automate(TaskStatusGenericEntity status, TaskStatusService service) {
        status.state = State.COMPLETED;
    }

}
