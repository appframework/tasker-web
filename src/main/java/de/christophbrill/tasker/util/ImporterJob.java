package de.christophbrill.tasker.util;

import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadStateException;
import de.christophbrill.tasker.persistence.model.EwsImporterConfigurationEntity;
import de.christophbrill.tasker.persistence.model.ImporterConfigurationEntity;
import de.christophbrill.tasker.persistence.model.PopImporterConfigurationEntity;
import io.quarkus.logging.Log;
import io.quarkus.runtime.configuration.ConfigUtils;
import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.HeuristicMixedException;
import jakarta.transaction.HeuristicRollbackException;
import jakarta.transaction.NotSupportedException;
import jakarta.transaction.RollbackException;
import jakarta.transaction.SystemException;
import java.util.List;
import org.hibernate.Hibernate;

@ApplicationScoped
public class ImporterJob {

    @Inject
    EwsImporter ewsImporter;

    @Inject
    PopImporter popImporter;

    @Scheduled(every = "10m")
    public void runImport() {
        if (ConfigUtils.getProfiles().contains("test")) {
            Log.info("Not executing import configurations in test profile");
            return;
        }
        try {
            List<ImporterConfigurationEntity> configurations = ImporterConfigurationEntity.<
                ImporterConfigurationEntity
            >find("disabled", false).list();
            for (ImporterConfigurationEntity configuration : configurations) {
                performImport(configuration, null);
            }
        } catch (BadStateException e) {
            Log.error(e.getResponse().getEntity(), e);
        } catch (
            RuntimeException
            | NotSupportedException
            | HeuristicRollbackException
            | HeuristicMixedException
            | RollbackException
            | SystemException e
        ) {
            Log.error(e.getMessage(), e);
        }
    }

    private void performImport(ImporterConfigurationEntity configuration, Progress<ImportResult> progress)
        throws SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException, NotSupportedException {
        if (Hibernate.getClass(configuration).equals(PopImporterConfigurationEntity.class)) {
            popImporter.performImport((PopImporterConfigurationEntity) configuration, progress);
        } else if (Hibernate.getClass(configuration).equals(EwsImporterConfigurationEntity.class)) {
            ewsImporter.performImport((EwsImporterConfigurationEntity) configuration, progress);
        } else {
            Log.errorv("Unknown configuration type {0}", Hibernate.getClass(configuration));
        }
    }

    public void runImport(ImporterConfigurationEntity configuration, Progress<ImportResult> progress) {
        Log.debug("Consuming import event");
        try {
            performImport(configuration, progress);
        } catch (
            RuntimeException
            | NotSupportedException
            | HeuristicRollbackException
            | HeuristicMixedException
            | RollbackException
            | SystemException e
        ) {
            progress.success = false;
            progress.message = e.getMessage();
            Log.error(e.getMessage(), e);
        } finally {
            progress.completed = true;
        }
    }
}
