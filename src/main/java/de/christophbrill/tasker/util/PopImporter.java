package de.christophbrill.tasker.util;

import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadStateException;
import de.christophbrill.tasker.persistence.model.EmailEntity;
import de.christophbrill.tasker.persistence.model.PopImporterConfigurationEntity;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEmailEntity;
import de.christophbrill.tasker.ui.dto.State;
import io.quarkus.logging.Log;
import jakarta.annotation.Nullable;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.mail.Address;
import jakarta.mail.AuthenticationFailedException;
import jakarta.mail.BodyPart;
import jakarta.mail.Flags;
import jakarta.mail.Folder;
import jakarta.mail.Message;
import jakarta.mail.Message.RecipientType;
import jakarta.mail.MessagingException;
import jakarta.mail.Part;
import jakarta.mail.Session;
import jakarta.mail.Store;
import jakarta.mail.URLName;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import jakarta.persistence.EntityManager;
import jakarta.transaction.HeuristicMixedException;
import jakarta.transaction.HeuristicRollbackException;
import jakarta.transaction.NotSupportedException;
import jakarta.transaction.RollbackException;
import jakarta.transaction.SystemException;
import jakarta.transaction.UserTransaction;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ApplicationScoped
@Named
public class PopImporter {

    private static final Pattern PATTERN_SUBJECT = Pattern.compile("#([\\d]+|new)");
    private static final Pattern PATTERN_ATTACHMENT = Pattern.compile("([^;]+);\\s+name=\\s*\"([^\"]+)\"");

    @Inject
    UserTransaction transaction;

    @Inject
    EntityManager entityManager;

    @Inject
    ImporterUtil importerUtil;

    public void performImport(PopImporterConfigurationEntity configuration, @Nullable Progress<ImportResult> progress)
        throws SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException, NotSupportedException {
        ImportResult result = new ImportResult();

        if (configuration.disabled) {
            Log.warnv("Cannot import from disabled configuration {0}", configuration.id);
            if (progress != null) {
                progress.message = "Configuration disabled";
                progress.completed = true;
                progress.success = false;
            }
            return;
        }

        Log.info("Starting POP import");

        int port = configuration.port == 0 ? 995 : configuration.port;

        Properties properties = new Properties();
        properties.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.pop3.socketFactory.fallback", "false");
        properties.setProperty("mail.pop3.port", Integer.toString(port));
        properties.setProperty("mail.pop3.socketFactory.port", Integer.toString(port));
        properties.setProperty("mail.pop3.ssl.trust", configuration.server);
        URLName urln = new URLName(
            "pop3",
            configuration.server,
            port,
            null,
            configuration.username,
            configuration.password
        );

        Session session = Session.getInstance(properties, null);
        try (Store store = session.getStore(urln)) {
            transaction.begin();
            try {
                store.connect(configuration.server, configuration.username, configuration.password);
            } catch (AuthenticationFailedException e) {
                configuration.currentFailedLogins = configuration.currentFailedLogins + 1;
                if (configuration.currentFailedLogins >= configuration.maxFailedLogins) {
                    configuration.disabled = true;
                }
                configuration.persist();
                transaction.commit();
                return;
            }
            try (Folder inbox = store.getFolder("Inbox")) {
                inbox.open(Folder.READ_WRITE);

                // get the list of inbox messages
                Message[] messages = inbox.getMessages();

                if (progress != null) {
                    progress.max = messages.length;
                    progress.message = "Importing e-mails...";
                }

                for (int i = 0; i < messages.length; i++) {
                    Message message = messages[i];

                    if (progress != null) {
                        progress.value = i;
                        progress.message = "Importing e-mail " + i;
                    }

                    if (i % 10 == 0) {
                        entityManager.flush();
                        transaction.commit();
                        entityManager.clear();
                        transaction.begin();
                        Log.debugv("Imported {0} customers", i);
                    }

                    // Check if the pattern matches anything we know, otherwise skip (for now)
                    String subject = message.getSubject();
                    Matcher matcher = PATTERN_SUBJECT.matcher(subject);
                    TaskEntity task = null;
                    if (matcher.find()) {
                        // Load or create the task for the message
                        if (matcher.group(1).equals("new")) {
                            task = importerUtil.boostrapTask(subject);
                            result.created++;
                        } else {
                            try {
                                Long taskId = Long.valueOf(matcher.group(1));
                                task = TaskEntity.findById(taskId);
                                result.updated++;
                            } catch (RuntimeException e) {
                                // Probably a non-integer value
                            }
                        }
                    }

                    if (task == null) {
                        task = importerUtil.bootstrapOrFindExisting(
                            getInReplyTo(message),
                            getMessageId(message),
                            subject
                        );
                        result.created++;
                    }

                    // Create the statis entity, which will reactivate the task if it was completed
                    TaskStatusEmailEntity taskStatus = new TaskStatusEmailEntity();
                    taskStatus.state = task.state;
                    if (taskStatus.state == State.COMPLETED) {
                        taskStatus.state = State.IN_PROGRESS;
                    }

                    // Pre-fill the fields with defaults
                    setFieldsFromMessage(task, taskStatus, message);

                    // Check if we know hot to handle the content of the mail
                    Object content = message.getContent();
                    try {
                        if (content instanceof MimeMultipart multipart) {
                            // Now handle the actual content
                            task = handleMultiPartEmail(task, taskStatus, multipart);
                        } else if (content instanceof String) {
                            task = handleBodyPart(task, taskStatus, message);
                        } else {
                            Log.errorv(
                                "Unsupported content type {0} {1}",
                                content.getClass().getSimpleName(),
                                message.getContentType()
                            );
                            continue;
                        }

                        // Save and be done
                        taskStatus.task = task;
                        taskStatus.persist();
                        message.setFlag(Flags.Flag.DELETED, true);
                    } catch (BadStateException e) {
                        // Will be imported in the next attempt
                        Log.error(e.getResponse().getEntity());
                    }
                }
            }
            if (progress != null) {
                progress.message = "Imported " + progress.max + " e-mails";
                progress.completed = true;
                progress.success = true;
            }
            configuration.lastImport = LocalDateTime.now();
            configuration.persist();
            transaction.commit();
            if (progress != null) {
                progress.result = result;
            }
        } catch (IOException | MessagingException e) {
            if (progress != null) {
                progress.message = e.getMessage();
                progress.completed = true;
                progress.success = false;
            }
            Log.warn(e.getMessage(), e);
            transaction.rollback();
            throw new BadStateException(e.getMessage());
        } finally {
            Log.info("POP import completed");
        }
    }

    private static String getMessageId(Part message) throws MessagingException {
        return getFirstHeader(message, "Message-ID");
    }

    private static String getInReplyTo(Part message) throws MessagingException {
        return getFirstHeader(message, "In-Reply-To");
    }

    private static String getFirstHeader(Part message, String headerName) throws MessagingException {
        String[] header = message.getHeader(headerName);
        if (header != null && header.length > 0) {
            return header[0];
        }
        return null;
    }

    private TaskEntity handleMultiPartEmail(TaskEntity task, TaskStatusEmailEntity taskStatus, MimeMultipart content)
        throws MessagingException, IOException {
        for (int i = 0; i < content.getCount(); i++) {
            BodyPart part = content.getBodyPart(i);
            if (!(part instanceof MimeBodyPart)) {
                Log.errorv("Unsupported body part type {0} of {1}", part.getClass().getSimpleName(), content);
                continue;
            }
            task = handleBodyPart(task, taskStatus, part);
        }
        return task;
    }

    private TaskEntity handleBodyPart(
        TaskEntity task,
        TaskStatusEmailEntity taskStatus,
        Part part
    ) throws MessagingException, IOException {
        String contentType = part.getContentType();
        if (contentType.startsWith("message/rfc822")) {
            MimeMessage content1 = (MimeMessage) part.getContent();
            // If the task does not have a proper title yet, set it
            if (PATTERN_SUBJECT.matcher(task.title).matches()) {
                task.title = content1.getSubject();
                task.persist();
            }
            setFieldsFromMessage(task, taskStatus, content1);

            Object content2 = content1.getContent();
            if (content2 instanceof String contentString) {
                taskStatus.body = contentString;
            } else {
                task = handleMultiPartEmail(task, taskStatus, (MimeMultipart) content2);
            }
        } else if (contentType.startsWith("text/plain")) {
            taskStatus.body = (String) part.getContent();
        } else if (contentType.startsWith("text/html") || contentType.startsWith("text/xml")) {
            if (taskStatus.body == null) {
                taskStatus.body = (String) part.getContent();
            }
        } else if (
            contentType.startsWith("multipart/alternative") ||
            contentType.startsWith("multipart/related") ||
            contentType.startsWith("multipart/mixed")
        ) {
            task = handleMultiPartEmail(task, taskStatus, (MimeMultipart) part.getContent());
        } else if (contentType.startsWith("application/pkcs7-signature")) {
            Matcher matcher = PATTERN_ATTACHMENT.matcher(contentType);
            if (matcher.matches()) {
                Log.debugv("Skipping attachment {0}", matcher.group(2));
            } else {
                Log.debugv("Could not determine filename from {0}, skipping anyway", contentType);
            }
        } else if (
            contentType.startsWith("application/") ||
            contentType.startsWith("image/") ||
            contentType.startsWith("text/")
        ) {
            Matcher matcher = PATTERN_ATTACHMENT.matcher(contentType);
            if (matcher.matches()) {
                handleAttachment(taskStatus, part, matcher.group(2), matcher.group(1));
            } else {
                Log.warnv("Could not determine filename from {0}", contentType);
                handleAttachment(taskStatus, part, "unknown_filename", contentType);
            }
        } else {
            throw new BadStateException("Unsupported body part type " + contentType);
        }
        return task;
    }

    private void handleAttachment(
        TaskStatusEmailEntity taskStatus,
        Part part,
        String filename,
        String contentType
    ) throws IOException, MessagingException {
        BinaryDataEntity attachment = new BinaryDataEntity();
        var bos = new ByteArrayOutputStream();
        part.getDataHandler().writeTo(bos);
        attachment.data = bos.toByteArray();
        attachment.filename = filename;
        attachment.contentType = contentType;
        attachment.size = attachment.data.length;
        attachment.persist();
        taskStatus.attachments.add(attachment);
    }

    private static void setFieldsFromMessage(TaskEntity task, TaskStatusEmailEntity taskStatus, Message message)
        throws MessagingException {
        taskStatus.subject = message.getSubject();
        taskStatus.created = LocalDateTime.ofInstant(message.getSentDate().toInstant(), ZoneId.systemDefault());
        taskStatus.recipients = toEmailList(message.getRecipients(RecipientType.TO));
        taskStatus.recipientsCc = toEmailList(message.getRecipients(RecipientType.CC));
        taskStatus.recipientsBcc = toEmailList(message.getRecipients(RecipientType.BCC));
        if (task.assignee == null && taskStatus.recipients != null) {
            for (EmailEntity email : taskStatus.recipients) {
                UserEntity findByEmail = UserEntity.<UserEntity>find("email", email.address).firstResult();
                if (findByEmail != null) {
                    task.assignee = findByEmail;
                    task.assigneeName = findByEmail.name;
                    break;
                }
            }
        }
        if (task.assignee == null && taskStatus.recipientsCc != null) {
            for (EmailEntity email : taskStatus.recipientsCc) {
                UserEntity findByEmail = UserEntity.<UserEntity>find("email", email.address).firstResult();
                if (findByEmail != null) {
                    task.assignee = findByEmail;
                    break;
                }
            }
        }
        taskStatus.sender = toEmail(message.getFrom()[0]);
        if (task.assignee == null) {
            task.assignee = UserEntity.<UserEntity>find("email", taskStatus.sender.address).firstResult();
        }
        taskStatus.at = LocalDateTime.ofInstant(message.getSentDate().toInstant(), ZoneId.systemDefault());

        taskStatus.messageId = getMessageId(message);
    }

    private static List<EmailEntity> toEmailList(Address[] recipients) {
        if (recipients == null) {
            return null;
        }
        List<EmailEntity> result = new ArrayList<>(recipients.length);
        for (Address recipient : recipients) {
            result.add(toEmail(recipient));
        }
        return result;
    }

    private static EmailEntity toEmail(Address recipient) {
        return new EmailEntity(((InternetAddress) recipient).getPersonal(), ((InternetAddress) recipient).getAddress());
    }
}
