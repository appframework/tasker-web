package de.christophbrill.tasker.util;

import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadStateException;
import de.christophbrill.tasker.persistence.model.EmailEntity;
import de.christophbrill.tasker.persistence.model.EwsImporterConfigurationEntity;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEmailEntity;
import de.christophbrill.tasker.ui.dto.State;
import io.quarkus.logging.Log;
import jakarta.annotation.Nullable;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.persistence.EntityManager;
import jakarta.transaction.SystemException;
import jakarta.transaction.UserTransaction;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.WebProxy;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.exception.service.local.ServiceLocalException;
import microsoft.exchange.webservices.data.core.exception.service.remote.ServiceRequestException;
import microsoft.exchange.webservices.data.core.service.folder.Folder;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.Attachment;
import microsoft.exchange.webservices.data.property.complex.AttachmentCollection;
import microsoft.exchange.webservices.data.property.complex.EmailAddress;
import microsoft.exchange.webservices.data.property.complex.EmailAddressCollection;
import microsoft.exchange.webservices.data.property.complex.FileAttachment;
import microsoft.exchange.webservices.data.property.complex.ItemAttachment;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.ItemView;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;

@ApplicationScoped
@Named
public class EwsImporter {

    @Inject
    UserTransaction transaction;

    @Inject
    EntityManager entityManager;

    @Inject
    ImporterUtil importerUtil;

    public void performImport(EwsImporterConfigurationEntity configuration, @Nullable Progress<ImportResult> progress)
        throws SecurityException, IllegalStateException, SystemException {
        ImportResult result = new ImportResult();
        if (configuration.disabled) {
            Log.warnv("Cannot import from disabled configuration {0}", configuration.id);
            if (progress != null) {
                progress.message = "Configuration disabled";
                progress.completed = true;
                progress.success = false;
            }
            return;
        }

        Log.info("Starting EWS import");

        try (ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP2)) {
            transaction.begin();

            // First of all prepare the service with the necessary configuration
            service.setUrl(new URI(configuration.server));
            service.setCredentials(new WebCredentials(configuration.username, configuration.password));

            // Optionally set a proxy server
            if (StringUtils.isNotEmpty(configuration.proxyServer) && configuration.proxyPort != null) {
                service.setWebProxy(new WebProxy(configuration.proxyServer, configuration.proxyPort));
            }

            // Now open the inbox ...
            Folder inbox;
            Folder sentItems;
            try {
                inbox = Folder.bind(service, WellKnownFolderName.Inbox);
                sentItems = Folder.bind(service, WellKnownFolderName.SentItems);
            } catch (ServiceRequestException e) {
                configuration.currentFailedLogins = configuration.currentFailedLogins + 1;
                if (configuration.currentFailedLogins >= configuration.maxFailedLogins) {
                    configuration.disabled = true;
                }
                configuration.persist();
                transaction.commit();
                throw e;
            }

            // For unknown reasons filtering in exchange does not work anymore
            // SearchFilter searchFilter = new SearchFilter.IsEqualTo(ItemSchema.Categories, configuration.getCategory());
            if (configuration.lastImport != null) {
                // tagging e-mails in the past will result them missing in the import
                //searchFilter = new SearchFilter.SearchFilterCollection(LogicalOperator.And, searchFilter, new SearchFilter.IsGreaterThanOrEqualTo(ItemSchema.DateTimeSent, Date.from(configuration.getLastImport().minusDays(1).atZone(ZoneId.systemDefault()).toInstant())));
            }

            ItemView view = new ItemView(10);
            // sorting the result causes missing e-mails
            // view.getOrderBy().add(ItemSchema.DateTimeReceived, SortDirection.Ascending);

            if (progress != null) {
                progress.message = "Importing e-mails...";
                progress.max = 0;
            }

            readBox(progress, result, service, inbox, view, configuration);
            readBox(progress, result, service, sentItems, view, configuration);

            if (progress != null) {
                progress.message = "Imported " + progress.max + " e-mails";
                progress.completed = true;
                progress.success = true;
            }
            configuration.lastImport = LocalDateTime.now();
            configuration.persist();
            transaction.commit();
            if (progress != null) {
                progress.result = result;
            }
        } catch (Exception e) {
            if (progress != null) {
                progress.message = e.getMessage();
                progress.completed = true;
                progress.success = false;
            }
            Log.warn(e.getMessage(), e);
            transaction.rollback();
            throw new BadStateException(e.getMessage());
        } finally {
            if (progress != null) {
                Log.infov("EWS import completed {0}/{1}, {2}", progress.value, progress.max, progress.success);
            } else {
                Log.info("EWS import completed");
            }
        }
    }

    private void readBox(
        Progress<ImportResult> progress,
        ImportResult result,
        ExchangeService service,
        Folder box,
        ItemView view,
        EwsImporterConfigurationEntity configuration
    ) throws Exception {
        // Query server for its items
        FindItemsResults<Item> searchResult = service.findItems(box.getId(), view);

        // If this is a second run, increase the max value, otherwise set it (ugly from UX perspective, but OK for now)
        int i = 0;
        if (progress != null) {
            if (progress.max != null) {
                i = progress.max;
            }
            progress.max = i + searchResult.getTotalCount();
        }

        while (searchResult.isMoreAvailable()) {
            service.loadPropertiesForItems(searchResult, PropertySet.FirstClassProperties);

            if (progress != null) {
                Log.infov("Import in progress {0}/{1}", progress.value, progress.max);
            }

            for (Item item : searchResult.getItems()) {
                i++;
                if (progress != null) {
                    progress.value = i;
                    progress.message = "Importing e-mail " + i + "/" + progress.max;
                }

                if (i % 10 == 0) {
                    entityManager.flush();
                    transaction.commit();
                    entityManager.clear();
                    transaction.begin();
                    Log.debugv("Imported {0} customers", i);
                }

                EmailMessage email = (EmailMessage) item;

                // Check if the category matches
                if (!item.getCategories().contains(configuration.category)) {
                    Log.debugv(
                        "Skipping e-mail {0} as it is lacking category {1}",
                        email.getInternetMessageId(),
                        configuration.category
                    );
                    result.skipped++;
                    continue;
                }

                // Check if the mail was already imported
                if (TaskStatusEmailEntity.findByMessageId(email.getInternetMessageId()).count() > 0) {
                    Log.debugv("Skipping e-mail {0} as it was imported earlier", email.getInternetMessageId());
                    checkAttachments(email);
                    result.skipped++;
                    continue;
                }

                TaskStatusEmailEntity taskStatus = new TaskStatusEmailEntity();

                TaskEntity task = importerUtil.bootstrapOrFindExisting(
                    email.getInReplyTo(),
                    email.getInternetMessageId(),
                    email.getSubject()
                );
                if (task.id == null) {
                    result.created++;
                } else {
                    result.skipped++;
                }

                taskStatus.state = task.state;
                if (taskStatus.state == State.COMPLETED) {
                    taskStatus.state = State.IN_PROGRESS;
                }

                taskStatus.at = LocalDateTime.ofInstant(email.getDateTimeSent().toInstant(), ZoneId.systemDefault());
                taskStatus.created = taskStatus.at;
                taskStatus.attachments = toAttachments(email.getAttachments());
                taskStatus.body = TextNodeUtils.text(Jsoup.parse(email.getBody().toString()).body());
                taskStatus.messageId = email.getInternetMessageId();
                taskStatus.recipients = toEmailList(email.getToRecipients());
                taskStatus.recipientsCc = toEmailList(email.getCcRecipients());
                taskStatus.recipientsBcc = toEmailList(email.getBccRecipients());
                taskStatus.sender = toEmail(email.getSender());
                taskStatus.subject = email.getSubject();

                if (task.id == null) {
                    task.persist();
                }

                taskStatus.task = task;
                taskStatus.persist();
            }
            view.setOffset(view.getOffset() + view.getPageSize());
            searchResult = service.findItems(box.getId(), view);
        }
    }

    private void checkAttachments(EmailMessage email) throws ServiceLocalException {
        List<TaskStatusEmailEntity> existings = TaskStatusEmailEntity.findByMessageId(
            email.getInternetMessageId()
        ).list();
        if (existings.size() != 1) {
            Log.warnv("Found {0} messages for {1}, expected 1", existings.size(), email.getInternetMessageId());
        }

        boolean updatedContent = false;
        for (BinaryDataEntity binaryData : existings.getFirst().attachments) {
            if (binaryData.data == null) {
                for (Attachment attachment : email.getAttachments()) {
                    if (attachment instanceof FileAttachment) {
                        if (attachment.getName().equals(binaryData.filename)) {
                            try {
                                attachment.load();
                            } catch (Exception e) {
                                Log.warnv(
                                    "Skipping attachment {0}: {1}",
                                    new Object[] { attachment.getContentId(), e.getMessage() },
                                    e
                                );
                            }
                            binaryData.data = ((FileAttachment) attachment).getContent();
                            updatedContent = true;
                        }
                    }
                }
            }
        }

        if (updatedContent) {
            existings.getFirst().persist();
        }
    }

    private static List<EmailEntity> toEmailList(EmailAddressCollection recipients) {
        if (recipients == null) {
            return null;
        }
        List<EmailEntity> result = new ArrayList<>(recipients.getCount());
        for (EmailAddress recipient : recipients) {
            result.add(toEmail(recipient));
        }
        return result;
    }

    private static EmailEntity toEmail(EmailAddress recipient) {
        return new EmailEntity(recipient.getName(), recipient.getAddress());
    }

    private List<BinaryDataEntity> toAttachments(AttachmentCollection attachments) {
        if (attachments == null) {
            return null;
        }
        List<BinaryDataEntity> result = new ArrayList<>(attachments.getCount());
        for (Attachment attachment : attachments) {
            if (attachment instanceof FileAttachment fileAttachment) {
                try {
                    fileAttachment.load();
                } catch (Exception e) {
                    Log.warnv(
                        "Skipping attachment {0}: {1}",
                        new Object[] { attachment.getContentId(), e.getMessage() },
                        e
                    );
                    continue;
                }
                BinaryDataEntity binaryData = new BinaryDataEntity();
                binaryData.contentType = attachment.getContentType();
                binaryData.filename = attachment.getName();
                binaryData.data = fileAttachment.getContent();
                binaryData.persist();
                result.add(binaryData);
            } else if (attachment instanceof ItemAttachment itemAttachment) {
                Item item = itemAttachment.getItem();
                Log.warnv("Skipping item attachment of type {0}", item != null ? item.getClass() : "null");
            } else {
                Log.warnv("Skipping attachment of type {0}", attachment.getClass());
            }
        }
        return result;
    }
}
