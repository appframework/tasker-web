package de.christophbrill.tasker.util;

import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEmailEntity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

@ApplicationScoped
@Named
public class ImporterUtil {

    public TaskEntity boostrapTask(String subject) {
        TaskEntity task;
        task = new TaskEntity();
        task.title = subject;
        task.persist();
        return task;
    }

    public TaskEntity bootstrapOrFindExisting(String inReplyTo, String messageId, String subject) {
        if (StringUtils.isNotEmpty(inReplyTo)) {
            List<TaskStatusEmailEntity> existings = TaskStatusEmailEntity.findByMessageId(inReplyTo).list();
            Collection<TaskEntity> tasks = new HashSet<>();
            for (TaskStatusEmailEntity existing : existings) {
                tasks.add(existing.task);
            }
            if (!tasks.isEmpty()) {
                // One or more matching found, use first
                return tasks.iterator().next();
            }
        }

        if (StringUtils.isNotEmpty(messageId)) {
            List<TaskStatusEmailEntity> existings = TaskStatusEmailEntity.findByInReplyTo(messageId).list();
            Collection<TaskEntity> tasks = new HashSet<>();
            for (TaskStatusEmailEntity existing : existings) {
                tasks.add(existing.task);
            }
            if (!tasks.isEmpty()) {
                // One or more matching found, use first
                return tasks.iterator().next();
            }
        }

        return boostrapTask(subject);
    }
}
