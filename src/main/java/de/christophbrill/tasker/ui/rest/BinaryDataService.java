package de.christophbrill.tasker.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractBinaryDataService;
import jakarta.annotation.Nonnull;
import jakarta.ws.rs.Path;

@Path("/binary_data")
public class BinaryDataService extends AbstractBinaryDataService {

    @Override
    protected void nullAndPersistUsers(@Nonnull Long id) {
        // TODO clear relation to taskstatusemail
    }
}
