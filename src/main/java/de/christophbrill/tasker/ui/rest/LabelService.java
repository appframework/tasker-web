package de.christophbrill.tasker.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.tasker.persistence.model.LabelEntity;
import de.christophbrill.tasker.persistence.model.Permission;
import de.christophbrill.tasker.persistence.selector.LabelSelector;
import de.christophbrill.tasker.ui.dto.Label;
import de.christophbrill.tasker.util.mappers.LabelMapper;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

@Path("/label")
public class LabelService extends AbstractResourceService<Label, LabelEntity> {

    @Override
    protected Class<LabelEntity> getEntityClass() {
        return LabelEntity.class;
    }

    @Override
    protected LabelSelector getSelector() {
        return new LabelSelector(em);
    }

    @Override
    protected LabelMapper getMapper() {
        return LabelMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_LABELS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_LABELS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_LABELS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_LABELS.name();
    }

    @GET
    @Path("/colors")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getColors() {
        return em.createQuery("select distinct color from Label order by color", String.class)
                .getResultList();
    }

}
