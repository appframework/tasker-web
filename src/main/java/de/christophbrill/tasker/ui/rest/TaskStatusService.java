package de.christophbrill.tasker.ui.rest;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.tasker.persistence.model.AutomationEntity;
import de.christophbrill.tasker.persistence.model.Permission;
import de.christophbrill.tasker.persistence.model.TaskEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusGenericEntity;
import de.christophbrill.tasker.persistence.model.TaskStatusPersonEntity;
import de.christophbrill.tasker.persistence.selector.TaskStatusSelector;
import de.christophbrill.tasker.ui.dto.Assignee;
import de.christophbrill.tasker.ui.dto.Automation.MatchType;
import de.christophbrill.tasker.ui.dto.Task;
import de.christophbrill.tasker.ui.dto.TaskStatus;
import de.christophbrill.tasker.ui.dto.TaskStatusDue;
import de.christophbrill.tasker.util.TaskSelectorProducer;
import de.christophbrill.tasker.util.automations.AutomationProcessor;
import de.christophbrill.tasker.util.automations.PatternAutomationProcessor;
import de.christophbrill.tasker.util.mappers.TaskMapper;
import de.christophbrill.tasker.util.mappers.TaskStatusMapper;
import io.quarkus.logging.Log;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.apache.commons.lang3.StringUtils;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Path("/taskstatus")
public class TaskStatusService extends AbstractResourceService<TaskStatus, TaskStatusEntity> {

    @Inject
    TaskSelectorProducer taskSelectorProducer;

    @Override
    protected Class<TaskStatusEntity> getEntityClass() {
        return TaskStatusEntity.class;
    }

    @Override
    protected TaskStatusSelector getSelector() {
        return new TaskStatusSelector(em);
    }

    @Override
    protected TaskStatusMapper getMapper() {
        return TaskStatusMapper.INSTANCE;
    }

    @Override
    protected TaskStatusEntity mapCreate(TaskStatus taskStatus) {
        TaskStatusEntity entity = super.mapCreate(taskStatus);
        if (entity.state == null) {
            entity.state = entity.task.state;
        }
        return entity;
    }

    @GET
    @Path("/exclusionpatterns")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getExclusionPatterns() {
        List<AutomationEntity> automations = AutomationEntity.<AutomationEntity>findAll().list();
        List<String> result = new ArrayList<>(automations.size());
        for (AutomationEntity t : automations) {
            if (t.textMatch == MatchType.EQUALS_COMPLETE) {
                result.add('^' + t.expression.replace("+", "\\+") + '$');
            } else {
                result.add(t.expression);
            }
        }
        return result;
    }

    @Override
    protected void validate(TaskStatus dto, TaskStatusEntity entity) {
        super.validate(dto, entity);
        if (entity instanceof TaskStatusPersonEntity taskStatusPerson) {
            if (taskStatusPerson.assignee == null && StringUtils.isEmpty(taskStatusPerson.name)) {
                throw new BadArgumentException("Either assignee or name are required");
            }
        }
        if (entity instanceof TaskStatusGenericEntity genericEntity) {
            applyAutomations(genericEntity);
        }

    }

    @Override
    protected TaskStatusEntity mapUpdate(TaskStatus dto) {
        TaskStatusEntity entity = super.mapUpdate(dto);
        if (entity.state == null) {
            entity.state = entity.task.state;
        }
        return entity;
    }

    @GET
    @Path("/assignees/{input}")
    @Produces("application/json")
    @Consumes("application/json")
    public List<Assignee> getPossibleAssingees(@PathParam("input") String input) {
        List<Object[]> assignees = TaskStatusEntity.getAssignees(input);
        List<Assignee> result = new ArrayList<>(assignees.size());
        for (Object[] nameIdOccurrences : assignees) {
            Number id = (Number) nameIdOccurrences[1];
            Assignee assignee = new Assignee(id != null ? id.longValue() : null, (String) nameIdOccurrences[0]);
            if (!result.contains(assignee)) {
                result.add(assignee);
            } else {
                if (id != null) {
                    for (Assignee existing : result) {
                        if (existing.equals(assignee) && existing.assigneeId == null) {
                            existing.assigneeId = id.longValue();
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }

    private <T> T getInstance(AutomationEntity automation)
        throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException {
        String qualifiedName = automation.qualifiedName;
        byte[] compiled = automation.compiled;
        Class<? extends T> klass;
        if (compiled != null && compiled.length > 0) {
            klass = (Class<? extends T>) de.christophbrill.tasker.util.compiler.Compiler.load(
                    qualifiedName,
                    compiled
            );
        } else {
            klass = (Class<? extends T>) Class.forName(automation.qualifiedName);
        }
        return klass.getDeclaredConstructor().newInstance();
    }

    private void applyAutomations(TaskStatusGenericEntity status) {
        String comment = status.comment;

        List<AutomationEntity> automations = AutomationEntity.<AutomationEntity>findAll().list();
        for (AutomationEntity automation : automations) {
            String qualifiedName = automation.qualifiedName;
            assert qualifiedName != null;
            try {
                if (
                    automation.textMatch == MatchType.EQUALS_COMPLETE && comment.equalsIgnoreCase(automation.expression)
                ) {
                    AutomationProcessor instance = getInstance(automation);
                    instance.automate(status, this);
                } else if (automation.textMatch == MatchType.REGULAR_EXPRESSION) {
                    PatternAutomationProcessor instance = getInstance(automation);
                    Pattern compiledPattern = Pattern.compile(automation.expression);
                    Matcher matcher = compiledPattern.matcher(comment);
                    while (matcher.find()) {
                        instance.automate(status, matcher, this);
                    }
                }
            } catch (
                InstantiationException
                | IllegalAccessException
                | ClassNotFoundException
                | IllegalArgumentException
                | InvocationTargetException
                | NoSuchMethodException
                | SecurityException e
            ) {
                Log.infov(e, "Failed to execute plugin {0}", qualifiedName);
            }
        }
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_TASKS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_TASKS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_TASKS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_TASKS.name();
    }

    public static class BulkChange {

        public Collection<Long> taskIds;
        public TaskStatus status;
    }

    @POST
    @Consumes("application/json")
    @Path("/bulk")
    @Produces("application/json")
    @Transactional
    public List<Task> bulkChange(BulkChange bulkChange) {
        List<TaskEntity> tasks = taskSelectorProducer.getSelector().withIds(bulkChange.taskIds).findAll();
        for (TaskEntity task : tasks) {
            if (bulkChange.status instanceof TaskStatusDue) {
                ((TaskStatusDue) bulkChange.status).deadline = task.deadline;
            }
            bulkChange.status.taskId = task.id;
            create(bulkChange.status);
        }

        List<TaskEntity> entities = taskSelectorProducer.getSelector().withIds(bulkChange.taskIds).findAll();
        return TaskMapper.INSTANCE.mapEntitiesToDtos(entities);
    }
}
