package de.christophbrill.tasker.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.tasker.persistence.model.Permission;
import de.christophbrill.tasker.persistence.model.TextSplitEntity;
import de.christophbrill.tasker.persistence.selector.TextSplitSelector;
import de.christophbrill.tasker.ui.dto.TextSplit;
import de.christophbrill.tasker.util.mappers.TextSplitMapper;
import jakarta.ws.rs.Path;

@Path("/textsplit")
public class TextSplitService extends AbstractResourceService<TextSplit, TextSplitEntity> {

    @Override
    protected Class<TextSplitEntity> getEntityClass() {
        return TextSplitEntity.class;
    }

    @Override
    protected TextSplitSelector getSelector() {
        return new TextSplitSelector(em);
    }

    @Override
    protected TextSplitMapper getMapper() {
        return TextSplitMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_TEXTSPLITS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_TEXTSPLITS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_TEXTSPLITS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_TEXTSPLITS.name();
    }
}
