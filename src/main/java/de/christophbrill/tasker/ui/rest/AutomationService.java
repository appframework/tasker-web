package de.christophbrill.tasker.ui.rest;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.tasker.persistence.model.AutomationEntity;
import de.christophbrill.tasker.persistence.model.Permission;
import de.christophbrill.tasker.persistence.selector.AutomationSelector;
import de.christophbrill.tasker.ui.dto.Automation;
import de.christophbrill.tasker.util.mappers.AutomationMapper;
import jakarta.ws.rs.Path;

@Path("/automation")
public class AutomationService extends AbstractResourceService<Automation, AutomationEntity> {

    @Override
    protected Class<AutomationEntity> getEntityClass() {
        return AutomationEntity.class;
    }

    @Override
    protected AutomationSelector getSelector() {
        return new AutomationSelector(em);
    }

    @Override
    protected AutomationMapper getMapper() {
        return AutomationMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_AUTOMATIONS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_AUTOMATIONS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_AUTOMATIONS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_AUTOMATIONS.name();
    }
}
