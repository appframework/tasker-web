package de.christophbrill.tasker.ui.rest;

import de.christophbrill.appframework.ui.async.ProgressCache;
import de.christophbrill.appframework.ui.dto.ImportResult;
import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.tasker.persistence.model.ImporterConfigurationEntity;
import de.christophbrill.tasker.persistence.model.Permission;
import de.christophbrill.tasker.persistence.selector.ImporterConfigurationSelector;
import de.christophbrill.tasker.ui.dto.ImporterConfiguration;
import de.christophbrill.tasker.util.ImporterJob;
import de.christophbrill.tasker.util.mappers.ImporterConfigurationMapper;
import io.quarkus.logging.Log;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.infrastructure.Infrastructure;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/importerconfiguration")
public class ImporterConfigurationService
    extends AbstractResourceService<ImporterConfiguration, ImporterConfigurationEntity> {

    @Inject
    ImporterJob importerJob;
    @Inject
    ProgressCache cache;

    protected Class<ImporterConfigurationEntity> getEntityClass() {
        return ImporterConfigurationEntity.class;
    }

    protected ImporterConfigurationSelector getSelector() {
        return new ImporterConfigurationSelector(em);
    }

    protected ImporterConfigurationMapper getMapper() {
        return ImporterConfigurationMapper.INSTANCE;
    }

    protected String getCreatePermission() {
        return Permission.ADMIN_IMPORTERCONFIGURATIONS.name();
    }

    protected String getReadPermission() {
        return Permission.SHOW_IMPORTERCONFIGURATIONS.name();
    }

    protected String getUpdatePermission() {
        return Permission.ADMIN_IMPORTERCONFIGURATIONS.name();
    }

    protected String getDeletePermission() {
        return Permission.ADMIN_IMPORTERCONFIGURATIONS.name();
    }

    @GET
    @Path("/{id}/import")
    @Produces(MediaType.TEXT_PLAIN)
    public String performImport(@PathParam("id") Long id) {
        var configuration = ImporterConfigurationEntity.<ImporterConfigurationEntity>findById(id);
        if (configuration == null) {
            throw new BadArgumentException("Invalid configuration ID specified");
        }
        Progress<ImportResult> progress = cache.createProgress();
        Log.tracev("Sending event to start progress for {0}", progress.key);

        Uni.createFrom()
            .item(() -> progress.key)
            .emitOn(Infrastructure.getDefaultWorkerPool())
            .subscribe()
            .with(uuid -> importerJob.runImport(configuration, progress), Throwable::printStackTrace);

        return progress.key;
    }
}
