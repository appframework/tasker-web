package de.christophbrill.tasker.ui.rest;

import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.exceptions.NullArgumentException;
import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.tasker.persistence.model.*;
import de.christophbrill.tasker.persistence.selector.TaskSelector;
import de.christophbrill.tasker.ui.dto.Label;
import de.christophbrill.tasker.ui.dto.State;
import de.christophbrill.tasker.ui.dto.Task;
import de.christophbrill.tasker.ui.dto.TaskStatusFile;
import de.christophbrill.tasker.util.TaskSelectorProducer;
import de.christophbrill.tasker.util.TextNodeUtils;
import de.christophbrill.tasker.util.mappers.LabelMapper;
import de.christophbrill.tasker.util.mappers.TaskMapper;
import de.christophbrill.tasker.util.mappers.TaskStatusMapper;
import io.quarkus.logging.Log;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;

@Path("/task")
public class TaskService extends AbstractResourceService<Task, TaskEntity> {

    @Inject
    TaskSelectorProducer taskSelectorProducer;

    @Override
    protected Class<TaskEntity> getEntityClass() {
        return TaskEntity.class;
    }

    @Override
    protected TaskSelector getSelector() {
        return taskSelectorProducer.getSelector();
    }

    @Override
    protected TaskMapper getMapper() {
        return TaskMapper.INSTANCE;
    }

    @Transactional
    public Task getById(Long id) {
        checkReadPermission();
        if (id == null) {
            throw new NullArgumentException("id");
        }
        TaskEntity find = TaskEntity.findById(id);
        if (find == null) {
            return null;
        }
        find.status
            .stream()
            .filter(TaskStatusEmailEntity.class::isInstance)
            .forEach(t -> {
                TaskStatusEmailEntity email = (TaskStatusEmailEntity) t;
                String body = email.body;
                if (body != null) {
                    if (
                        body.contains("http://schemas.microsoft.com/office/2004/12/omml") ||
                        body.contains("<!--[if gte mso 9]>") ||
                        body.contains("class=\"Apple-interchange-newline\"") ||
                        body.startsWith("<html") ||
                        body.startsWith("<!DOCTYPE html")
                    ) {
                        body = TextNodeUtils.text(Jsoup.parse(body).body());
                    }
                    body = body.replace("\r\n", "\n");
                    body = body.replace("\n\n", "\n");
                    email.body = body;
                    email.persist();
                }
            });
        // Assure we never delete anything initially
        String previous = UUID.randomUUID().toString();
        for (TaskStatusEntity t : find.status) {
            if (t instanceof TaskStatusEmailEntity tse) {
                // Grab the body and delete the matching previous content
                String body = tse.body;
                String replaced = body.replace(previous, "");

                // If we removed content, update the entity
                if (replaced.length() != body.length()) {
                    tse.body = replaced;
                    t.persist();
                }

                // Keep the original content as it might occur on the next email
                previous = body;
            }
        }
        return TaskMapper.INSTANCE.mapEntityToDto(find);
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_TASKS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_TASKS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_TASKS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_TASKS.name();
    }

    @GET
    @Path("/merge/{x}/into/{y}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Task merge(@PathParam("x") long x, @PathParam("y") long y) {
        checkUpdatePermission();
        if (x == y) {
            throw new BadArgumentException("'from' and 'to' must be different");
        }

        TaskEntity from = TaskEntity.findById(x);
        TaskEntity to = TaskEntity.findById(y);
        for (TaskStatusEntity taskStatus : from.status) {
            to.status.add(taskStatus);
            taskStatus.task = to;
        }

        from.status.clear();
        to.persist();
        from.persist();

        from.delete();

        return TaskMapper.INSTANCE.mapEntityToDto(to);
    }

    @GET
    @Path("/assigned")
    @Produces({ "application/json" })
    public List<Task> getAssignedTasks(@QueryParam("search") String search) {
        checkReadPermission();
        UserEntity user = taskSelectorProducer.loadUser(identity.getPrincipal());

        List<TaskEntity> entities = getSelector()
            .withSearch(search)
            .withAssignee(user)
            .withFollowupBeforeOrDeadline(LocalDateTime.now())
            .withoutState(State.COMPLETED)
            .withSortColumn("deadline", false)
            .withSortColumn("due", true)
            .withSortColumn("title", true)
            .findAll();

        return TaskMapper.INSTANCE.mapEntitiesToDtos(entities);
    }

    @GET
    @Path("/overdue")
    @Produces({ "application/json" })
    public List<Task> getOverdueTasks(@QueryParam("search") String search) {
        checkReadPermission();
        UserEntity user = taskSelectorProducer.loadUser(identity.getPrincipal());

        List<TaskEntity> entities = getSelector()
            .withSearch(search)
            .withoutAssignee(user)
            .withFollowupBeforeOrDeadline(LocalDateTime.now())
            .withoutState(State.COMPLETED)
            .withSortColumn("deadline", false)
            .withSortColumn("due", true)
            .withSortColumn("title", true)
            .findAll();

        return TaskMapper.INSTANCE.mapEntitiesToDtos(entities);
    }

    @POST
    @Path("/{id}/addlabel")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @Transactional
    public Label addLabel(Label label, @PathParam("id") long taskId) {
        checkUpdatePermission();
        LabelEntity labelEntity;
        if (label.id == null) {
            if (StringUtils.isEmpty(label.name)) {
                throw new BadArgumentException("No label ID or name given");
            }
            labelEntity = LabelEntity.<LabelEntity>find(
                "select l from Label l where l.name = ?1",
                label.name
            ).firstResult();
            if (labelEntity == null) {
                labelEntity = new LabelEntity();
                labelEntity.color = "#000000";
                labelEntity.name = label.name;
                labelEntity.persist();
            }
        } else {
            labelEntity = LabelEntity.findById(label.id);
            if (labelEntity == null) {
                throw new BadArgumentException("No label with given ID");
            }
        }

        TaskEntity task = TaskEntity.findById(taskId);
        if (task == null) {
            throw new BadArgumentException("No label with given ID");
        }

        task.labels.add(labelEntity);
        task.persist();
        return LabelMapper.INSTANCE.mapEntityToDto(labelEntity);
    }

    @POST
    @Consumes({ "application/json" })
    @Path("/{id}/removelabel")
    @Produces({ "application/json" })
    @Transactional
    public void removeLabel(long labelId, @PathParam("id") long taskId) {
        checkUpdatePermission();
        LabelEntity labelEntity = LabelEntity.findById(labelId);
        if (labelEntity == null) {
            throw new BadArgumentException("No label with given ID");
        }

        TaskEntity task = TaskEntity.findById(taskId);
        if (task == null) {
            throw new BadArgumentException("No task with given ID");
        }

        task.labels.remove(labelEntity);
        task.persist();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}/upload")
    @Transactional
    public TaskStatusFile uploadFile(@PathParam("id") long taskId,
                                     @RestForm String filename,
                                     @RestForm("file") FileUpload file) {
        checkUpdatePermission();
        TaskEntity task = TaskEntity.findById(taskId);

        if (task == null) {
            throw new BadArgumentException("No task with given ID");
        }

        BinaryDataEntity binaryData = new BinaryDataEntity();
        try {
            binaryData.filename = filename;
            byte[] bytes = Files.readAllBytes(file.uploadedFile());
            binaryData.size = bytes.length;
            binaryData.data = bytes;
            binaryData.contentType = URLConnection.guessContentTypeFromName(filename);
            binaryData.persist();

            TaskStatusFileEntity taskStatusFileEntity = new TaskStatusFileEntity();
            taskStatusFileEntity.task = task;
            taskStatusFileEntity.file = binaryData;
            taskStatusFileEntity.persistAndFlush();

            return (TaskStatusFile) TaskStatusMapper.INSTANCE.mapEntityToDto(taskStatusFileEntity);
        } catch (IOException e) {
            throw new BadArgumentException("Could not read binary data: " + e.getMessage());
        }
    }

    @GET
    @Transactional
    @Path("maintenance")
    public void maintenance() {
        checkUpdatePermission();
        for (var task : TaskEntity.<TaskEntity>listAll()) {
            var modified = false;
            for (var status : task.status.reversed()) {
                if (status instanceof TaskStatusDueEntity) {
                    if (task.due.equals(((TaskStatusDueEntity) status).due)) {
                        Log.infof("Had to update due date of task %d from %s to %s", task.id, task.due, ((TaskStatusDueEntity) status).due);
                        task.due = ((TaskStatusDueEntity) status).due;
                        modified = true;
                    }
                    break;
                }
            }
            if (!task.status.isEmpty()) {
                var last = task.status.getLast();
                if (last != null) {
                    task.state = last.state;
                    modified = true;
                }
            }
            if (modified) {
                task.persist();
            }
        }
    }
}
