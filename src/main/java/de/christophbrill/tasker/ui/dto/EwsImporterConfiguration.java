package de.christophbrill.tasker.ui.dto;

public class EwsImporterConfiguration extends ImporterConfiguration {

    public String category;
    public String proxyServer;
    public Integer proxyPort;
}
