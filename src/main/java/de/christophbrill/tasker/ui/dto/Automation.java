package de.christophbrill.tasker.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Automation extends AbstractDto {

    public enum MatchType {
        EQUALS_COMPLETE,
        REGULAR_EXPRESSION,
    }

    public String qualifiedName;
    public String expression;
    public MatchType textMatch;
    public String source;
    public boolean compiled;
    public boolean builtin;
}
