package de.christophbrill.tasker.ui.dto;

import jakarta.annotation.Nonnull;
import java.util.Objects;

public class Assignee implements Comparable<Assignee> {

    public Long assigneeId;
    public String name;

    public Assignee(Long assigneeId, String name) {
        this.assigneeId = assigneeId;
        this.name = name;
    }

    @Override
    public int compareTo(@Nonnull Assignee assignee) {
        return name.compareTo(assignee.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Assignee assignee = (Assignee) o;
        return Objects.equals(name, assignee.name);
    }
}
