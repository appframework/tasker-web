package de.christophbrill.tasker.ui.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import de.christophbrill.appframework.ui.dto.AbstractDto;
import java.time.LocalDateTime;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes(
    {
        @JsonSubTypes.Type(value = EwsImporterConfiguration.class, name = "EWS"),
        @JsonSubTypes.Type(value = PopImporterConfiguration.class, name = "POP"),
    }
)
public abstract class ImporterConfiguration extends AbstractDto {

    public String server;
    public String username;
    public String password;
    public int maxFailedLogins;
    public int currentFailedLogins;
    public boolean disabled;
    public LocalDateTime lastImport;
}
