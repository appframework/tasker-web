package de.christophbrill.tasker.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;
import java.time.LocalDateTime;
import java.util.List;

public class Task extends AbstractDto {

    public String title;
    public List<TaskStatus> status;
    public LocalDateTime due;
    public boolean deadline;
    public String assignee;
    public State state;
    public List<Label> labels;
    public Priority priority;
    public List<TaskStatusReference> referencedBy;
}
