package de.christophbrill.tasker.ui.dto;

import java.time.LocalDateTime;

public class TaskStatusEffort extends TaskStatus {

    public LocalDateTime at;
    /** In minutes */
    public int duration;
}
