package de.christophbrill.tasker.ui.dto;

import de.christophbrill.appframework.ui.dto.BinaryData;
import java.time.LocalDateTime;
import java.util.List;

public class TaskStatusEmail extends TaskStatus {

    public Email sender;
    public LocalDateTime at;
    public List<Email> recipients;
    public List<Email> recipientsCc;
    public List<Email> recipientsBcc;
    public String subject;
    public String body;
    public String messageId;
    public String inReplyTo;
    public List<BinaryData> attachments;
}
