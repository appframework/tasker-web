package de.christophbrill.tasker.ui.dto;

public enum State {
    NEW,
    IN_PROGRESS,
    COMPLETED,
}
