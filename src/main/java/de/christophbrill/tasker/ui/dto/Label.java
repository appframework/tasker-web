package de.christophbrill.tasker.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Label extends AbstractDto {

    public String name;
    public String color;
}
