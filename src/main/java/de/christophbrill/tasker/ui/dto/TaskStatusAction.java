package de.christophbrill.tasker.ui.dto;

public class TaskStatusAction extends TaskStatus {

    public String title;
    public boolean completed;
}
