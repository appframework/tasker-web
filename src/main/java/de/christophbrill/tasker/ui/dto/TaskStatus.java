package de.christophbrill.tasker.ui.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import de.christophbrill.appframework.ui.dto.AbstractDto;
import java.time.LocalDateTime;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes(
    {
        @JsonSubTypes.Type(value = TaskStatusAction.class, name = "action"),
        @JsonSubTypes.Type(value = TaskStatusDue.class, name = "due"),
        @JsonSubTypes.Type(value = TaskStatusGeneric.class, name = "generic"),
        @JsonSubTypes.Type(value = TaskStatusPerson.class, name = "person"),
        @JsonSubTypes.Type(value = TaskStatusEmail.class, name = "email"),
        @JsonSubTypes.Type(value = TaskStatusPriority.class, name = "priority"),
        @JsonSubTypes.Type(value = TaskStatusFile.class, name = "file"),
        @JsonSubTypes.Type(value = TaskStatusReference.class, name = "reference"),
        @JsonSubTypes.Type(value = TaskStatusEffort.class, name = "effort"),
    }
)
public abstract class TaskStatus extends AbstractDto {

    public Long taskId;
    public State state;
    public LocalDateTime created;
    public String createdBy;
}
