package de.christophbrill.tasker.ui.dto;

public enum Priority {
    HIGH,
    MEDIUM,
    LOW,
}
