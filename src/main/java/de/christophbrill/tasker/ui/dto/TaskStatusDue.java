package de.christophbrill.tasker.ui.dto;

import java.time.LocalDateTime;

public class TaskStatusDue extends TaskStatus {

    public LocalDateTime due;
    public boolean deadline;
}
