package de.christophbrill.tasker.ui.dto;

public class TaskStatusReference extends TaskStatus {

    public enum ReferenceType {
        PART_OF,
        RELATES_TO,
    }

    public Task reference;
    public ReferenceType relationType;
}
