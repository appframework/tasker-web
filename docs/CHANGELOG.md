## [1.1.42](https://gitlab.com/appframework/tasker-web/compare/1.1.41...1.1.42) (2025-03-14)


### Bug Fixes

* **deps:** update dependency primelocale to v2.1.1 ([0921011](https://gitlab.com/appframework/tasker-web/commit/09210114ccc97fd34f347ba91d56286f5a8b263c))
* **deps:** update primevue and friends to v4.3.2 ([447b5d8](https://gitlab.com/appframework/tasker-web/commit/447b5d89cd961fe9b4f3e533527c740f3fb49baa))
* **deps:** update quarkus.platform.version to v3.19.3 ([c5fd18a](https://gitlab.com/appframework/tasker-web/commit/c5fd18a53831ee3c776e3ea5dac5a700eda60027))

## [1.1.41](https://gitlab.com/appframework/tasker-web/compare/1.1.40...1.1.41) (2025-03-11)


### Bug Fixes

* **deps:** update quinoa.version to v2.5.3 ([7658efa](https://gitlab.com/appframework/tasker-web/commit/7658efa9982f424681375f86241b6b25f3f21689))

## [1.1.40](https://gitlab.com/appframework/tasker-web/compare/1.1.39...1.1.40) (2025-03-11)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v13 ([40c7f35](https://gitlab.com/appframework/tasker-web/commit/40c7f35c176ae87ec9134503451f6dbbc21f61f6))
* **deps:** update dependency bootstrap-vue-next to ^0.28.0 ([7eea17f](https://gitlab.com/appframework/tasker-web/commit/7eea17f7301db7bddd796df52dd9bfcd017a0fb9))
* **deps:** update dependency org.jsoup:jsoup to v1.19.1 ([684054f](https://gitlab.com/appframework/tasker-web/commit/684054f3ee31d57be63709cf1007839dd3490999))
* **deps:** update dependency primelocale to v2 ([90ae145](https://gitlab.com/appframework/tasker-web/commit/90ae145c719d95188c61ae640f0e0201d3449152))
* **deps:** update quarkus.platform.version to v3.19.2 ([7c05426](https://gitlab.com/appframework/tasker-web/commit/7c054260a62cbc6b68d34f5fc79e9217d62e14da))

## [1.1.39](https://gitlab.com/appframework/tasker-web/compare/1.1.38...1.1.39) (2025-02-26)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.28 ([432caf1](https://gitlab.com/appframework/tasker-web/commit/432caf1163b19cc28525693c1304fe6cb2d5b06a))
* **deps:** update dependency bootstrap-vue-next to v0.26.30 ([f92921d](https://gitlab.com/appframework/tasker-web/commit/f92921d829fca662d95d40f270046de6b7e224d3))
* **deps:** update quarkus.platform.version to v3.18.3 ([0c65f50](https://gitlab.com/appframework/tasker-web/commit/0c65f50721373c36f00516cb6fa8b883d4b8ebc1))

## [1.1.38](https://gitlab.com/appframework/tasker-web/compare/1.1.37...1.1.38) (2025-02-16)


### Bug Fixes

* Add ability to fix broken database ([150c3d9](https://gitlab.com/appframework/tasker-web/commit/150c3d9a5300136f7639a9a39a3355c93f358f40))

## [1.1.37](https://gitlab.com/appframework/tasker-web/compare/1.1.36...1.1.37) (2025-02-14)

## [1.1.36](https://gitlab.com/appframework/tasker-web/compare/1.1.35...1.1.36) (2025-02-13)


### Bug Fixes

* Add missing task state for references ([6f85f8e](https://gitlab.com/appframework/tasker-web/commit/6f85f8e582e564dfdd445ee1695fb0054f67ed22))

## [1.1.35](https://gitlab.com/appframework/tasker-web/compare/1.1.34...1.1.35) (2025-02-12)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.26 ([80351ff](https://gitlab.com/appframework/tasker-web/commit/80351ff5f420cfb1c44aff70f9a97cfae5d41dcb))
* **deps:** update dependency primelocale to v1.6.0 ([d3de9e3](https://gitlab.com/appframework/tasker-web/commit/d3de9e3c0b47b614fefe280eb82eeeb4ccf6c949))
* **deps:** update dependency vue-i18n to v11.1.1 ([748b075](https://gitlab.com/appframework/tasker-web/commit/748b075568fb87cb88c07b5b0ec9ea4a95483b60))
* **deps:** update quarkus.platform.version to v3.18.2 ([69809ef](https://gitlab.com/appframework/tasker-web/commit/69809ef27926e92d2547f544527e00e7903a8691))

## [1.1.34](https://gitlab.com/appframework/tasker-web/compare/1.1.33...1.1.34) (2025-02-10)

## [1.1.33](https://gitlab.com/appframework/tasker-web/compare/1.1.32...1.1.33) (2025-02-03)


### Bug Fixes

* **deps:** update dependency vue-i18n to v11.1.0 ([d58cd86](https://gitlab.com/appframework/tasker-web/commit/d58cd867b485436b63cd2ac274fe6d9f4b4bed66))
* **deps:** update quarkus.platform.version to v3.18.1 ([e2e2aa7](https://gitlab.com/appframework/tasker-web/commit/e2e2aa7b7622ba9e63d8c6f5a725fd6748c033c0))

## [1.1.32](https://gitlab.com/appframework/tasker-web/compare/1.1.31...1.1.32) (2025-01-28)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.22 ([4767b0d](https://gitlab.com/appframework/tasker-web/commit/4767b0d0704e2edb20112bc9e4520833bb58971d))
* **deps:** update dependency primelocale to v1.5.0 ([3f830c6](https://gitlab.com/appframework/tasker-web/commit/3f830c68736bd8528e24c9a560db4f69eca22ec9))

## [1.1.31](https://gitlab.com/appframework/tasker-web/compare/1.1.30...1.1.31) (2025-01-27)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.5.0 ([548f660](https://gitlab.com/appframework/tasker-web/commit/548f66018df7704bbaf13f202c640e093e3751d2))
* **deps:** update dependency bootstrap-vue-next to v0.26.21 ([c177fec](https://gitlab.com/appframework/tasker-web/commit/c177fec26ea9b7356268ab9c37e1cab948e58d51))
* **deps:** update quarkus.platform.version to v3.18.0 ([4b85f2c](https://gitlab.com/appframework/tasker-web/commit/4b85f2c0625087296656748201cd8d1e0ad1c294))

## [1.1.30](https://gitlab.com/appframework/tasker-web/compare/1.1.29...1.1.30) (2025-01-22)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.20 ([26fa8c7](https://gitlab.com/appframework/tasker-web/commit/26fa8c76d21fa9efe316148ce0953618ea85bd89))
* **deps:** update dependency primelocale to v1.4.0 ([07a80d6](https://gitlab.com/appframework/tasker-web/commit/07a80d6c1324487bd2d3ec815d5a59165865e07d))
* **deps:** update quarkus.platform.version to v3.17.7 ([4d73400](https://gitlab.com/appframework/tasker-web/commit/4d7340057762af85a6e2baaa9cd54a655ac825c2))

## [1.1.29](https://gitlab.com/appframework/tasker-web/compare/1.1.28...1.1.29) (2025-01-15)

## [1.1.28](https://gitlab.com/appframework/tasker-web/compare/1.1.27...1.1.28) (2025-01-15)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.4.0 ([5b7e17c](https://gitlab.com/appframework/tasker-web/commit/5b7e17c8caabeedc1fdb4638ffc1dbbde590651b))
* **deps:** update dependency primelocale to v1.3.0 ([d366a73](https://gitlab.com/appframework/tasker-web/commit/d366a73b06aeab86fea183a376ea878bae078d49))
* **deps:** update quarkus.platform.version to v3.17.6 ([621f170](https://gitlab.com/appframework/tasker-web/commit/621f170f91316728e84ad8a67c4cc8b4b6af123b))

## [1.1.27](https://gitlab.com/appframework/tasker-web/compare/1.1.26...1.1.27) (2025-01-09)


### Bug Fixes

* **deps:** update dependency vue-i18n to v11 ([78e1f65](https://gitlab.com/appframework/tasker-web/commit/78e1f65ac4e2354253fd4495f982b37aabecad6d))

## [1.1.26](https://gitlab.com/appframework/tasker-web/compare/1.1.25...1.1.26) (2025-01-08)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.3.0 ([53e3e64](https://gitlab.com/appframework/tasker-web/commit/53e3e64c871111b21062d4d1c42424db9eaa17a7))
* **deps:** update dependency bootstrap-vue-next to v0.26.19 ([4065a6e](https://gitlab.com/appframework/tasker-web/commit/4065a6e7c7034cf9f34f33474494deef87dce458))
* **deps:** update dependency primelocale to v1.2.3 ([d680410](https://gitlab.com/appframework/tasker-web/commit/d680410d88f04dbb630d28e479001648f0d54222))

## [1.1.25](https://gitlab.com/appframework/tasker-web/compare/1.1.24...1.1.25) (2024-12-31)

## [1.1.24](https://gitlab.com/appframework/tasker-web/compare/1.1.23...1.1.24) (2024-12-25)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.18 ([5dce9a7](https://gitlab.com/appframework/tasker-web/commit/5dce9a79e99282dae5f64a3f3403a94842c6e914))
* **deps:** update quarkus.platform.version to v3.17.5 ([ddd4767](https://gitlab.com/appframework/tasker-web/commit/ddd47670b4d1e71733d65b6c968da8216e7e8301))

## [1.1.23](https://gitlab.com/appframework/tasker-web/compare/1.1.22...1.1.23) (2024-12-18)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.15 ([009fd8e](https://gitlab.com/appframework/tasker-web/commit/009fd8ec48a0551cbbd21048e303e26f7ec43768))

## [1.1.22](https://gitlab.com/appframework/tasker-web/compare/1.1.21...1.1.22) (2024-12-13)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.14 ([dbb0c7a](https://gitlab.com/appframework/tasker-web/commit/dbb0c7a4f1a92221b55bfeb21b9813a7d497da93))

## [1.1.21](https://gitlab.com/appframework/tasker-web/compare/1.1.20...1.1.21) (2024-12-11)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12 ([c1370de](https://gitlab.com/appframework/tasker-web/commit/c1370dea81773f1ef0165781e7bbf32b4db7e904))

## [1.1.20](https://gitlab.com/appframework/tasker-web/compare/1.1.19...1.1.20) (2024-12-11)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.11 ([4c2ab26](https://gitlab.com/appframework/tasker-web/commit/4c2ab2688913de154a4c1938bb8b5a0ddfa1785c))

## [1.1.19](https://gitlab.com/appframework/tasker-web/compare/1.1.18...1.1.19) (2024-12-06)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v11.3.0 ([49ffc74](https://gitlab.com/appframework/tasker-web/commit/49ffc740eb4a1bd061643c9b99afa70d7958f518))
* **deps:** update dependency vue-router to v4.5.0 ([c19d48f](https://gitlab.com/appframework/tasker-web/commit/c19d48fe1daff7c438c812c9ee2ccbf2fd04d097))

## [1.1.18](https://gitlab.com/appframework/tasker-web/compare/1.1.17...1.1.18) (2024-12-05)

## [1.1.17](https://gitlab.com/appframework/tasker-web/compare/1.1.16...1.1.17) (2024-12-04)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.8 ([ecb8d95](https://gitlab.com/appframework/tasker-web/commit/ecb8d95e9fe9d7626fdcb5871e4b7103d37674b2))
* **deps:** update dependency org.jsoup:jsoup to v1.18.3 ([106a73f](https://gitlab.com/appframework/tasker-web/commit/106a73f081a1b6cd15fa8f2e825fde9d15959534))
* **deps:** update dependency vue-i18n to v10.0.5 ([333fdb3](https://gitlab.com/appframework/tasker-web/commit/333fdb3218183d206bdabceef06248aa374c5b3e))

## [1.1.16](https://gitlab.com/appframework/tasker-web/compare/1.1.15...1.1.16) (2024-11-30)

## [1.1.15](https://gitlab.com/appframework/tasker-web/compare/1.1.14...1.1.15) (2024-11-28)

## [1.1.14](https://gitlab.com/appframework/tasker-web/compare/1.1.13...1.1.14) (2024-11-27)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.5 ([2f70625](https://gitlab.com/appframework/tasker-web/commit/2f70625cc733d9dd9e537aa7e91cfea141d001aa))
* **deps:** update dependency primelocale to v1.2.2 ([56ea63d](https://gitlab.com/appframework/tasker-web/commit/56ea63d87f4d75018bf70e9f0cbd46f1ff376939))

## [1.1.13](https://gitlab.com/appframework/tasker-web/compare/1.1.12...1.1.13) (2024-11-20)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to ^0.26.0 ([b9789e2](https://gitlab.com/appframework/tasker-web/commit/b9789e2c1aa6573ab705908dd0e24cc2f4a67598))
* **deps:** update dependency primelocale to v1.2.0 ([ccc4d2c](https://gitlab.com/appframework/tasker-web/commit/ccc4d2c532d1e3d70e20c0c06bb038256e8d5924))
* **deps:** update dependency vue to v3.5.13 ([71c5c02](https://gitlab.com/appframework/tasker-web/commit/71c5c02170a8fcea40d77e3d0d9dbba85c880d57))

## [1.1.12](https://gitlab.com/appframework/tasker-web/compare/1.1.11...1.1.12) (2024-11-15)


### Bug Fixes

* **deps:** update dependency primelocale to v1.1.1 ([3733542](https://gitlab.com/appframework/tasker-web/commit/3733542af5b8c0e74a399c2617b1f5244d2fec71))

## [1.1.11](https://gitlab.com/appframework/tasker-web/compare/1.1.10...1.1.11) (2024-11-14)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.14 ([874151a](https://gitlab.com/appframework/tasker-web/commit/874151a2d415d4d2af9582360a333272a9dd6fd3))

## [1.1.10](https://gitlab.com/appframework/tasker-web/compare/1.1.9...1.1.10) (2024-11-07)

## [1.1.9](https://gitlab.com/appframework/tasker-web/compare/1.1.8...1.1.9) (2024-11-06)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v11.2.0 ([03679d8](https://gitlab.com/appframework/tasker-web/commit/03679d80f1fdbbcadca59123dd53f7befe15843e))
* **deps:** update dependency bootstrap-vue-next to v0.25.13 ([dfad32c](https://gitlab.com/appframework/tasker-web/commit/dfad32c48db4c23ede309a8d45b7a7e9c4063b73))

## [1.1.8](https://gitlab.com/appframework/tasker-web/compare/1.1.7...1.1.8) (2024-11-01)

## [1.1.7](https://gitlab.com/appframework/tasker-web/compare/1.1.6...1.1.7) (2024-10-29)

## [1.1.6](https://gitlab.com/appframework/tasker-web/compare/1.1.5...1.1.6) (2024-10-29)

## [1.1.5](https://gitlab.com/appframework/tasker-web/compare/1.1.4...1.1.5) (2024-10-28)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.10 ([06a557e](https://gitlab.com/appframework/tasker-web/commit/06a557e2cf8cc6e7e2d966e7461d2e9934606f46))

## [1.1.4](https://gitlab.com/appframework/tasker-web/compare/1.1.3...1.1.4) (2024-10-28)

## [1.1.3](https://gitlab.com/appframework/tasker-web/compare/1.1.2...1.1.3) (2024-10-25)


### Bug Fixes

* Copy&pasto ([c2a7091](https://gitlab.com/appframework/tasker-web/commit/c2a709123076b050db7c5520d3c8495d1faa9951))

## [1.1.2](https://gitlab.com/appframework/tasker-web/compare/1.1.1...1.1.2) (2024-10-24)

## [1.1.1](https://gitlab.com/appframework/tasker-web/compare/1.1.0...1.1.1) (2024-10-23)

# [1.1.0](https://gitlab.com/appframework/tasker-web/compare/1.0.9...1.1.0) (2024-10-22)


### Bug Fixes

* Add ability to exclude labels from search ([fe7a545](https://gitlab.com/appframework/tasker-web/commit/fe7a54581388281e4b97c48d09c12e39b1a2beb8))
* Add missing translations ([4ed6f0b](https://gitlab.com/appframework/tasker-web/commit/4ed6f0b6503d8d0c8cb0a8c5da19fbf1257d12e5))
* Apply automations during validation phase ([06bbe78](https://gitlab.com/appframework/tasker-web/commit/06bbe7802d141e7f9e84507432e0e3ef0f831c24))
* **deps:** update dependency bootstrap-vue-next to v0.25.7 ([db79e6e](https://gitlab.com/appframework/tasker-web/commit/db79e6ef42f3e8eb7b9cff52a8cf6d791daffbcd))
* Fix dependencies of webapp ([82ae9d2](https://gitlab.com/appframework/tasker-web/commit/82ae9d232113c8dfe6bc0a0c61b5e04a619e8551))
* Properly return regular expressions ([65ced1c](https://gitlab.com/appframework/tasker-web/commit/65ced1c1fb4af99a526f0edf2c9f91745721703b))
* Remove double-escaping regular expression ([ee27994](https://gitlab.com/appframework/tasker-web/commit/ee27994783f1f3c104557ebfc3139a8f1e55d792))


### Features

* Add history view of a task ([67f0690](https://gitlab.com/appframework/tasker-web/commit/67f06901cd5d120d3ba79c91e54737eaddf05743))

## [1.0.9](https://gitlab.com/appframework/tasker-web/compare/1.0.8...1.0.9) (2024-10-15)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.25.2 ([8c39819](https://gitlab.com/appframework/tasker-web/commit/8c3981909a13aacb6c968e87ffafcd68c807aa32))

## [1.0.8](https://gitlab.com/appframework/tasker-web/compare/1.0.7...1.0.8) (2024-10-15)


### Bug Fixes

* Add missing CSS for completed tasks ([3564791](https://gitlab.com/appframework/tasker-web/commit/35647916f0e36ae384841c8278931e2599c90243))

## [1.0.7](https://gitlab.com/appframework/tasker-web/compare/1.0.6...1.0.7) (2024-10-14)


### Bug Fixes

* Add implementation of jakarta mail API ([46e2fa4](https://gitlab.com/appframework/tasker-web/commit/46e2fa4baf30b115889a182c5c963cc4023e9464))

## [1.0.6](https://gitlab.com/appframework/tasker-web/compare/1.0.5...1.0.6) (2024-10-14)

## [1.0.5](https://gitlab.com/appframework/tasker-web/compare/1.0.4...1.0.5) (2024-10-13)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to ^0.25.0 ([475bb1e](https://gitlab.com/appframework/tasker-web/commit/475bb1e6fa214c199297c9d1bc05f9179c317b5d))
* **deps:** update dependency primevue to v4.1.0 ([227af26](https://gitlab.com/appframework/tasker-web/commit/227af26ef76d4d773b6844b75490541db3ae05d9))

## [1.0.4](https://gitlab.com/appframework/tasker-web/compare/v1.0.3...1.0.4) (2024-10-12)

## [1.0.3](https://gitlab.com/appframework/tasker-web/compare/v1.0.2...v1.0.3) (2024-10-09)


### Bug Fixes

* **deps:** update dependency vue-i18n to v10.0.4 ([16b8992](https://gitlab.com/appframework/tasker-web/commit/16b8992a75a155f6b35cae4add129813accf3018))

## [1.0.2](https://gitlab.com/appframework/tasker-web/compare/v1.0.1...v1.0.2) (2024-10-07)


### Bug Fixes

* Prevent accidential filtering to just added labels ([7ae7129](https://gitlab.com/appframework/tasker-web/commit/7ae712989b3fee558afca2c84ebffacfd0d4df03))

## [1.0.1](https://gitlab.com/appframework/tasker-web/compare/v1.0.0...v1.0.1) (2024-10-05)

# 1.0.0 (2024-10-05)


### Bug Fixes

* Adapt to rest-API change ([19b3cbe](https://gitlab.com/appframework/tasker-web/commit/19b3cbe8f87e8a91619e7056e27af28be30e5e21))
* Add assigneeId field to mapper ([92acc02](https://gitlab.com/appframework/tasker-web/commit/92acc027e4ca806c9c5e99e10694ee5a477c8fdf))
* Add missing parameter ([910dfaf](https://gitlab.com/appframework/tasker-web/commit/910dfafb3d43fd3622ec36ea3b210e91f75d24a6))
* Add missing permission checks ([6a94cea](https://gitlab.com/appframework/tasker-web/commit/6a94ceadc374f4e5f00ac9d596e75694a6cee19f))
* Add SAP routing on production builds ([c7b43a9](https://gitlab.com/appframework/tasker-web/commit/c7b43a915e498a83cab95ba7a4cdd96960d1bf29))
* Add TASK permissions to admins by default ([505ad70](https://gitlab.com/appframework/tasker-web/commit/505ad703c924eda5e145b36b9c5e85c1cc872d9b))
* **ci:** Add "local" prefix to custom docker image ([9edb5fd](https://gitlab.com/appframework/tasker-web/commit/9edb5fdb6a3c7b568daaeefd294926ddced51b36))
* **ci:** Add prefix back ([239df63](https://gitlab.com/appframework/tasker-web/commit/239df63781f4f575a0d9f67ba6d0b0dd1c27c92b))
* **ci:** Enable docker-in-docker (dind) ([281e90e](https://gitlab.com/appframework/tasker-web/commit/281e90e0a4c7dd5b5469f8c805ea9068c5c3de5f))
* **ci:** Enable git submodules ([c812946](https://gitlab.com/appframework/tasker-web/commit/c812946e5573894e13d0ac24bf747b47307eb77b))
* **ci:** Fix stages ([b2417e1](https://gitlab.com/appframework/tasker-web/commit/b2417e1b48c1dde41df2eb4a802ca3fa6937c6bf))
* **ci:** Never try to pull custom image ([003585b](https://gitlab.com/appframework/tasker-web/commit/003585b21888bed6ba39c1ca77b74b9b442c9cf8))
* **ci:** Reenable sonar ([a1eeec4](https://gitlab.com/appframework/tasker-web/commit/a1eeec4d53b89351870bbecf0d89a859715e6260))
* **ci:** Try to enable dind ([2b0251b](https://gitlab.com/appframework/tasker-web/commit/2b0251bf742940adcccf0303944f145e8ee63852))
* **ci:** Try to repair submodules ([0462a31](https://gitlab.com/appframework/tasker-web/commit/0462a31f6e739e5f06152f594893e0d426a78d06))
* **ci:** Use alpine image ([98e5a71](https://gitlab.com/appframework/tasker-web/commit/98e5a71ca2695c44e836e839a9c570c2eacce1b2))
* **ci:** Use correct image name ([8efb680](https://gitlab.com/appframework/tasker-web/commit/8efb680dd2503de27c852f20fb450f9f43052da7))
* **ci:** Use correct JDK ([dd721dd](https://gitlab.com/appframework/tasker-web/commit/dd721ddf64067be8a95bba4dbd0b48119b008f4a))
* **ci:** Use different port ([8e2c865](https://gitlab.com/appframework/tasker-web/commit/8e2c8654f3030d3758764d1ff22d706364fc2bbd))
* Complete fix when searching for due date ([5287efd](https://gitlab.com/appframework/tasker-web/commit/5287efd59ca0e6ae45244284fcad70868e09521c))
* **deps:** update dependency @vueuse/core to v11.1.0 ([542c65e](https://gitlab.com/appframework/tasker-web/commit/542c65e51459b318c4f957e7b45638fcc6635c0a))
* **deps:** update dependency bootstrap-vue-next to v0.24.17 ([2719656](https://gitlab.com/appframework/tasker-web/commit/271965677570fb079c3a1c30c3aa98748cc752fe))
* **deps:** update dependency bootstrap-vue-next to v0.24.18 ([63c36a2](https://gitlab.com/appframework/tasker-web/commit/63c36a21ebb431d2ed8ad0be638f97612de44299))
* **deps:** update dependency bootstrap-vue-next to v0.24.21 ([2d245e3](https://gitlab.com/appframework/tasker-web/commit/2d245e3223a16814223dd1f16666b45b3ac406e8))
* **deps:** update dependency bootstrap-vue-next to v0.24.23 ([e2a3a50](https://gitlab.com/appframework/tasker-web/commit/e2a3a504a453f77eebee56c84fd41bdcac70efbf))
* **deps:** update dependency com.microsoft.playwright:playwright to v1.45.0 ([a824c21](https://gitlab.com/appframework/tasker-web/commit/a824c2159fecbd78f47f95d54efe46dbe03254c3))
* **deps:** update dependency com.microsoft.playwright:playwright to v1.45.1 ([ab5bf72](https://gitlab.com/appframework/tasker-web/commit/ab5bf72fae13ddaf0f7e99db3b44adb26452ca94))
* **deps:** update dependency com.microsoft.playwright:playwright to v1.46.0 ([dac8c25](https://gitlab.com/appframework/tasker-web/commit/dac8c2519f51239c56ee1d919b41b9f84272c51c))
* **deps:** update dependency date-fns to v4 ([0d1b894](https://gitlab.com/appframework/tasker-web/commit/0d1b8940ab34c84491974956ac1609ca7a178b9b))
* **deps:** update dependency io.quarkiverse.quinoa:quarkus-quinoa to v2.4.8 ([2bda303](https://gitlab.com/appframework/tasker-web/commit/2bda303dc99cac47e4cc74873e760461424eac64))
* **deps:** update dependency io.quarkiverse.quinoa:quarkus-quinoa to v2.4.9 ([10d32a8](https://gitlab.com/appframework/tasker-web/commit/10d32a884b835cefd99a78108e5ba43223f22b9c))
* **deps:** update dependency org.jsoup:jsoup to v1.18.1 ([68405f7](https://gitlab.com/appframework/tasker-web/commit/68405f7de2d193555dff7b19b52a7cdbff6c1669))
* **deps:** update dependency org.webjars.npm:angular-translate to v2.19.1 ([8241b56](https://gitlab.com/appframework/tasker-web/commit/8241b56e01a30037aa0615a9d21357229d987b58))
* **deps:** update dependency org.webjars.npm:angular-translate-loader-static-files to v2.19.1 ([395705a](https://gitlab.com/appframework/tasker-web/commit/395705aee0cb959f4fa44835ee73714cb3ae8ff2))
* **deps:** update dependency org.webjars.npm:dayjs to v1.11.11 ([2029cc4](https://gitlab.com/appframework/tasker-web/commit/2029cc460477204c2d68c6849a28bacbdd8359e6))
* **deps:** update dependency org.webjars.npm:dayjs to v1.11.12 ([2e70cb2](https://gitlab.com/appframework/tasker-web/commit/2e70cb294d5842f37f58e13aedc8791dd1465fcb))
* **deps:** update dependency org.webjars.npm:dayjs to v1.11.13 ([54882a6](https://gitlab.com/appframework/tasker-web/commit/54882a6083409a2bab2966a3aa39171a1e26496c))
* **deps:** update dependency vue-i18n to v10 ([9dbfef3](https://gitlab.com/appframework/tasker-web/commit/9dbfef31e128cd5e7b624feb4bce2bfb31646f37))
* **deps:** update dependency vue-i18n to v10.0.3 ([8c1370a](https://gitlab.com/appframework/tasker-web/commit/8c1370adc6f59a52a91696bbe94c2721e8372838))
* Enable date-only filtering ([372fe07](https://gitlab.com/appframework/tasker-web/commit/372fe07a569cff9981fc10df1f06a7b81725ab1c))
* **exclusions:** Regular expressions don't need to match completely ([80408ed](https://gitlab.com/appframework/tasker-web/commit/80408edeb16d8d42b5f622b04209151dd0ad31bf))
* Fix application after dependecy updates ([f60402e](https://gitlab.com/appframework/tasker-web/commit/f60402e18cc97189285e5a729dd5c2b1e32a935f))
* Fix build ([d14954e](https://gitlab.com/appframework/tasker-web/commit/d14954e4f2bfd4c3f7a3387e5f4fa70f3b4776ea))
* Fix build hickup ([8b34906](https://gitlab.com/appframework/tasker-web/commit/8b34906ec1ea89b2fb86b0605215bb4a188884e6))
* Fix creating new tasks ([0976acb](https://gitlab.com/appframework/tasker-web/commit/0976acbacf807f1415425b46b7445c799f74260b))
* Fix due handling ([2d16dfd](https://gitlab.com/appframework/tasker-web/commit/2d16dfde3e6cb5da5b9f85aabad2a9f9b07693bd))
* Fix eslint warnings ([59eef13](https://gitlab.com/appframework/tasker-web/commit/59eef132e4dc445b742bcd55608aec00be3b8898))
* Fix new assignee handling ([8aaa15d](https://gitlab.com/appframework/tasker-web/commit/8aaa15dd678e24c66b5aedfd5add1150f92ada1c))
* Fix passing string instead of object ([0f24d4e](https://gitlab.com/appframework/tasker-web/commit/0f24d4e17c895a7e491e2badf3394dcd4f404142))
* Fix showing assingnee ([3a8ce0d](https://gitlab.com/appframework/tasker-web/commit/3a8ce0d60d9d8a7c8296121eb11fc51fc8a8efeb))
* Fix task display ([ea9ddcb](https://gitlab.com/appframework/tasker-web/commit/ea9ddcb1139631e4e8169d980891ad0e2c7d88b4))
* Flush before returning ([7bc3291](https://gitlab.com/appframework/tasker-web/commit/7bc3291272e68c9f42fc787a9185c0b7d13cea0a))
* Hide ID field when creating new task ([ad66a21](https://gitlab.com/appframework/tasker-web/commit/ad66a213152cab1358ef6c8c3fdfa63a2dc72a3e))
* Indent in README to highlight commands ([3c62f17](https://gitlab.com/appframework/tasker-web/commit/3c62f1744f1d566775ef147ac0613a88e0365fe3))
* **mapping:** Adapt code for removal of assigneeId ([1aa4dd9](https://gitlab.com/appframework/tasker-web/commit/1aa4dd9acce8383da92bf0e956bcba443f8ccc7c))
* Minor bugfixes seen when running 'build prod' ([9d24a91](https://gitlab.com/appframework/tasker-web/commit/9d24a91148596030feb3fff5f5862754926b309f))
* Minor translation fixes ([6837caf](https://gitlab.com/appframework/tasker-web/commit/6837caf4afed95ce7892f9ce4faea8cbf3c5d669))
* Styling fixes ([5372a11](https://gitlab.com/appframework/tasker-web/commit/5372a1179b6866073a3010a418505b8cbadb9378))
* Use JDK 21 for docker images ([8b25914](https://gitlab.com/appframework/tasker-web/commit/8b259141875c0eb364e3ebe9e9c6c8f4cb00ad05))


### Features

* Add build timestamp to app ([de2c6ce](https://gitlab.com/appframework/tasker-web/commit/de2c6ce9aba73d31780e47fdbac5fc0126c057ed))
* Add color alternatives ([8685de4](https://gitlab.com/appframework/tasker-web/commit/8685de4395ee986d082013e50edcf594bca142cb))
* Add CRUD tests for priority and generic task status ([f93d4d5](https://gitlab.com/appframework/tasker-web/commit/f93d4d5414d758e784ef25312a6b9efbf28f12ff))
* Add CRUD tests to ImporterConfiguration ([b69c3a8](https://gitlab.com/appframework/tasker-web/commit/b69c3a88c7ffd6244bf94ba51b9a206436cca705))
* Add CRUD tests to Label ([12bd4a5](https://gitlab.com/appframework/tasker-web/commit/12bd4a55908d15abe8b9560c4f450a8e563f3206))
* Add CRUD tests to Task ([d9ed983](https://gitlab.com/appframework/tasker-web/commit/d9ed983b9fec7a9251142d5bc43a86ace1a4f340))
* Add support for previews ([25e8ad1](https://gitlab.com/appframework/tasker-web/commit/25e8ad122ddfd927234c0a79d24c29d3e943f919))
* Add unit test for pager.js ([fd45a39](https://gitlab.com/appframework/tasker-web/commit/fd45a392c9806d8a466640eb89662a898a28071e))
* **ci:** Add .gitlab-ci.yml ([2bbb007](https://gitlab.com/appframework/tasker-web/commit/2bbb00787ea78ddb65414e3990b43df09432a98a))
* **ci:** Add sonarcloud ([83a3e61](https://gitlab.com/appframework/tasker-web/commit/83a3e61b86476ba3f85e88893b1fb0d8de0064a9))
* **ci:** Add test stage ([83fd4d6](https://gitlab.com/appframework/tasker-web/commit/83fd4d6e5cf65c07bb92a7e7a0d226557c723ba1))
* **ci:** Use default quarkus pipeline ([cab05c9](https://gitlab.com/appframework/tasker-web/commit/cab05c94e5311529a794dfda80b204f3afc2f822))
* Design improvements ([e0e1924](https://gitlab.com/appframework/tasker-web/commit/e0e1924af3ac0be2c4dfaf2b5be34ad88f29d6b9))
* Enable stylistic eslint ([f5ad2f5](https://gitlab.com/appframework/tasker-web/commit/f5ad2f5dc50f4e9f5474773f1157f109c0b4a948))
* Extend tests ([e14696c](https://gitlab.com/appframework/tasker-web/commit/e14696cebb20b8178555e878ce2714f9aa79a9a6))
* Implement chart view ([d82fda2](https://gitlab.com/appframework/tasker-web/commit/d82fda2173a6a5adbfe84b7bee1a9abf072148ac))
* Implement profile editing ([264e2ef](https://gitlab.com/appframework/tasker-web/commit/264e2ef0e7b054cb5feb6a88cb09204fb47d4679))
* Implement switching locales ([43048fd](https://gitlab.com/appframework/tasker-web/commit/43048fddb3743382b2cb845d7a34fc464b7baa6d))
* Improve login/logout ([91484bf](https://gitlab.com/appframework/tasker-web/commit/91484bfea3de562601468ad9b85a0f79b17c0250))
* Improve usage of bootstrap-icons ([bc0d73c](https://gitlab.com/appframework/tasker-web/commit/bc0d73c884298259f4dab3c99d598a654554c7f8))
* Introduce common ListComponent for tables ([d1f28d7](https://gitlab.com/appframework/tasker-web/commit/d1f28d70d0aecc53d01973aaf11b860fd5bc256a))
* Migrate from handwritten npm-integration to quarkus-quinoa ([cc92b2d](https://gitlab.com/appframework/tasker-web/commit/cc92b2d663203268ee160e4aa969bb3eb8060b06))
* Move version extraxtion to appframework ([a7415c8](https://gitlab.com/appframework/tasker-web/commit/a7415c8bdd220c6e528d4845c2c9b45137f4f13a))
* **test:** Convert UI tests to playwright ([7c49b6c](https://gitlab.com/appframework/tasker-web/commit/7c49b6cff2807667410195d058356a5fc4c9548c))
* **ui:** Switch from plain Javascript to Typescript ([72405cb](https://gitlab.com/appframework/tasker-web/commit/72405cb897b53d077e8eef036ee717845b11d214))
* Update test setup to rely soley on flyway scripts ([e5718c0](https://gitlab.com/appframework/tasker-web/commit/e5718c05889e2b2740b2e6e2191bb4a895decd42))
* Update to appframework 3.0.0-SNAPSHOT ([d0db003](https://gitlab.com/appframework/tasker-web/commit/d0db003c521651781405e10aade71e5fc1841583))
* Use problem details in UI ([3ca4264](https://gitlab.com/appframework/tasker-web/commit/3ca4264aa3b773bd7f55918ed9160a9db45d132d))
