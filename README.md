# tasker

*tasker* is a mixture between a TODO list, a reminder tool and a knowledge base.

## Building the image

    mvn install -DskipTests -Dquarkus.container-image.build=true -Dquarkus.container-image.tag=latest

## Deploying without an image

    rsync -av --delete target/quarkus-app/ YOURSERVER:/opt/tasker/

## Creating new tests

    mvn exec:java -e -D exec.mainClass=com.microsoft.playwright.CLI -D exec.args="codegen localhost:8081"
